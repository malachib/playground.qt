import QtQuick
import QtQuick.Controls
import QtQuick.Window

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")


    ShaderEffectSource {
        id: theSource
        sourceItem: theItem
    }

    Image {
        id: theItem
        width: 160
        height: 140
        source: "content/qt-logo.png"
    }


    ShaderEffect {
        width: 160
        height: 160
        property variant source: theSource
        property variant shadow: ShaderEffectSource {
            sourceItem: ShaderEffect {
                width: theItem.width
                height: theItem.height
                property variant delta: Qt.size(0.0, 1.0 / height)
                property variant source: ShaderEffectSource {
                    sourceItem: ShaderEffect {
                        width: theItem.width
                        height: theItem.height
                        property variant delta: Qt.size(1.0 / width, 0.0)
                        property variant source: theSource
                        fragmentShader: "content/shaders/blur.frag.qsb"
                    }
                }
                fragmentShader: "content/shaders/blur.frag.qsb"
            }
        }
        property real angle: 0
        property variant offset: Qt.point(15.0 * Math.cos(angle), 15.0 * Math.sin(angle))
        NumberAnimation on angle { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: 6000 }
        property variant delta: Qt.size(offset.x / width, offset.y / height)
        property real darkness: shadowSlider.value
        fragmentShader: "content/shaders/shadow.frag.qsb"
        Slider {
            id: shadowSlider
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.leftMargin: 4
            anchors.rightMargin: 4
            height: 40
        }
    }
}
