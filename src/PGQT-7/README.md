# Overview

Qt Shader usage has changed rapidly through the years.

## spir-v

As outlined in [1] this appears to be the 6.5 LTS preferred way to do things.
Code pared down and adapted from [3] and [3.1]

It's noteworthy that [3.1] checked in `.qbs` files, but that's not needed as they
are generated and embedded into resources as part of build [4]

# References

1. https://doc.qt.io/qt-6/qtshadertools-overview.html
2. https://doc.qt.io/qt-6/qml-qtquick-shadereffect.html
3. https://doc.qt.io/qt-6/qtquick-shadereffects-example.html
   1. https://code.qt.io/cgit/qt/qtdeclarative.git/tree/examples/quick/shadereffects?h=6.5
4. https://doc.qt.io/qt-6/qtshadertools-build.html