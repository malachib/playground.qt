# -*- coding: utf-8 -*-

try:
    from PySide2 import QtCore
    from PySide2 import QtWidgets
    from PySide2 import QtQuick
except:
    from PyQt5.QtCore import pyqtSlot as Slot
    from PyQt5 import QtCore
    from PyQt5 import QtWidgets
    from PyQt5 import QtQuick

from pyside.DarkSky import *

class Weather(QObject):
    forecastUpdated = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self._forecast = StaticForecast()
        self._datapoint = DataPoint(self._forecast.currently())

    @Property(DataBlock, notify=forecastUpdated)
    def daily(self):
        return DataBlock(self._forecast.daily(), DailyDataPoint)

    @Property(DataBlock, notify=forecastUpdated)
    def hourly(self):
        return DataBlock(self._forecast.hourly(), HourlyDataPoint)
