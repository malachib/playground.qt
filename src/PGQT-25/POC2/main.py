#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QApplication
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QUrl
from PySide2.QtQml import qmlRegisterType

from pyside.Weather import *
from pyside.DarkSky import *
import forecastio

# to help with registering types
# https://stackoverflow.com/questions/50160098/pyside2-with-qml-as-ui

qmlRegisterType(Weather, "PGQT", 1, 0, "Weather")
qmlRegisterType(DataPoint, "PGQT", 1, 0, "DataPoint")
qmlRegisterType(DataBlock, "PGQT", 1, 0, "DataBlock")
qmlRegisterType(DataBlockModel, "PGQT", 1, 0, "DataBlockModel")

def get_apikey():
    file = open("conf/darksky-api-key", "r")
    # read API key and yank off trailing whitespace
    api_key = file.read().rstrip()
    #print('API key: {}'.format(api_key))
    return api_key

def load_forecast():
    lat = -31.967819
    lng =  115.87718
    forecast = forecastio.load_forecast(get_apikey(), lat, lng)

app = QApplication([])

view = QQuickView(QUrl("view.qml"))
view.show()

load_forecast()

app.exec_()
