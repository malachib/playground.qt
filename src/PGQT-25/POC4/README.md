# Observations & Opinions

## Initial Configuration

Deviates notable from guide [1] namely in that:

1. Empty Application (without Python in the name) is new option
2. Gives option to set up virtual environment (nice)
3. MAY be dependent on Python language server PyLS.  Installing it
4. Does not auto-install PySide in venv, though offers to do so (curious).  Taking it up on the offer.

Note that it doesn't seem to give you option to choose particular PySide6 and installs
the latest.  Unknown if latest works with previous Qt Kits i.e. PySide6 6.7.2 does it
work with Qt 6.6.3?

Fortunately one is able to update requirements.txt directly it seems

# References

1.  https://doc.qt.io/qtforpython-6.5/tutorials/qmlapp/qmlapplication.html