# Overview

# Observations & Opinions

## Qt niceties

Web searches for Qt/QML in PyCharm reveal QML plugin, but doing search in marketplace reveals nothing.  Beforehand, PyCharm briefly advertised that QML support is available
in PyCharm pro.

It's not a deal breaker, but it's a negative lacking intellisense of any kind for QML

Looks like it may be a recent thing that you need Pro [1]. [1.1] is of interest

# References

1. https://www.jetbrains.com/help/pycharm/qml-syntax-support.html
    1. https://youtrack.jetbrains.com/issue/CPP-4576