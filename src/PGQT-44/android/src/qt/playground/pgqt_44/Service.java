package qt.playground.pgqt_44;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.qtproject.qt.android.bindings.QtService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.os.Build;
import android.os.IBinder;

public class Service extends QtService
{
    private static final String TAG = "PGQT-44 Service";
    private static final String CHANNEL_ID = "PGQT-44 Channel";

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(TAG, "Creating service");
    }

    // From [3.1]
    void createNotificationChannel()
    {
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
       {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
       }
    }

    // From [3] and [3.1]
    void foregroundHelper(Context context, Intent intent)
    {
        //String input = intent.getStringExtra("inputExtra");
        String input = "PGQT-44";
        createNotificationChannel();
        Intent notificationIntent = new Intent(context, Service.class);
        // FLAG_IMMUTABLE is new for API 30+ [5.1]
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
        Notification notification = new Notification.Builder(context, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_LOW)
                .setOngoing(true)
                // NOTE: Icon is critical to foreground service working right.
                // No default icon appears, and service will just exit without this
                .setSmallIcon(android.R.drawable.ic_lock_idle_alarm)
                .build();

        startForeground(1, notification);
    }

    // [4]
    public static void standaloneNotification(Context context, String message)
    {
        try
        {
            NotificationManager manager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            //{
                 NotificationChannel channel = new NotificationChannel(
                         "Qt",
                         "Qt Notifier",
                         NotificationManager.IMPORTANCE_DEFAULT
                 );
                 manager.createNotificationChannel(channel);
            //}

            Notification notification = new Notification.Builder(context, channel.getId())
                .setContentTitle("PGQT-44 Standalone")
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_SOUND)
                // DEBT: Use a different icon here!
                .setSmallIcon(android.R.drawable.ic_media_next)
                //.setColor(Color.GREEN)
                .build();

            Log.i(TAG, "standaloneNotification");
            manager.notify(0, notification);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    // [3.2]
    void evaluateIncomingIntent(Intent intent)
    {
        if(intent != null && intent.getAction() != null)
        {
            Log.d(TAG, "evaluateIncomingIntent");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        int ret = super.onStartCommand(intent, flags, startId);
        //Context context = getApplicationContext();
        Context context = this;

        evaluateIncomingIntent(intent);

        // [2] Creates an infinite loop, I think it relates to
        // descrepency between onStartCommand and other 'start' mechanism I've seen
        //context.startForegroundService(intent);
        
        foregroundHelper(context, intent);

        Log.i(TAG, "onStartCommand");

        // Seems to work ok, just be sure to do notification channel (probably for permissions)
        // otherwise Android gets super mad and disconnects while deploying
        standaloneNotification(context, "Testing 1 2 3");

        // returning START_NOT_STICKY as per [3.1]
        return START_NOT_STICKY;
        //return ret;
    }

    @Override
    public void onDestroy()
    {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    // [3.1]
    //@Nullable
    @Override
    public IBinder onBind(Intent intent) { return null; }
}
