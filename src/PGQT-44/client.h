#pragma once

#include <QObject>

class NotifyClient : public QObject
{
    Q_OBJECT

public:
    NotifyClient(QObject* parent) : QObject(parent) {}

public slots:
    void notify(const QString& v);
};


