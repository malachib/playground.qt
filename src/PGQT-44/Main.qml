import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

Window {
    id: root
    width: 640
    height: 480
    visible: false
    property bool isReady: false
    title: qsTr("PGQT-44: Hello World")

    property int notifyCount: 0
    signal notifyRequested(msg: string)

    ColumnLayout {
        anchors.fill: parent
        anchors.centerIn: parent

        Button {

            Layout.fillWidth: true
            text: "PGQT-44: Notify"
            onClicked: notifyRequested(notifyCount++)
        }

    }

    /*
    Timer {
        id: timer
        interval: 1000
        running: false
        repeat: false
        onTriggered: root.visible = true;//console.log("TEST")
    } */

    //onIsReadyChanged: timer.start()

    Image {
        id: image
        // DEBT: Doesn't match aspect/resolution of our splash screen
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
        source: "android/res/img/green-5341423_1920.jpg"
    }

    PropertyAnimation {
        target: image;
        id: imageFade
        property: "opacity"
        to: 0
        duration: 3000
    }

    onIsReadyChanged: {
        visible = true
        imageFade.running = true
    }

    // Get out of the way of incoming splash screen [7]
    Loader {
        id: loader
        asynchronous: true
        anchors.fill: parent
        sourceComponent: Window {
            width: root.width
            height: root.height

            Component.onCompleted: root.isReady = true
        }
    }
}
