# Qt Notification / Service (to flex real Java)

Document v0.1

# 1. Goals

# 2. Infrastructure

Testing on:

* Pixel 3 w/ Qt 6.6.2 android_arm64_v8a
* Qt targets SDK android-33
* Qt targets build-tools 33.0.2
* Qt targets NDK 25.1.8937393

# 3. Observations

## 3.1. Android Studio

### 3.1.1. Logcat

Logcat is still maximum awesome

### 3.1.2. IDE itself

Not a slight against the IDE, but it doesn't resolve QtService and thusly provides limited value

## 3.2. Android Services

Looks like "Foreground Service" is more what you'd think of as a background service [6] it's the most persistent of the bunch.

## 3.3. App: Notifications

If nighttime mode is on you may not see regular notifications.  Watch out!

## 3.4. App: Splash Screen

Was getting ready to follow lots of guidance [7] - however, so far Qt Creator's AndroidManifest.xml editor got our leaves splash
screen online(ish).  What does that mean?  It comes up, but as some[7] (myself included) complain, you get a flash of white first.

# 4. Troubleshooting

## 4.1. App Catastrophic Failures

### 4.1.1. Deploy Problems

### 4.1.1.1. Won't Upload

```
23:39:41: The process "/media/usb-ssd/malachi_home/Qt/6.6.2/gcc_64/bin/androiddeployqt" exited with code 16.
23:39:41: Installing the app failed with an unknown error.
23:39:41: Pulling files necessary for debugging.
23:39:41: Package deploy: Running command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/bin/app_process64 /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/app_process".
23:39:41: The command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/bin/app_process64 /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/app_process" terminated with exit code 1.
23:39:41: Package deploy: Failed to pull "/system/bin/app_process64" to "/home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/app_process".
23:39:41: Package deploy: Running command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/bin/linker64 /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/linker64".
23:39:41: The command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/bin/linker64 /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/linker64" terminated with exit code 1.
23:39:41: Package deploy: Failed to pull "/system/bin/linker64" to "/home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/linker64".
23:39:41: Package deploy: Running command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/lib64/libc.so /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/libc.so".
23:39:41: The command "/home/malachi/Android/Sdk/platform-tools/adb -s 8C8X1KN0E pull /system/lib64/libc.so /home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/libc.so" terminated with exit code 1.
23:39:41: Package deploy: Failed to pull "/system/lib64/libc.so" to "/home/malachi/Projects/playground/playground.qt/src/build-PGQT-44-Android_Qt_6_6_2_Clang_arm64_v8a-Debug/android-app-process/libc.so".
Error while building/deploying project PGQT-44 (kit: Android Qt 6.6.2 Clang arm64-v8a)
When executing step "Deploy to Android device"
23:39:41: Elapsed time: 00:08.
```

Forceful uninstall as per 4.1.1.2. gets us back to deploying again

Use logcat, startup intents bit me a bunch here, such as:

* Android needed an Icon when using a splash screen, couldn't get it, aborted process

### 4.1.1.2. Won't Start

```
Activity Manager threw the error: Error type 3
Error: Activity class {qt.playground.pgqt_44/org.qtproject.qt.android.bindings.QtActivity} does not exist.
```

As described in [5]

I got mileage out of running:

```
adb uninstall qt.playground.pgqt_44
```

### 4.1.1.3. Odd Crash on start

Appears to occurr if you attempt a notify without proper NotificationChannel

```
2024-03-24 01:00:59.984 12630-12630 PersistedS...edReceiver com....android.permissioncontroller  D  Received android.intent.action.PACKAGE_FULLY_REMOVED for qt.playground.pgqt_44 for u0
2024-03-24 01:01:00.000  3907-4804  MediaProvider           com...ndroid.providers.media.module  I  Begin Intent { act=android.intent.action.PACKAGE_FULLY_REMOVED dat=package:qt.playground.pgqt_44 flg=0x5000010 cmp=com.google.android.providers.media.module/com.android.providers.media.MediaService (has extras) }
2024-03-24 01:01:00.004  3907-4804  MediaProvider           com...ndroid.providers.media.module  D  Deleted 0 Android/media items belonging to qt.playground.pgqt_44 on /data/user/0/com.google.android.providers.media.module/databases/external.db
2024-03-24 01:01:00.008  3907-4804  MediaProvider           com...ndroid.providers.media.module  I  End Intent { act=android.intent.action.PACKAGE_FULLY_REMOVED dat=package:qt.playground.pgqt_44 flg=0x5000010 cmp=com.google.android.providers.media.module/com.android.providers.media.MediaService (has extras) }
2024-03-24 01:01:01.097 16280-16280 AppButtonsPrefCtl       com.android.settings                 D  Got broadcast response: Restart status for qt.playground.pgqt_44 false
2024-03-24 01:01:05.335  2000-2122  PackageManager          system_server                        E  Missing permission state for qt.playground.pgqt_44 and user 0
2024-03-24 01:01:05.396  2000-2122  PackageManager          system_server                        E  Missing permission state for qt.playground.pgqt_44 and user 0
```

### 4.1.2. Runtime Problems

#### 4.1.2.1. "keeps stopping"

FLAG_IMMUTABLE and friends:

```
FATAL EXCEPTION: main
      Process: qt.playground.pgqt_44:qt_service, PID: 16952
      java.lang.RuntimeException: Unable to start service qt.playground.pgqt_44.Service@80f4648 with Intent { cmp=qt.playground.pgqt_44/.Service }: java.lang.IllegalArgumentException: qt.playground.pgqt_44: Targeting S+ (version 31 and above) requires that one of FLAG_IMMUTABLE or FLAG_MUTABLE be specified when creating a PendingIntent.
      Strongly consider using FLAG_IMMUTABLE, only use FLAG_MUTABLE if some functionality depends on the PendingIntent being mutable, e.g. if it needs to be used with inline replies or bubbles.
      	at android.app.ActivityThread.handleServiceArgs(ActivityThread.java:4661)
      	at android.app.ActivityThread.access$2000(ActivityThread.java:247)
      	at android.app.ActivityThread$H.handleMessage(ActivityThread.java:2095)
      	at android.os.Handler.dispatchMessage(Handler.java:106)
      	at android.os.Looper.loopOnce(Looper.java:201)
      	at android.os.Looper.loop(Looper.java:288)
      	at android.app.ActivityThread.main(ActivityThread.java:7842)
      	at java.lang.reflect.Method.invoke(Native Method)
      	at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:548)
      	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:1003)
      Caused by: java.lang.IllegalArgumentException: qt.playground.pgqt_44: Targeting S+ (version 31 and above) requires that one of FLAG_IMMUTABLE or FLAG_MUTABLE be specified when creating a PendingIntent.
      Strongly consider using FLAG_IMMUTABLE, only use FLAG_MUTABLE if some functionality depends on the PendingIntent being mutable, e.g. if it needs to be used with inline replies or bubbles.
      	at android.app.PendingIntent.checkFlags(PendingIntent.java:375)
      	at android.app.PendingIntent.getActivityAsUser(PendingIntent.java:458)
      	at android.app.PendingIntent.getActivity(PendingIntent.java:444)
      	at android.app.PendingIntent.getActivity(PendingIntent.java:408)
      	at qt.playground.pgqt_44.Service.foregroundHelper(Service.java:50)
      	at qt.playground.pgqt_44.Service.onStartCommand(Service.java:88)
      	at android.app.ActivityThread.handleServiceArgs(ActivityThread.java:4643)
      	... 9 more
```

Adding `FLAG_IMMUTABLE` as per [5.1] gets us back on track