// For QAndroidService
#include <QtCore/private/qandroidextras_p.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <pgqt/lib.h>

#include "client.h"

// Prone to https://bugreports.qt.io/browse/QTCREATORBUG-6864, bleh

void NotifyClient::notify(const QString& v)
{
    auto javaString = QJniObject::fromString(v);
    QJniObject::callStaticMethod<void>("qt/playground/pgqt_44/Service",
        "standaloneNotification",
        "(Landroid/content/Context;Ljava/lang/String;)V",
        QNativeInterface::QAndroidApplication::context(),
        javaString.object<jstring>());
}


static void start_service()
{
    auto activity = QJniObject(QNativeInterface::QAndroidApplication::context());
    QAndroidIntent serviceIntent(activity.object(),
                                 "qt/playground/pgqt_44/Service");
    QJniObject result = activity.callObjectMethod(
        "startService",
        "(Landroid/content/Intent;)Landroid/content/ComponentName;",
        serviceIntent.handle().object());
}


int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationName("PGQT-44");

    // Straight outta [1.4]
    if (argc <= 1) {
        // code to handle main activity execution
        qDebug() << "Normal startup";
        start_service();
    } else if (argc > 1 && strcmp(argv[1], "-service") == 0) {
        // Verified with raw logcat this does happen.  Cool.  Just not visible
        // in normal Qt Creator app output
        qInfo() << "Service starting with from the same .so file";

        QAndroidService app(argc, argv);
        return app.exec();
    } else {
        qWarning() << "Unrecognized command line argument";
        return -1;
    }

    QGuiApplication app(argc, argv);

    //pgqt::v1::dumpQrc();

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/qt/qml/PGQT-44/Main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl)
        {
            //qDebug() << "GOT: " << obj;
            auto client = new NotifyClient(obj);

            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);

            QObject::connect(
                obj, SIGNAL(notifyRequested(QString)),
                client, SLOT(notify(QString)));

        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
