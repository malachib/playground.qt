# Overview

Guidance from references below

NOTE: That QML Camera approach in qt 6.2 is very different than 5.x (may be breaking changes)

# References

1. https://doc.qt.io/qt-6.2/qml-qtmultimedia-camera.html
2. https://doc.qt.io/qt-6.2/cameraoverview.html
3. https://stackoverflow.com/questions/54722290/scaling-and-positioning-in-qml
