// References documented in README.md

#include <QCameraDevice>
#include <QGuiApplication>
#include <QMediaDevices>
#include <QQmlApplicationEngine>
#include <QDebug>


bool checkCameraAvailbility()
{
    QList<QCameraDevice> videoInputs = QMediaDevices::videoInputs();

    qDebug() << "checkCameraAvailbility: videoInputs.count=" << videoInputs.count();

    if (videoInputs.count() > 0)
        return true;
    else
        return false;
}

int main(int argc, char *argv[])
{
    if(!checkCameraAvailbility()) return -1;

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/PGQT_36/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
