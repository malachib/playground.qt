// References documented in README.md

import QtQuick

//import QtGraphicalEffects 1.0
import QtMultimedia 6.2

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    MediaDevices {
        id: mediaDevices
    }

    CaptureSession {
        camera: Camera {
            id: camera

            cameraDevice: mediaDevices.defaultVideoInput
            focusMode: Camera.FocusModeAutoNear
            customFocusPoint: Qt.point(0.2, 0.2) // Focus relative to top-left corner

            // EXPERIMENTATION, all kinda "works"
            //zoomFactor: maximumZoomFactor
            //whiteBalanceMode: Camera.WhiteBalanceManual
            //colorTemperature: 5600
        }
        videoOutput: videoOutput
    }

    VideoOutput {
        id: videoOutput
        anchors.fill: parent
        visible: true
    }

    Rectangle {
        id: videoContent
        //color: "blue"
        //opacity: 0.2

        visible: false
        anchors.left: videoOutput.left
        anchors.right: videoOutput.right

        y: videoOutput.contentRect.y
        height: videoOutput.contentRect.height
    }

    Rectangle {
        id: infoBar
        anchors.left: videoContent.left
        anchors.right: videoContent.right
        anchors.top: videoContent.top
        color: "red"
        opacity: 0.2
        height: 25
    }

    Image {
        id: logo
        source: "images/qt-logo.jpg"
        opacity: 0.2
        // "doesn't actually change size of element" [3]
        //scale: 0.3    // this scale happens post-anchor, so not so great for our needs
        //anchors.left: videoOutput.horizontalCenter
        anchors.right: videoContent.right
        anchors.top: videoContent.verticalCenter
        anchors.bottom: videoContent.bottom

        fillMode: Image.PreserveAspectFit

        /*
         * scaling is somewhat at odds with anchoring+preserve aspect fit, as they are already scaling
        transform: Scale {
            xScale: 0.3
            yScale: 0.3
        } */
    }

    /*  Not really Qt6 compatible
    ColorOverlay {
        source: logo
    }   */
}
