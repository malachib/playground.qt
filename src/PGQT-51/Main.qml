import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import pgqt_51

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-51")

    ColumnLayout {
        anchors.fill: parent

        Button {
            text: "hi"
            onClicked: Session.request()
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ScrollBar.vertical.policy: ScrollBar.AlwaysOn

            TextArea {
                id: output
                readOnly: true
            }
        }
    }

    Connections {
        target: Session

        function onGotReply(s)
        {
            console.info("onGotReply: ", s);
            output.text = s
        }
    }
}
