## Notes

QCoro seems to generally require WebSockets
Enscripten (WASM compiler) install guidance here: https://doc.qt.io/qt-6/wasm.html
QCoro as of this writing (v0.10.0) uses experimental::coroutines_traits which
LLVM indicates will be removed in LLVM 15

## Results

Windell Laptop:

09DEC23 Qt 6.5.0 MSVC = compiles and runs
09DEC23 Qt 6.6.1 MSVC = cmake config fails due to lack of websockets
09DEC23 Qt 6.6.1 WASM = enscripten not configured (not QCoro's fault)

C131 Debian VM

21DEC23 Qt 6.6.1 GCC (Desktop) = compiles and runs
21DEC23 Qt 6.6.1 WASM (Single Threaded) = compiles and runs
21DEC23 Qt 6.6.1 Clang arm64-v8a = compiles and runs
