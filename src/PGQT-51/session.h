#pragma once

#include <QObject>
#include <QNetworkAccessManager>

#include <qcorotask.h>

// TODO: Consider testing QML_ELEMENT here also
class Session : public QObject
{
    QNetworkAccessManager nam_;

    Q_OBJECT

public:
    Session(QObject* parent = nullptr) : QObject(parent) {}

public slots:
    QCoro::Task<> request();

signals:
    void gotReply(QString);
};
