#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

// DEBT: We expect this to work.  Instead, dbus include dies - wouldn't think top level include
// would demand an optional component's presence
//#include <qcoro.h>

#include <qcorotask.h>

#include "session.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterSingletonInstance("pgqt_51", 1, 0, "Session", new Session(engine.rootContext()));

    // DEBT: Rework this to use engine.loadFromModule technique
    const QUrl url(u"qrc:/qt/qml/PGQT-51/Main.qml"_qs);
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreated,
        &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
