#include <QNetworkRequest>
#include <QString>
#include <QUrl>

#include <qcorotask.h>
#include <qcoro/network/qcoronetwork.h>

#include "session.h"

// FIX: "QVariant: Provided metatype for 'QCoro::Task<void>' does not support destruction and copy construction" error
// If this weren't such a simple proof of concept, I might label that as DEBT.  Instead, this proof of concept is not
// fuly viable if all code which references it has this warning.
QCoro::Task<> Session::request()
{
    QNetworkRequest r(QString("https://api.weather.gov/stations?limit=5"));
    auto* reply = co_await nam_.get(r);
    QByteArray data = reply->readAll();
    emit gotReply(QString::fromUtf8(data));
}
