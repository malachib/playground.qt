# Overview

# 1. Targets

## 1.1. Desktop GCC (Debian)

Primary target

### 1.1.1. Design Goals

We specifically want an end-user access to Google Drive API.
Vendor level access (shared across all PGQT-624s) is interesting, but technically
outside the scope of our goals.

## 1.2. Android

Not a critical target

I anticipate platform differences will create unsatisfying results without
significant elbow grease to integrate PGQT-54 lessons.

## 1.3. WASM

Not a critical target

I anticipate major issues here, but will give it a try.

# 2. RESERVED

# 3. Observations

## 3.1. googleQt lib

On casual glance I like the feeling I get [2].  I don't like that there's just a master branch with no tags.

Noteworthy is a much fresher fork [2.1].  Their commits look sensible, so testing against the fork.

Rather large library, takes a while to compile.

### 3.1.1. CMakeLists.txt

README indicates to use .pro file, but we have CMakeLists.txt.  I'll take it.

Inspecting CMakeLists.txt shows quite a few required dependencies, including:

* Sql
* Xml
* Core5Compat

Noteworthy is he only pulls in those dependencies if Qt6Core is NOT found.  A little odd.

### 3.1.2. Size

Lib is very big.  Not that this is a major factor, but rough numbers is it takes over 10 minutes to compile on ChromeBook.

### 3.1.3. PR thoughts

I hope dependencies don't interrupt Android compatibility.  I imagine I could PR and make Sql optional.

Maybe a PR to split it up due to size.  That might obviate the desire for optional components above.

### 3.1.4. async

Nifty this seems to use something like a QCoro approach.  Not sure if it's only fluent or if it uses the language constructs.  In either case, nice to have.

## 3.2. google-drive-qt

This one tends to come up in all the searches [3] and is positively ancient.
Last commit was ~2013.  No thank you.

## 3.3. gRPC

It occurs to me that gRPC was a fundamental tech that a lot of Google APIs relied on.  See if there's a more direct (Google supported?) path to using gRPC.

## 3.4. NDK

It occurs to me that C++ is last in line, but very much supported by Google.  If not 3.3.
gRPC specifically, investigate in a broader scope canonical support for GCP and friends.

# 4. Troubleshooting

## 4.1. ChromeBook Debian missing dependencies

```
Target "googleQt" links to:

    Qt6::Xml

but the target was not found.
```

While trying to bring up googleQt, Xml dependency is not satisifed.  This is odd because `libqt6xml6` is present.

Reinstalling package doesn't help.

Turns out the odd 3.1.1. Qt6Core choice means WE have to pull in Xml, Sql, etc. ourselves.

# References

1. https://www.qt.io/blog/2017/01/25/connecting-qt-application-google-services-using-oauth-2-0
2. https://github.com/osoftteam/googleQt
    1. https://github.com/jarkkokoivikko-code-q/googleQt
3. https://github.com/jalcine/google-drive-qt