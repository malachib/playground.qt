
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "session.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    auto session = new Session(&engine);

    session->test1();

    qmlRegisterSingletonInstance("PGQT_62", 1, 0, "Session", session);

    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

#if QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
    engine.load("qrc:/qt/qml/PGQT-62/Main.qml");
#else
    engine.loadFromModule("PGQT-62", "Main");
#endif

    return app.exec();
}
