#pragma once

#include "nimble/ble.h"
#include "modlog/modlog.h"
// DEBT: Example uses this, we probably need it too
//#include "esp_peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ble_hs_cfg;

#ifdef __cplusplus
}
#endif