#include <QGuiApplication>

// Somewhat entertainingly, #if QT_CONFIG(permissions) check catastrophically
// fails on Qt 6.4.2.  So we have to do this instead
#if QT_VERSION < QT_VERSION_CHECK(6, 5, 0)
#define QT_HAS_BT_PERMISSION 0
#else
#define QT_HAS_BT_PERMISSION 1
#endif

#if QT_HAS_BT_PERMISSION
#include <QBluetoothPermission>
#endif
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QVariant>

#if PGQT_62_COAP
#include <estd/sstream.h>
#include <coap/encoder/streambuf.h>
#endif

#include "session.h"
#if PGQT_62_PROTOBUF
#include "version.pb.h"
#endif

void Session::test1()
{
    // permission guidance from [4], [4.1], [4.3]
#if QT_HAS_BT_PERMISSION
    QBluetoothPermission p;
    p.setCommunicationModes(QBluetoothPermission::Access);
    // DEBT: Do a checkPermission too
    qGuiApp->requestPermission(
        p,
        this,
        [=](const QPermission&){});
#endif

    qDebug() << "Session::test1 phase 1";

    // NOTE: This is a blocking call
    auto discoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);

    qDebug() << "Session::test1 phase 2";

    discoveryAgent->setLowEnergyDiscoveryTimeout(25000);

    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this, &Session::addDevice);
    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::errorOccurred,
            this, &Session::deviceScanError);
    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished,
            this, &Session::deviceScanFinished);
    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::canceled,
            this, &Session::deviceScanFinished);
    discoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
}

void Session::addDevice(const QBluetoothDeviceInfo& info)
{
    QString name = info.name();

    if(name == "Pixel 7")
        investigate(info);

    name += " (" + QVariant::fromValue(info.majorDeviceClass()).toString() + ",";
    auto temp = QString::number(info.minorDeviceClass());
    name += temp + ",";
    name += QVariant::fromValue(info.serviceClasses()).toString();
    name += ")";

    //qDebug() << "addDevice" << info.name();
    if(names_.contains(name)) return;

    names_.append(name);
    emit namesChanged();
}


void Session::investigate(const QBluetoothDeviceInfo& info)
{
    // "Such an object essentially acts as a placeholder towards a remote Low Energy peripheral device"
    // [1.1]
    auto control = QLowEnergyController::createCentral(info);

    connect(control, &QLowEnergyController::serviceDiscovered,
            this, &Session::serviceDiscovered);
    connect(control, &QLowEnergyController::discoveryFinished,
            this, &Session::serviceScanDone);

    connect(control, &QLowEnergyController::errorOccurred, this,
            [this](QLowEnergyController::Error error) {
                Q_UNUSED(error);
                //setError("Cannot connect to remote device.");
            });

    connect(control, &QLowEnergyController::connected, this, [=]() {
        //setInfo("Controller connected. Search services...");
        control->discoverServices();
    });
    connect(control, &QLowEnergyController::disconnected, this, [=]() {
        //setError("LowEnergy controller disconnected");
    });

    // Connect
    control->connectToDevice();
}

void Session::serviceDiscovered(const QBluetoothUuid& gatt)
{
    qDebug() << "Session::serviceDiscovered" << gatt;

    if(gatt == QBluetoothUuid(QBluetoothUuid::ServiceClassUuid::DeviceInformation))
    {
        qDebug() << "Session::serviceDiscovered match";
    }

    // Look for 0x1855 that's telephone service

#if PGQT_62_PROTOBUF
    pgqt_62::CmdGetVersion cmd;
#endif
#if PGQT_62_COAP
    using H = embr::coap::Header;

    // DEBT: Use a span buf or a high level malloc'd buf here.  For the time being
    // a stringbuf has no specific problems other than if you forget to make it a
    // NON null terminated string, you'll have problems
    using streambuf_type = estd::layer1::basic_out_stringbuf<char, 128, false>;
    embr::coap::StreambufEncoder<streambuf_type> encoder;

    H header(H::NonConfirmable, H::Code::Get);

    encoder.header(header);
    // DEBT: finalize() continues to be a pain
    //encoder.finalize();

    //out.rdbuf()->str().data();
#endif
}

void Session::serviceScanDone() {}

void Session::deviceScanError() {}
void Session::deviceScanFinished() {}
