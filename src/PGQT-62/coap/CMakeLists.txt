cmake_minimum_required(VERSION 3.16)

project(PGQT-62-coap VERSION 0.1 LANGUAGES CXX)

include(FetchContent)

FetchContent_Declare(coap
  GIT_REPOSITORY https://github.com/malachi-iot/mc-coap.git
  SOURCE_SUBDIR src
  GIT_TAG v0.9.1
)

FetchContent_MakeAvailable(coap)

add_library(${PROJECT_NAME} INTERFACE)

target_compile_definitions(${PROJECT_NAME} INTERFACE PGQT_62_COAP=1)

target_link_libraries(${PROJECT_NAME} INTERFACE embr::coap)

add_library(PGQT-62::coap ALIAS ${PROJECT_NAME})
