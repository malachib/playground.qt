#pragma once

#include <QBluetoothDeviceInfo>
#include <QObject>

class Session : public QObject
{
    Q_OBJECT

    QStringList names_;

    Q_PROPERTY(QStringList names MEMBER names_ NOTIFY namesChanged)

public:
    Session(QObject* parent) : QObject(parent) {}

    Q_INVOKABLE void test1();

    void addDevice(const QBluetoothDeviceInfo& info);
    void deviceScanError();
    void deviceScanFinished();
    void investigate(const QBluetoothDeviceInfo&);
    void serviceDiscovered(const QBluetoothUuid& newService);
    void serviceScanDone();

signals:
    void namesChanged();
};
