# Overview

# Detail

## 1. Projects

### 1.1. App

Goal: to control tunable parameter provided in 1.2.
protobuf version is `proto3` since that's what demo app uses [5.2.1]

### 1.2. ESP-IDF

Goal: provide one tunable parameter to increase or decrease PWM on an LED
At least one of the following 3 is implemented:

#### 1.2.1. protocomm / protobuf

This is the preferred choice. as compared to1.3.
Would very much like "Security 1" [5.1] though no security is permissible for PGQT-62.

#### 1.2.2. CoAP

If 1.2.1. proves to be too much of a chore, we can attempt layering CoAP instead.  This is, as much as I love CoAP, not the preferred option.

#### 1.2.3. Raw

Using this is a starting point for BLE/GAP/GATT understanding

Relies on NimBLE (See section 4.5)
Uses esp-idf peripheral example [5.4.1] as a guide

If both 1.2.1 and 1.2.2 are too much of a rabbit hole, we can DIY the most basic of comms to achieve proof of concept success.  Some old partial examples may [6] exist

Findings in section 4.7 steer us away from CAN bus-esque messages, since
there's no easy standard to conform to plus that would complicate end-to-end
testing between this and section 1.1. App

## 2. Targets

### 2.1. Debian

* libqt6bluetooth6
* bluez
* libkf5bluezqt6
* protobuf-compiler
* libprotobuf-dev (maybe)

### 2.2. Windows

### 2.3. ESP32

### 2.4. Android

## 3. Troubleshooting

### 2.5. ChromeBook

We don't expect thngs to run well here.  This is merely a place to verify
builds and partial execution works.

### 3.1. Permissions

Unsurprisingly without extra care we get:

```
W qt.bluetooth.android: : Permissions not authorized for a specified mode: QFlags<QBluetoothPermission::CommunicationMode>(Access)
W qt.bluetooth.android: : Search not possible due to missing QBluetoothPermission::Access permission
```

To handle this, use `qGuiApp->requestPermissions` [4]

### 3.2. MTd_StaticDebug doesn't match MDd_DynamicDebug

Seeing tons of these during protobuf-lite linking:

```
error LNK2038: mismatch detected for 'RuntimeLibrary': value 'MTd_StaticDebug' doesn't match value 'MDd_DynamicDebug' in mocs_compilation.cpp.obj
```

This is likely an indication that we're incorrectly mixing and matching SHARED and STATIC
[7.10]

A little confusing because I thought I was specifying static everywhere, and that
`proto` which itself is a static lib.

### 3.3. ChromeBook Qt 6.4.2 non-display

Unsure why, but ChromeBook fails to render GUI quite right.  Specifically, title is gone.  

## 4. Observations

### 4.1. Permissions not at first

On initial run, app requests permissions for location & observing nearby devices.  This is expected
as per [4.3].  Noteworthy also is this technique is new since Qt 6.6 as is [4.1]

### 4.2. Protocomm

### 4.3. Protobuf

"Protocomm internally uses protobuf (protocol buffers) for secure session establishment" [5]
This is not a promise that it will continue to do so.  Being that we too can choose our own transport,
I'm hoping investing some into protobuf isn't a bad idea

Ironic, even though protobuf lib is all Google, it appears there's legwork to do to link in NDK [7.6], [7.6.1]
If we're lucky CMake-native build won't be hard [7.3.1].  CMake approach does seem to work but it's SLOW --
protobuf seems BIG!  Trying protobuf-lite

### 4.4. CoAP

Since Protobuf is a mega deep dive, diverted onto CoAP.  Some big thinking has gone on for BLE+CoAP [8] and suggests it's a decent fit.

It appears part of this analysis is to save bytes used by `mid`, indicating BLE
obviates the need.  As I write this I don't fully understand that.  But, for
proof of concepts, I am willing to burn up those 2 bytes out of an admittedly small 20 bytes.

### 4.5. ESP-IDF Bluetooth

This section focuses on observations ESP-IDF/ESP32 specific.  Otherwise, see
section 4.6.1.

Looks like NimBLE is gonna be our thing, it's said to be lighter than Bluedroid [5.3.2] and we're just fine doing only BLE.

Honorable mention is "BluFi" - which seems to be a tuned Bluetooth (is it BLE?) for credential provisioning.
Cool stuff, but outside the scope of this project.

#### 4.5.1. Example

Espressif's basic example [5.4.1] uses logging `MODLOG_DFLT` which itself comes
from Apache's mynewt [10.1] [10].  That meks perfect sense since NimBLE itself
comes from that project [5.4] [5.4.2]

### 4.6. BLE

BLE has a max size of 512 and a preferred size (MTU, I think) of 20
An interesting BLE advertising web decoder exists [2.4] (be sure to
strip it down to raw hex alphanums)

#### 4.6.1. Implementations

##### 4.6.1.1. NimBLE

Not trivial!!

#### 4.6.2. Characteristic

##### 4.6.2.1. Descriptor

Contained within a characteristic [2.5], [5.4.1]

##### 4.6.2.2. Property

Contained within a characteristic [2.5]

### 4.7. CAN bus

This is outside the scope of this project.  However, it's more than a curiousity
since this project feeds a larger goal which includes provisioning CAN bus devices.  Noteworthy is that there is no set BLE CAN transceiver spec [9].
I imagine most conform to the ELM327 AT command standard [9.1] so we may elect
to do so also

A quick search for ELM327 and esp-idf mostly hits on people consuming BLE,
not providing it.

### 4.8. embr service (on ESP32)

Looks like there's potential for a NimBLE embr service since it's very event
driven.  It may be a red herring though, because the subject-oberver pattern
may provide little vakue here.  Reason being I doubt there will be more than
one subscriber to the BLE events, thus vastly reducing the utility of the
subject-observer mechanism.

The only reason left to pursue an embr service would then be if it made it
easier to interact with the BLE service, which doesn't seem likely.

# Results

| Date | Project | Machine | Target | Qt Ver | Result | Notes |
| ---- |  ---- |  ----- |  ---- | ---- | --- | --- |
| 08JAN24 | app | C131-VM | Clang Android arm64 | Qt 6.6.1 | OK |
| 11JAN24 | app | ChromeBook | Clang arm64 | Qt 6.4.2 | Partial | See section 2.5.

# References

1. https://doc.qt.io/qt-6.5/qtbluetooth-index.html
    1. https://doc.qt.io/qt-6.5/qlowenergycontroller.html
2. https://btprodspecificationrefs.blob.core.windows.net/assigned-numbers/Assigned%20Number%20Types/Assigned_Numbers.pdf
    1. https://www.bluetooth.com/specifications/assigned-numbers/
    2. https://www.bluetooth.com/specifications/specs/
    3. https://stackoverflow.com/questions/38741928/what-is-the-length-limit-for-ble-characteristics
    4. https://reelyactive.github.io/advlib/
    5. https://novelbits.io/bluetooth-gatt-services-characteristics/
3. https://forum.qt.io/topic/140317/missing-bluez-and-bluetooth-dev-package/11
4. https://doc.qt.io/qt-6.5/qcoreapplication.html
    1. https://ekkesapps.wordpress.com/qt-6-qmake/qt-permissions-api-new/
    2. https://doc.qt.io/qt-6.5/permissions.html
    3. https://doc.qt.io/qt-6/qbluetoothpermission.html#details
5. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-reference/provisioning/protocomm.html
    1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-reference/provisioning/provisioning.html
    2. https://github.com/espressif/esp-idf-provisioning-android
        1. https://github.com/espressif/esp-idf-provisioning-android/blob/Provisioning_App_Release_2.1.2/provisioning/src/main/proto/wifi_config.proto
    3. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-reference/bluetooth/esp_gatts.html
        1. https://github.com/espressif/esp-idf/tree/v5.1.2/examples/bluetooth/bluedroid/ble/gatt_server_service_table
        2. https://www.esp32.com/viewtopic.php?t=16094
    4. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-reference/bluetooth/nimble/index.html
        1. https://github.com/espressif/esp-idf/tree/v5.1.2/examples/bluetooth/nimble/bleprph
        2. https://mynewt.apache.org/latest/network/index.html#ble-user-guide
    5. https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/blufi.html
6. https://medium.com/@shigmas/ble-on-the-esp32-and-qt-89e8fb23007e
7. https://protobuf.dev/getting-started/cpptutorial/
    1. https://github.com/protocolbuffers/protobuf
    2. https://cmake.org/cmake/help/v3.19/module/FindProtobuf.html
    3. https://github.com/protocolbuffers/protobuf/blob/v25.1/src/README.md
        1. https://github.com/protocolbuffers/protobuf/blob/v25.1/cmake/README.md
    4. https://stackoverflow.com/questions/67094240/use-protobuf-in-android-ndk-using-cmake
    5. https://stackoverflow.com/questions/60894674/android-qt-protobuf-compiling-protobuf-with-ndks-prebuilt-toolchains
    6. https://github.com/protocolbuffers/protobuf/issues/5279
        1. https://gist.github.com/ZhangMenghe/9210e1396a97a230e4d2a78b59bb97c1
    7. https://stackoverflow.com/questions/6405767/protocol-buffer-lite-versus-regular-protocol-buffer
    8. https://stackoverflow.com/questions/73208290/is-it-possible-to-include-protobuf-using-cmakes-fetchcontent
    9. https://stackoverflow.com/questions/32715225/cmakelist-to-generate-cc-and-h-files-from-proto-file-under-a-specific-folder
    10. https://stackoverflow.com/questions/14714877/mismatch-detected-for-runtimelibrary
8. https://datatracker.ietf.org/doc/draft-amsuess-core-coap-over-gatt/
    1. https://gitlab.com/chrysn/coap-over-gatt
9. https://stackoverflow.com/questions/52075456/which-gatt-profile-and-services-are-used-by-obd-ble-adapters-like-lelink-automa
    1. https://www.sparkfun.com/datasheets/Widgets/ELM327_AT_Commands.pdf
10. https://github.com/apache/mynewt-core/tree/master
    1. https://github.com/apache/mynewt-core/blob/1_9_0_dev/sys/log/modlog/include/modlog/modlog.h
    2. https://mynewt.apache.org/latest/network/ble_hs/ble_att.html