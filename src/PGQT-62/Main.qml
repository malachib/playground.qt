import QtQuick

import PGQT_62

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-62")

    ListView {
        model: Session.names
        anchors.fill: parent
        delegate: Text {
            font.pointSize: 12
            text: modelData
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "red"
        visible: false
    }
}
