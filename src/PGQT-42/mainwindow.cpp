#include <QDebug>
#include <QFileDialog>
#include <QMenuBar>
#include <QProgressBar>
#include <QPushButton>
#include <QStyle>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));

    fileMenu->addAction(tr("&Open"), this, &MainWindow::fileOpen);

    QVBoxLayout* vboxLayout = new QVBoxLayout(centralWidget());
    QHBoxLayout* buttonsLayout = new QHBoxLayout;

    vboxLayout->addWidget(&fileNameLabel);
    vboxLayout->addWidget(&progressBar);
    vboxLayout->addLayout(buttonsLayout);

    QPushButton* rewindButton = new QPushButton("rewind");
    playButton = new QPushButton("play");
    QPushButton* fastforwardButton = new QPushButton("fast forward");

    buttonsLayout->addWidget(rewindButton);
    buttonsLayout->addWidget(playButton);
    buttonsLayout->addWidget(fastforwardButton);

    connect(rewindButton, &QPushButton::clicked, this, &MainWindow::rewindPressed);
    connect(playButton, &QPushButton::clicked, this, &MainWindow::playPressed);
    connect(fastforwardButton, &QPushButton::clicked, this, &MainWindow::ffPressed);

    connect(&progressBar, &QProgressBar::valueChanged, this, &MainWindow::progressBarChanged);

    mediaPlayer.setAudioOutput(&audioOutput);

    connect(&mediaPlayer, &QMediaPlayer::mediaStatusChanged, this, &MainWindow::mediaStatusChanged);
    connect(&mediaPlayer, &QMediaPlayer::durationChanged, this, &MainWindow::mediaDurationChanged);
    connect(&mediaPlayer, &QMediaPlayer::positionChanged, this, &MainWindow::mediaPositionChanged);
    connect(&mediaPlayer, &QMediaPlayer::playbackStateChanged, this, &MainWindow::mediaPlaybackStateChanged);

    // DEBT: Although this suggestion from [4] works great, I do want to know how to handle the custom ones
    // a little better (same technique as card game app) - perhaps like [5] recommends?
    QIcon icon = style()->standardIcon(QStyle::SP_MediaSeekForward);
    fastforwardButton->setIcon(icon);
    icon = style()->standardIcon(QStyle::SP_MediaSeekBackward);
    rewindButton->setIcon(icon);
    icon = style()->standardIcon(QStyle::SP_MediaPlay);
    playButton->setIcon(icon);
}

void MainWindow::fileOpen()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Audio"), QString(), "Audio Files (*.wav, *.mp3)");

    // Exhibits annoying XCB error [2], [2.1], [2.2] - the general consensus is to ignore it

    fileNameLabel.setText("Loading...");

    QUrl fileUrl = QUrl::fromLocalFile(filename);

    mediaPlayer.setSource(fileUrl);
}

// Guidance from [1]

void MainWindow::rewindPressed()
{
    qint64 rw = mediaPlayer.position() - 5000;
    mediaPlayer.setPosition(rw);
}


void MainWindow::playPressed()
{
    if(mediaPlayer.playbackState() == QMediaPlayer::PlayingState)
        mediaPlayer.pause();
    else
    {
        mediaPlayer.play();
    }
}


void MainWindow::ffPressed()
{
    qint64 ff = mediaPlayer.position() + 5000;
    mediaPlayer.setPosition(ff);
}


void MainWindow::progressBarChanged(int value)
{
    // NOTE: Keep a close eye on this, cyclic/infinite signaling is a risk.
    // DEBT: It seems to be handling this OK, but I would prefer to not emit
    // progress bar changed messages when we manually set it during
    // mediaPositionChanged
    mediaPlayer.setPosition(value);
}


void MainWindow::mediaPositionChanged(qint64 value)
{
    progressBar.setValue(value);
}


void MainWindow::mediaDurationChanged(qint64 value)
{
    progressBar.setMaximum(value);
}


void MainWindow::mediaStatusChanged(QMediaPlayer::MediaStatus mediaStatus)
{
    switch(mediaStatus)
    {
        case QMediaPlayer::LoadedMedia:
        {
            // NOTE: [4] Section 5 strongly suggests one can interrogate duration()
            // property here, but that is not the case.  One must hook the duration changed
            // signal as [1.1] 'duration' documentation indicates
            progressBar.setValue(0);
            fileNameLabel.setText(mediaPlayer.source().fileName());
            break;
        }

        case QMediaPlayer::EndOfMedia:
            qDebug() << "mediaStatusChanged - EndOfMedia";
            break;

        /* Doesn't help
        case QMediaPlayer::BufferedMedia:
        {
            int duration = mediaPlayer.duration();
            qDebug() << "mediaStatusChanged - Buffered - duration=" << duration;
            break;
        }
        */

        default:
            break;
    }
}


void MainWindow::mediaPlaybackStateChanged(QMediaPlayer::PlaybackState p)
{
    switch(p)
    {
        case QMediaPlayer::PlayingState:
            playButton->setText("Stop");
            break;

        case QMediaPlayer::StoppedState:
        case QMediaPlayer::PausedState:
            playButton->setText("Play");
            break;
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

