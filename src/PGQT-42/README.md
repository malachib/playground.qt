# References

1. https://doc.qt.io/qt-6.2/audiooverview.html
   1. https://doc.qt.io/qt-6.2/qmediaplayer.html
2. https://forum.qt.io/topic/132363/qfiledialog-prints-bad-window-warning/2
   1. https://bugreports.qt.io/plugins/servlet/mobile#issue/QTBUG-87141
   2. https://bugreports.qt.io/browse/QTBUG-56893
3. https://stackoverflow.com/questions/43156906/qmediaplayer-duration-returns-0-always
4. "Audio Player.pdf" (CS 335 Excercise 8) assignment
5. https://stackoverflow.com/questions/29027105/how-can-i-use-a-sp-icon-in-a-custom-stylesheet
