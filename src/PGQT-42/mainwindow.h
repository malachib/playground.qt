#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAudioOutput>
#include <QLabel>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QProgressBar>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QMediaPlayer mediaPlayer;
    QAudioOutput audioOutput;
    QProgressBar progressBar;
    QLabel fileNameLabel;
    QPushButton* playButton;

private slots:
    void fileOpen();
    void rewindPressed();
    void playPressed();
    void ffPressed();

    void progressBarChanged(int value);

    void mediaPlaybackStateChanged(QMediaPlayer::PlaybackState);
    void mediaDurationChanged(qint64 value);
    void mediaPositionChanged(qint64 value);
    void mediaStatusChanged(QMediaPlayer::MediaStatus);

};
#endif // MAINWINDOW_H
