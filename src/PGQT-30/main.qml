import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-30")

    RowLayout {
        Button {
            text: "poc1"
            onClicked: {
                var component = Qt.createComponent("poc1.qml");
                var window = component.createObject(root)
                window.show();
            }
        }

        Button {
            text: "poc2"
            onClicked: {
                var component = Qt.createComponent("poc2.qml");
                var window = component.createObject(root)
                window.show();
            }
        }

        Button {
            text: "poc3"
            onClicked: {
                var component = Qt.createComponent("poc3.qml");
                var window = component.createObject(root)
                window.show();
            }
        }
    }
}
