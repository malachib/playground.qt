import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

// Initial guidance from
// https://doc.qt.io/qt-6/qml-qtquick-layouts-rowlayout.html

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("poc1")

    RowLayout {
        id: layout
        anchors.fill: parent
        spacing: 6

        Rectangle {
            id: tealRect
            color: 'teal'
            Layout.fillWidth: true
            Layout.minimumWidth: 50
            Layout.preferredWidth: 100
            Layout.maximumWidth: 300
            Layout.minimumHeight: 150

            Text {
                anchors.centerIn: parent
                text: parent.width + 'x' + parent.height
            }
        }
        Rectangle {
            id: plumRect
            color: 'plum'
            Layout.fillWidth: true
            Layout.minimumWidth: 100
            Layout.preferredWidth: 200
            Layout.preferredHeight: 100

            Text {
                anchors.centerIn: parent
                text: parent.width + 'x' + parent.height
            }

            ListView {
                // Presumably due to its delegate, ListView width somewhat resolves itself.
                // Height, however, only renders one element unless we expressly state a different
                // height.  anchoring to parent's bottom achieves this in this case
                anchors.fill: parent

                model: model
                delegate: del
            }
        }

        ListView {
            model: model

            Layout.fillWidth: true
            Layout.minimumWidth: 50
            Layout.maximumWidth: 150

            anchors.top: tealRect.top
            anchors.bottom: tealRect.bottom


            delegate: del
        }
    }


    ListModel {
        id: model

        ListElement {
            key: 1
            value: "One"
        }

        ListElement {
            key: 2
            value: "Two"
        }
    }

    Component {
        id: del
        RowLayout {
            id: delRow

            Text {
                Layout.fillWidth: true
                Layout.preferredWidth: 20
                Layout.minimumWidth: 10

                text: key
            }

            Text {
                Layout.fillWidth: true
                Layout.preferredWidth: 50
                Layout.minimumWidth: 40

                text: value
            }
        }
    }
}
