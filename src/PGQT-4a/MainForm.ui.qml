import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    width: 640
    height: 480

    property alias button1: button1
    property alias button2: button2

    // copy and pasting this from https://github.com/lemirep/QtQuickCarGauges/blob/master/qml/main.qml
    RoundGauge
    {
        anchors
        {
            left : parent.left
            right : parent.horizontalCenter
            top : parent.top
            bottom : parent.verticalCenter
        }
        //            outerCirclingColor: "#cc6600"
        /*
        textFont.family : "Helvetica"
        textFont.bold : true
        textFont.italic : true*/
        digitalFont.family : "Helvetica"
        digitalFont.bold : true
        digitalFont.italic : true

        //            textFont.pointSize : 12

        unit: "km/h"
        unitFont.pointSize: 12
        unitFont.bold: true
        unitFont.italic: true
        unitFont.family: "Helvetica"
        fullCircle: true
        subDivs: 33
        minValue: 0
        maxValue: 340
        lowValues: 30
        highValues: 220
        currentValue: parent.randVal *  (maxValue - minValue) + minValue;
        digitalFont.pointSize: 15
    }

    RowLayout {
        anchors.centerIn: parent

        Button {
            id: button1
            text: qsTr("Press Me 1")
        }

        Button {
            id: button2
            text: qsTr("Press Me 2")
        }
    }
}
