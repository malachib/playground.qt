#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "someclass.h"

int main(int argc, char *argv[])
{
    SomeClass sc;
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    const QQmlContext* context = engine.rootContext();

    // need from viewer ala http://stackoverflow.com/questions/21437841/how-to-connect-a-qt-quick-button-click-to-a-c-method
    //context->setContextProperty(QStringLiteral("_someObject"), &sc);

    return app.exec();
}
