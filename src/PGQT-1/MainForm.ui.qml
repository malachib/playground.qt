import QtQuick 2.5
import QtQuick.Controls 1.4

Rectangle {
    property alias mouseArea: mouseArea

    width: 360
    height: 360

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        Button {
            id: button1
            x: 175
            y: 207
            text: qsTr("Button")
            action: none.none
        }
    }

    Text {
        anchors.centerIn: parent
        text: "Hello World"
    }
}
