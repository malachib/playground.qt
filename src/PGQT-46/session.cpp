#include "session.h"

using namespace pgqt_46;

Session::Session(QObject* parent) : QObject(parent)
{
    connect(this, &Session::counterChanged, this, &Session::onCounterChanged);
}


QString Session::name() const
{
    return name_ + ": " + QString::number(counter_);
}


void Session::onCounterChanged()
{
    emit nameChanged();
}
