# Overview

To see problem, click one '+' button, then click the other.

Observe that one of the two counters stops updating

Using binding diagnostic technique described at [1], namely:

```	
export QT_LOGGING_RULES="qt.qml.binding.removal.info=true"
```

Setting this via the run configuration in Qt Creator works also

## Binging QML element

[3] and [2] "Edit 1" presents a similar but simpler
scneario which works because no imperatives are present to break the binding
(the plumbing of textChanged initiates the update rather than an =)

# References

1. https://www.kdab.com/new-qt-5-10-diagnostics-breaking-qml-bindings/
2. https://stackoverflow.com/questions/41232999/two-way-binding-c-model-in-qml
3. http://imaginativethinking.ca/bi-directional-data-binding-qt-quick/