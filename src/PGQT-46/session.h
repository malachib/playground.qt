#pragma once

#include <QObject>

namespace pgqt_46 {

class Session : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(int counter MEMBER counter_ NOTIFY counterChanged)

    QString name_ = "session1";
    int counter_ = 0;

private slots:
    void onCounterChanged();

public:
    explicit Session(QObject* parent = nullptr);

    void setName(QString n);
    QString name() const;

signals:
    void nameChanged();
    void counterChanged();
};

}
