import QtQuick 2.15
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.15

Window {
    width: 320
    height: 240
    visible: true
    title: qsTr("PGQT-46")

    component Demo: RowLayout {
        id: demo

        property string name;

        property int counter;

        signal clicked()

        Text {
            text: demo.name
        }

        TextField {
            id: textInput1
        }

        Text {
            text: "counter: " + demo.counter
        }

        Button {
            text: "+"

            font.pointSize: 48

            // This line in particular generates a binding overwrite, because
            // specifically assigning to an already-bound property does break the binding
            // and a ++ is very much an assignment
            //onClicked: ++demo.counter

            // Therefore, we must not attempt a reverse binding in this scenario, even though
            // it halfway works.  Instead, a emit a raw signal and let component consumer do
            // the heavy lifting
            onClicked: parent.clicked()
        }
    }

    component Demo2: Demo {
        id: root

        onClicked: session.counter = counter + 1

        Binding {
            when: session
            root.name: session.name
            root.counter: session.counter
        }
    }

    ColumnLayout {

        Demo2 { }

        Demo {
            id: demo2
            name: session ? session.name : ""

            counter: session ? session.counter : 0
            onClicked: session.counter = counter + 1
        }

    }

    /* technically these work well enough, but since demo.counter++ breaks things,
       the connection to these bindings also breaks.
       See README "Binding QML element" section
    Binding {
        when: session
        target: session
        property: "counter"
        value: demo1.counter
    }


    Binding {
        when: session
        target: session
        property: "counter"
        value: demo2.counter
    } */
}
