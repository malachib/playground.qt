# Observations

If we can fade so smoothly, what the heck is that flicker/redraw from Windows visibility then?

Do a stage 2 in here where we try flipping smoothly over to QML because then we can do some
more fun animations than just a fade (though I like the fade)