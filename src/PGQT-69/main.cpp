#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTimer>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/PGQT-69/Main.qml"_qs);
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection
    );

    QObject::connect(
        &app,
        &QGuiApplication::applicationStateChanged,
        [=]
        {
            // DEBT: Unlike Tracelet Monitor, there's a slight pulsating here where it gets a little darker
            QTimer::singleShot(1250, [=]
            {
                QNativeInterface::QAndroidApplication::hideSplashScreen(1250);
            });
        });

    engine.load(url);

    // Whoa! It works!
    QNativeInterface::QAndroidApplication::hideSplashScreen(5000);

    return app.exec();
}
