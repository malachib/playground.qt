import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import PGQT_31

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-31")

    // NOTE: Putting this in RowLayout does not work - Buttons can't see it
    function showWindow(s)
    {
        var component = Qt.createComponent(s + ".qml");
        var window = component.createObject(root)
        window.show();
    }

    RowLayout {

        Button {
            text: "poc1"
            onClicked: {
                var component = Qt.createComponent("poc1.qml");
                var window = component.createObject(root)
                window.show();
            }
        }

        Button {
            text: "poc2"
            onClicked: {
                var component = Qt.createComponent("poc2.qml");
                var window = component.createObject(root)
                window.show();
            }
        }

        Button {
            text: "poc3"
            onClicked: showWindow("poc3")
        }

        Button {
            text: "poc4"
            onClicked: showWindow("poc4")
        }

        Button {
            text: "poc5"
            onClicked: showWindow("poc6")
        }

        Button {
            text: "poc6"
            onClicked: showWindow("poc6")
        }
    }

}
