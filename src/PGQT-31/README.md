# Overview

Document v0.1

## 1. Proof of Concepts
### POC1

### POC2

Nearly simplest model approach conceivable.
Does work, if you can count not using any kind of list or even the word
'model' working

### POC3

Like POC1, but with an inline delegate to alleviate the headache of getting
at ListView accessible properties

### POC4

Like POC1, but with C++ provided model

### POC5

Combo of POC1 and POC2 - maybe an entity inside the model elements
will behave better.

### POC6

TableView

## 2. Guidance

### 2.1. Writeback binding

"Note: If a model role is bound to a required property, assigning to that property will not modify the model. It will instead break the binding to the model (just like assigning to any other property breaks existing bindings). If you want to use required properties and change the model data, make model also a required property and assign to model.propertyName." - https://doc.qt.io/qt-6/qtquick-modelviewsdata-modelview.html#changing-model-data