/**
  * References:
  *
  * 1. README.md v0.1
  */
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import PGQT_31

Window {

    width: 640
    height: 480
    visible: true
    title: qsTr("POC-1")

    SyntheticData {
        id: model1
    }

    Component {
        id: del

        RowLayout {

            required property int index
            required property string val1

            // Required for writeback to perform correctly without breaking the binding.
            // See [1] Section 2.1
            // DEBT: I'd like to do better than a variant if we can.
            // ListElement and ListModel both end up being null (invalid cast)
            required property variant model;

            Text {

                Layout.preferredWidth: 50

                text: index
            }

            TextField {
                id: editor

                Layout.preferredWidth: 100

                text: val1

                onTextChanged: model.val1 = text

                Binding {
                    // "Cannot read property 'model' of null"
                    //target: ListView.view.model[index]
                    property: "val1"
                    value: editor.text
                }

                //onTextChange: val1 = text
            }

        }
    }

    ColumnLayout {
        ListView {
            Layout.fillHeight: true
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignTop

            delegate: del
            model: model1
        }

        ListView {
            id: debugListView

            Layout.fillHeight: true
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignTop

            delegate: Text {
                text: val1
            }

            model: model1
        }
    }

}
