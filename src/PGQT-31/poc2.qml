import QtQuick 2.0
import QtQuick.Controls
import QtQuick.Layouts

// Adapted from
// https://stackoverflow.com/questions/41232999/two-way-binding-c-model-in-qml

Window {

    width: 640
    height: 480
    visible: true
    title: qsTr("POC-2")

    Item {
        id: testMessage

        required property int val1
        required property string val2

        val1: 0
        val2: "test"
    }

    RowLayout {

        TextField {
            id: editor

            Layout.preferredHeight: 100

            //Binding model -> view
            text: testMessage.val2

            //Binding model <- view
            Binding {
                target: testMessage
                property: "val2"
                value: editor.text
            }
        }

        Label {
            id: display

            Layout.preferredHeight: 100

            //Binding model -> view
            text: testMessage.val2
        }

    }
}
