cmake_minimum_required(VERSION 3.16)

project(PGQT-31 VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 6.2 COMPONENTS Quick REQUIRED)

# See PGQT-28
set(QML_IMPORT_PATH ${CMAKE_SOURCE_DIR}/qml ${CMAKE_BINARY_DIR}/imports CACHE STRING "" FORCE)

qt_add_executable(appPGQT-31
    main.cpp
)

qt_add_qml_module(appPGQT-31
    URI PGQT_31
    VERSION 1.0
    QML_FILES main.qml poc1.qml poc2.qml poc3.qml poc4.qml poc5.qml poc6.qml SyntheticData.qml
    SOURCES writeablemodel.cpp writeablemodel.h
)

set_target_properties(appPGQT-31 PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

target_link_libraries(appPGQT-31
    PRIVATE Qt6::Quick)

install(TARGETS appPGQT-31
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
