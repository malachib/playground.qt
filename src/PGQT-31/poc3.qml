import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import PGQT_31

Window {

    width: 640
    height: 480
    visible: true
    title: qsTr("POC-3")


    SyntheticData {
        id: model1
    }

    ColumnLayout {
        ListView {
            id: listView

            Layout.fillHeight: true
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignTop

            delegate: TextField {

                id: editor

                required property int index
                required property string val1
                // DEBT: See comments in POC1
                required property variant model;

                text: val1

                onTextChanged: model.val1 = text
                Keys.onReturnPressed: model.val1 = text

                Binding {
                    // "Unable to assign [undefined] to QObject*"
                    //target: model1[index]
                    // "Cannot read property 'model' of null"
                    //target: ListView.view.model[index]

                    property: "val1"
                    value: editor.text

                    // Following make no difference/generate errors:

                    //editor.text: val1
                    //ListView.view.model.val1: editor.text

                    //when: ListView.isCurrentItem
                }
            }

            model: model1
        }

        ListView {
            id: debugListView

            Layout.fillHeight: true
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignTop

            delegate: Text {
                text: val1
            }

            model: model1
        }
    }

}
