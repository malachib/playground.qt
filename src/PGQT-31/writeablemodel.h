#ifndef WRITEABLEMODEL_H
#define WRITEABLEMODEL_H

#include <QObject>
#include <qqml.h>
#include <QAbstractListModel>

class WriteableModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit WriteableModel(QObject *parent = nullptr);

signals:

};

#endif // WRITEABLEMODEL_H
