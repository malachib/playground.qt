import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    // Overlap code pulled from
    // https://gist.github.com/vannell/c9c452e43eadf4f2e087
    // To eventually use to compute "under" layer of two intersecting rectangles (top and bottom)
    property int xOverlap: 0
    property int yOverlap: 0
    property int xOverlapWidth: 0
    property int yOverlapHeight: 0

    function computeOverlap1D(b, t, b_size, t_size)
    {
        var result = {
            min: 0,
            size: 0
        }

        if(b < t)
        {
            if(b + b_size < t)
            {
                // We have no x overlap here
            }
            else
            {
                // overlap starts right at t.x if b.width spills over
                result.min = t;

                // complete horizontal coverage?
                if(b + b_size > t + t_size)
                {
                    result.size = t_size;
                }
                else
                {
                    // not complete, so calculate how much b.width
                    // spills over
                    result.size = (b + b_size) - t;

                }
            }
        }
        else
        {
            if(b > t + t_size)
            {
                // We have no x overlap here
            }
            else
            {
                result.min = b;

                // complete (remainder) horizontal coverage?
                if(b + b_size > t + t_size)
                {
                    result.size = (t + t_size) - b;
                }
                else
                {
                    // not complete, so entire b width is
                    // encapsulated here
                    result.min = b;
                }
            }
        }

        return result;
    }

    function computeOverlap(bottom, top) {
            var b = bottom.mapToItem(null, 0, 0, bottom.width, bottom.height)
            var t = top.mapToItem(null, 0, 0, top.width, top.height)

            var result = computeOverlap1D(b.x, t.x, b.width, t.width);

            xOverlap = result.min;
            xOverlapWidth = result.size;

            var result = computeOverlap1D(b.y, t.y, b.height, t.height);

            yOverlap = result.min;
            yOverlapHeight = result.size;

            return;
            // for our purposes, overlap always begins with top rectange x,y origin
            // we just then see how deep in the bottom overlaps
            if(b.x < t.x)
            {
                if(b.x + b.width < t.x)
                {
                    // We have no x overlap here
                }
                else
                {
                    // overlap starts right at t.x if b.width spills over
                    xOverlap = t.x;

                    // complete horizontal coverage?
                    if(b.x + b.width > t.x + t.width)
                    {
                        xOverlapWidth = t.width;
                    }
                    else
                    {
                        // not complete, so calculate how much b.width
                        // spills over
                        xOverlapWidth = (b.x + b.width) - t.x;
                    }
                }
            }
            else
            {
                if(b.x > t.x + t.width)
                {
                    // We have no x overlap here
                }
                else
                {
                    xOverlap = b.x;

                    // complete (remainder) horizontal coverage?
                    if(b.x + b.width > t.x + t.width)
                    {
                        xOverlapWidth = (t.x + t.width) - b.x;
                    }
                    else
                    {
                        // not complete, so entire b width is
                        // encapsulated here
                        xOverlapWidth = b.width;
                    }
                }
            }

            if(b.y < t.y)
            {
                if(b.y + b.height < t.y)
                {
                    // We have no x overlap here
                }
                else
                {
                    // overlap starts right at t.x if b.width spills over
                    yOverlap = t.y;
                }
            }
            else
            {
                if(b.y > t.y + t.height)
                {
                    // We have no x overlap here
                }
                else
                {
                    yOverlap = b.y;
                }
            }

            //xOverlap = Math.min(r1.x + r1.width, r2.x + r2.width) - Math.max(r1.x, r2.x);
            //yOverlap = Math.min(r1.y + r1.height, r2.y + r2.height) - Math.max(r1.y, r2.y);
            //xOverlap = Math.min(r1.x, r2.x);
            //yOverlap = Math.min(r1.y, r2.y);
        }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1 {
        }

        Page {

            id: test_page

            MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    onClicked: test_rectangle2.state == 'clicked' ? test_rectangle2.state = "" : test_rectangle2.state = 'clicked';
                }


            Rectangle {
                //anchors.centerIn: parent
                x: test_rectangle.x - 40
                y: 50
                width: 600
                height: 200
                id: test_rectangle2
                opacity: 0.5
                color: "yellow"

                Label {
                    id: test_label2
                    font.pointSize: 40
                    text: qsTr("Blah")
                    anchors.left: test_rectangle
                    y: 0
                    //visible: true
                    padding: 50
                }

                onYChanged: computeOverlap(test_rectangle2, test_rectangle)

                states: [
                    State {
                        name: "moved"
                        PropertyChanges { target: test_rectangle2; x: 50 }
                    },
                    State {
                        name: "clicked"
                        PropertyChanges
                        {
                            target: test_rectangle2; y: 200; color: "orange"
                            //visible: false
                        }
                    }
                ]

                transitions: Transition {
                    PropertyAnimation { properties: "x,y"; easing.type: Easing.InOutQuad; duration: 1000 }
                }


                visible: false
            }

            // OK this isn't gonna work because it's not "sourcing anything"
            // we instead have to somehow carve out a rectangle on the existing one and blur it
            /*
            Rectangle {
                id: blur_rect
               x: xOverlap
               y: yOverlap
               opacity: 0.1
               width: xOverlapWidth
               height: yOverlapHeight
               color: "green"
               visible: false
            } */

            FastBlur {
                id: id_blur_3
                // the overlaps are "working", but they end up rescaling the original giving it
                // a squishy look
                /*
                x: xOverlap
                y: yOverlap
                width: xOverlapWidth
                height: yOverlapHeight */
                anchors.fill: test_rectangle2
                source: test_rectangle2
                radius: 20
                //opacity: 0.6
                visible: false
            }

            Item {
                id: rect_stencil

                //anchors.fill: test_rectangle2
                width: test_rectangle2.width
                height: test_rectangle2.height

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: -1

                    color: "black"
                }

                //color: "transparent"

                Rectangle {

                    color: "lightblue"
                    //anchors.left: test_page.left

                    x: xOverlap
                    y: yOverlap
                    width: xOverlapWidth
                    height: yOverlapHeight

                }

                visible: true
            }

            OpacityMask {
                id: rect_opac
                anchors.fill: rect_stencil
                source: test_rectangle2
                maskSource: rect_stencil
                visible: false
            }

            /*
            FastBlur {
                id: id_blur_4
                anchors.fill: rect_opac
                source: rect_opac
                radius: 20
            } */




            Rectangle {
                anchors.centerIn: parent
                width: 500
                height: 200
                id: test_rectangle
                opacity: 0.5
                color: "steelblue"
                Label {
                    id: test_label
                    font.pointSize: 40
                    text: qsTr("Second page")
                    anchors.centerIn: parent
                    visible: false
                    padding: 50
                }
                //visible: false
                FastBlur {
                    id: id_blur
                    anchors.fill: test_label
                    source: test_label
                    radius: 32
                    SequentialAnimation on radius  {
                            loops: Animation.Infinite
                            PropertyAnimation
                            {
                                to: 50
                                duration: 2000
                            }
                            PropertyAnimation {
                                to: 0
                                duration: 1000
                            }
                    }
                }
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("First")
        }
        TabButton {
            text: qsTr("Second")
        }
    }
}
