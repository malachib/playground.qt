#!/bin/sh

# As per [1]

pushd ext/libcurl

# [1] guidance on CROSS_COMPILE requires gcc, but we are using llvm
# whose prefixes really aren't quite the same
export CROSS_COMPILE=TEST

./configure --host=${CROSS_COMPILE} --with-ssl --with-zlib --disable-ftp --disable-gopher --disable-file --disable-imap --disable-ldap --disable-ldaps --disable-pop3 --disable-proxy --disable-rtsp --disable-smtp --disable-telnet --disable-tftp --without-gnutls --without-libidn --without-librtmp --disable-dict