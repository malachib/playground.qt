#!/bin/bash

mkdir -p ext

pushd ext

# TODO: Figure out which --branch to really grab android_openssl from
# unfortunately they didn't use git-lfs, and neither did openssl_for_ios script
# so those repos are big fat and slow to pull down

git clone --depth 10 --branch curl-8_4_0 https://github.com/curl/curl
git clone --depth 10  https://github.com/KDAB/android_openssl
git clone --depth 1 --branch v1.3 https://github.com/madler/zlib.git
git clone --depth 1 --branch OpenSSL_1_1_1u https://github.com/openssl/openssl.git
#git clone https://github.com/leenjewel/openssl_for_ios_and_android.git

#cd openssl_for_ios_and_android
#git reset --hard aae1672