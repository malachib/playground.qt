#!/bin/sh

# DEBT: Very c131-debian specific

# DEBT: these versions arbitrarily chosen
export ANDROID_VERSION=31
export NDK_VERSION=25.1.8937393

export NDK_PATH=$HOME/Android/Sdk/ndk/$NDK_VERSION

# llvm specific below this line
export COMPILE_PREFIX=aarch64-linux-android${ANDROID_VERSION}-clang
export NDK_TOOLCHAIN_PATH=$NDK_PATH/toolchains/llvm/prebuilt/linux-x86_64/bin
#export PATH=$PATH:

# 30NOV23 Guidance from [1], seems to compile zlib
export CC=${NDK_TOOLCHAIN_PATH}/${COMPILE_PREFIX}
export CPP=${NDK_TOOLCHAIN_PATH}/${COMPILE_PREFIX}++
export AR=${NDK_TOOLCHAIN_PATH}/llvm-ar
# Unverified below this line
export AS=${NDK_TOOLCHAIN_PATH}/llvm-as
export RANLIB=${NDK_TOOLCHAIN_PATH}/llvm-ranlib