# Overview

## 1. RESERVED

## 2. Explorations

### 2.1. Link [1] - cross compiling manually

Starting by explicitly making a build script to follow his lead.
Adjusting to pull from github instead of their sites

## 3. Observations / Questions

### 3.1. NDK location (c131-debian)

Q: It doesn't look like NDK is in the Qt folder at all.  Is it using the one(s) at ~/Android/Sdk/ndk?
A: Yes, it appears multiple ndk versions are hosted there

### 3.2. OpenSSL location

Link [1] tells us to explicitly grab OpenSSL.  However, casual glance on c131-debian
indicates Qt has already partially set up OpenSSL 1.1.x at `~/Android/android_openssl`

I'm thinking we probably should use the Qt provided one to avoid linker conflicts - yet, something feels off about all this

Looks like this comes from [5.1.1]

### 3.3. [3.1] build script observations

### 3.4. [5.1] Qt official OpenSSL support

This and [5.1.1] look very promising.  In particular, there's guideance
for consuming OpenSSL right from CMake.  That's nifty, though we will
see if that has any bearing on compiling libcurl.

### 3.5. NDK location (Windows)

This remains a mystery

# References

1. https://gist.github.com/bertrandmartel/d964444f4a85c2598053
    1. https://github.com/madler/zlib
2. https://github.com/ibaoger/libcurl-android
3. https://stackoverflow.com/questions/52375753/compiling-libcurl-for-android-64
    1. https://github.com/leenjewel/openssl_for_ios_and_android
4. https://stackoverflow.com/questions/11330180/porting-libcurl-on-android-with-ssl-support
5. https://doc.qt.io/qt-6/android-openssl-support.html
    1. https://github.com/KDAB/android_openssl
6. https://wiki.openssl.org/index.php/Android
