import QtQuick

import QtCore

// Be advised, 'Settings' QML has no associated UI element

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Settings {
        category: "Core UI"
        // Remember what position user placed Window at
        property alias x: root.x
        property alias y: root.y
    }
}
