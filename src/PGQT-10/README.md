# Overview

## Legacy

Format will need some conversion to run under Qt6, as per
https://blog.basyskom.com/2021/porting-a-qt-5-application-to-qt-6/

This is an extremely old project, so starting a new one in line with PGQT-7

## spir-v

Attempting to import [2.1]
Hitting errors in part because [2.1] has some uniform values we haven't set yet,
such as `iResolution` as mentioned in [4]

Attempted [5.1] but that happens at runtime and we're getting a compile error

"Vulkan-style GLSL has no separate uniform variables. Instead, shaders must always use a uniform block with a binding point of 0." [9]

There's a Qt5 flavor of what we're trying to do at [8] and specifically TShaderToy.qml.  That one relies on raw GLSL rather than a compiled QBS file, however.

We are trolled hilariously by [12].  However, [12.1] says "Variable naming in Qt Quick Effect Maker is in most cases compatible with Shadertoy, making it easy to port Shadertoy shaders to Qt Quick Effect Maker" - that is promising

### TODO

#### 1. Understand how to feed "uniform" variables such as `iResolution`

[6] gives us some clues, though we kinda knew this from [4]
Qt docs mentioned a block 0, which seems to be similar to "uniform buffer objects" as per [7]

#### 2. Isolate and address Y-axis flip

#### 3. Learn how to do relative vs absolute coordinate spaces

Specifically sometimes it seems like we can draw in relative positions to the source,
where other times it seems we must draw "on top" of source. [15] is technical but may help
me here.

#### 4. Reach 60 fps on Debian VM

A lot of optimization may be needed for this, and perhaps Debian VM cannot reach that
speed.

I am hoping to optimize shaders by bypassing ShaderEffectSource - [20] implies that one is expected to combine shaders into one big fragment.  Yet, PGQT-7's source material doesn't do that and clearly it "works" if not paying a performance penalty. [21] corroborates the
"single glsl" notion.

#### 5. Do video also

Trying to do MP4.  Failing semi silently.  Seeing

```
Available HW decoding frameworks:
     dxva2
     d3d11va
```

Which [21] seems to be talking about.  No video playback of any kind

## Findings

### GL

Stumbled on this in help for QQEM [12.1].  [14] has some details too.

| type | name | description |
| ----- | ----- | -------------------------- |
| float | iTime | Animated time in seconds. |
| int |  iFrame | Animated frame number. |

Critically, "qt_TexCoord0.x == 0 is at the left-most side of the input and qt_TexCoord0.x == 1 is at the right-most side." [5.3]

Also, somehow Y is flipped during 'glitch' fragment processing.
Also, some bizzarre origin behavior, things are not drawn from the origin point
I expect

### Nifty Overview

[18]

### Platforms

#### wasm

Looks like it WANTS to work, but familiar qrc/resource not found issue MAY be
inhibiting it

#### Android

Compiles.  We get:

```
I AdrenoGLES-0: Error: shader version mismatch
W libappspir-v_arm64-v8a.so: Failed to link shader program: Error: shader version mismatch
04-24 18:04:31.174 11215 11270 W libappspir-v_arm64-v8a.so:
W libappspir-v_arm64-v8a.so: Failed to build graphics pipeline state
```

Almost definitely because of OpenGL 3.x specificity.  OpenGL 3.0 on Android has
been supported for years [14]

#### macOS

Kinda-sorta fired up one time.
Shader was glitching and vertically wrapping pixels for "burke science"
Uncle.mp4 somehow fails to load
~~Font also fails to load~~

Subsequently, app crashes on startup.  Presumably this is some glitch
in the shader treatment, but I am not sure.  Debugging sends us straight
to assembly code on QSGRenderThread crashing in what looks like macOS
specific code.

# References

1. PGQT-7 spir-v
2. https://www.shadertoy.com/
    1. https://www.shadertoy.com/view/wld3WN
    2. https://www.shadertoy.com/view/4sfSz7
    3. https://www.shadertoy.com/howto
    4. https://www.shadertoy.com/view/tsccWr
    5. https://gist.github.com/markknol/d06c0167c75ab5c6720fe9083e4319e1
    6. https://www.shadertoy.com/view/ldXGW4
3. https://stackoverflow.com/questions/28540290/why-it-is-necessary-to-set-precision-for-the-fragment-shader
4. https://stackoverflow.com/questions/27888323/what-does-iresolution-mean-in-a-shader
5. https://doc.qt.io/qt-6/qopenglshaderprogram.html
    1. https://doc.qt.io/qt-6/qml-qtquick3d-setuniformvalue.html
    2. https://doc.qt.io/qt-6/qtshadertools-build.html
    3. https://www.qt.io/blog/in-depth-custom-shader-effects
    4. https://doc.qt.io/qt-6/qml-qtquick-uniformanimator.html
    5. https://doc.qt.io/qt-6/qtquick-performance.html
6. https://gist.github.com/aras-p/d40c430330506f0c9f5e83915b6b0725
7. https://learnopengl.com/Advanced-OpenGL/Advanced-GLSL
8. https://github.com/jaredtao/TaoShaderToy
9. https://doc.qt.io/qt-6/qml-qtquick-shadereffect.html
10. https://www.oreilly.com/library/view/opengl-programming-guide/9780132748445/app09lev1sec2.html
11. https://www.lighthouse3d.com/tutorials/glsl-tutorial/uniform-blocks/
12. https://doc.qt.io/qt-6/qqem-porting-shadertoy.html
    1. https://doc.qt.io/qt-6/qtquickeffectmaker-index.html
13. https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_on_the_web/GLSL_Shaders
14. https://forum.qt.io/topic/57631/opengl-es-3-0-with-qt-quick-on-android/6
15. https://www.khronos.org/opengl/wiki/Fragment_Shader
    1. https://registry.khronos.org/OpenGL-Refpages/gl4/html/gl_FragCoord.xhtml
    2. https://registry.khronos.org/OpenGL-Refpages/gl4/html/gl_Position.xhtml
16. https://www.dafont.com/vcr-osd-mono.font
17. https://woboq.com/blog/gpu-drawing-using-shadereffects-in-qtquick.html
    1. http://aligorith.blogspot.com/2016/09/qml-tip-making-one-shadereffect-use.html
18. https://stackoverflow.com/questions/39206743/what-is-the-difference-between-a-slot-and-a-method-in-qt
19. https://github.com/qmlbook/qmlbook//blob/master/docs/ch11-shaders/shaders.rst
20. https://stackoverflow.com/questions/17196639/qt-multiple-qglshaderprogram-for-one-texture
    1. https://stackoverflow.com/questions/11387062/web-gl-multiple-fragment-shaders-per-program
21. https://stackoverflow.com/questions/75025298/qt6-qtmultimedia-ffmpeg-backend-preview-procedure
    1. https://stackoverflow.com/questions/27549708/qml-how-to-specify-image-file-path-relative-to-application-folder
22. https://stackoverflow.com/questions/30973781/qt-add-custom-font-from-resource