#pragma once

#include <QObject>
#include <QtQml>

class ShaderToyTimer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal iTime READ iTime NOTIFY iTimeChanged)
    Q_PROPERTY(int iFrame READ iFrame NOTIFY iFrameChanged)
    Q_PROPERTY(qreal fps READ fps WRITE setFps NOTIFY fpsChanged)

    QML_ELEMENT

    qreal iTime_ = 0;
    qreal fps_ = 30;
    int iFrame_ = 0;
    int timerId_ = -1;
    qreal interval_in_s_;

protected:
    void timerEvent(QTimerEvent*) override;

public:
    ShaderToyTimer(QObject* parent = nullptr) : QObject(parent) {}

    qreal iTime() const { return iTime_; }
    int iFrame() const { return iFrame_; }
    qreal fps() const { return fps_; }
    void setFps(qreal v)
    {
        if(fps_ == v) return;

        fps_ = v;
        emit fpsChanged();
    }

    // Using slot instead of Q_INVOCABLE as per [18]
public slots:
    void start();
    //void stop();

signals:
    void iTimeChanged();
    void iFrameChanged();
    void fpsChanged();
};
