import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

import QtMultimedia

Item {
    id: root

    ShaderToyTimer {
        id: timer
        Component.onCompleted: start()
    }

    Item {
        id: theItem

        anchors.fill: parent
        Image {
            anchors.fill: parent
            id: imageLogo
            fillMode: Image.PreserveAspectFit
        }

        Video {
            id: video
            anchors.fill: parent
            //fillMode: Image.PreserveAspectFit
            // [21.1]
            source: "file://" + applicationDirPath + "/content/videos/Uncle.mp4"

            //Component.onCompleted: play()
            visible: false

            onErrorOccurred: console.log("Video err", video.errorString)
        }

        Text {
            anchors.right: theItem.right
            anchors.bottom: theItem.bottom

            //text: "Time index: " + Math.round(glitch.iTime * 100) / 100
            text: "Time index: " + timer.iTime.toFixed(2)
            color: "orangered"

            font.pointSize: 32
            font.family: "VCR OSD Mono"
        }
    }

    // Stacked shaders gleaned from PGQT-7
    Glitch {
        id: glitch
        timer: timer

        glitch_percent: 0.07
        duration: 10

        anchors.fill: parent

        // DEBT: [17] indicates ShaderEffectSource is expensive, and [17.1] shows us a technique
        // to chain shaders using less shader effect sources.  Try [17.1] technique

        source: ShaderEffectSource {
            sourceItem: Roll {
                id: roll
                //anchors.fill: theItem
                // DEBT: See why we can't use anchors and document that here
                width: root.width
                height: root.height
                //anchors.fill: root

                timer: timer

                scalinesOpt: ((iTime % 10) < 0.1) * 10
                bottomStaticOpt: ((iTime % 15) > 14.5) * 3
                rgbOffsetOpt: 0
                vertJerkOpt: ((iTime % 5) > 4.5)
                //horzFuzzOpt: ((iTime % 7.5) > 7.4) * 2

                SequentialAnimation on horzFuzzOpt {
                    loops: Animation.Infinite
                    // [17] and [5.4] indicate these are the optimal animators for shaders
                    UniformAnimator { duration: 7400; from: 0.0; to: 0.0;  }
                    UniformAnimator { duration: 50; to: 1.0 }
                    UniformAnimator { duration: 250; to: 1.0 }
                    UniformAnimator { duration: 50; to: 0.0 }
                    UniformAnimator { duration: 7400; from: 0.0; to: 0.0;  }
                    UniformAnimator { duration: 0; to: 2.0 }
                    UniformAnimator { duration: 250; to: 2.0 }
                    UniformAnimator { duration: 0; to: 0.0 }
                }

                //onHorzFuzzOptChanged: console.log("fuzz", horzFuzzOpt.toPrecision(4))

                source: ShaderEffectSource {

                    sourceItem: theItem
                }
            }
        }
    }

    // DEBT: It really feels like State should be doing this,
    // but I can't find the proper onXXX
    transitions: [
        Transition {
            to: "qt"
            onRunningChanged: {
                if(running)
                {
                    video.pause();
                    video.visible = false;
                    imageLogo.visible = true;
                }
            }
        },
        Transition {
            to: "bs"

            // TODO: Not working as expected
            /*
            SequentialAnimation {
                UniformAnimator {
                    target: roll
                    uniform: "bottomStaticOpt"
                    duration: 0
                    to: 3
                }
                UniformAnimator {
                    target: roll
                    uniform: "bottomStaticOpt"
                    duration: 5000
                    to: 0
                }
            } */
        },
        Transition {
            to: "vid1"
            onRunningChanged: {
                //console.log("vid1 transition", running)
                if(running)
                {
                    video.play();
                    video.visible = true
                    imageLogo.visible = false;
                }
            }
        }
    ]

    states: [
        State {
            name: "bs"
            PropertyChanges {
                imageLogo.source: "content/images/BurkeScience.jpg"
            }
            PropertyChanges {
                roll.bottomStaticOpt: 3
            }
        },
        State {
            name: "qt"
            PropertyChanges {
                imageLogo {
                    source: "content/qt-logo.png"
                }
            }
        },
        State {
            name: "vid1"
            PropertyChanges {
                imageLogo {
                    //source: null
                    visible: false
                }
            }
        },
        State {
            name: "vid2"
        }

    ]

    state: "qt"

    function rollState()
    {
        switch(state)
        {
            case "qt": state = "bs"; break
            case "bs": state = "vid1"; break
            case "vid1": state = "qt"; break
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            //console.log("state", root.state)
            rollState()
            //video.play()
        }
    }
}
