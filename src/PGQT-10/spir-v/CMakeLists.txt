cmake_minimum_required(VERSION 3.16)

project(PGQT-10.spir-v VERSION 0.1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(TARGET_NAME appspir-v)

find_package(Qt6 6.5 REQUIRED COMPONENTS Quick ShaderTools)

# NOTE: Not well tested yet
qt_policy(SET QTP0001 NEW)

qt_standard_project_setup(REQUIRES 6.5)

qt_add_executable(${TARGET_NAME}
    main.cpp
)

# From https://stackoverflow.com/questions/27660048/cmake-check-if-mac-os-x-use-apple-or-apple
if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(MACOSX TRUE)
    # Getting there, but still no dice.  Probably folder gets wiped out
    set(EXTRA_BINARY_PREFIX /Current/MacOS)
endif()


# FIX: For macOS, neither file nor configure_file put this video in the folder alongside the app
# because on macOS it puts executable into Contents/MacOS alongside plist
file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/content/videos/Uncle.mp4" DESTINATION
    ${CMAKE_CURRENT_BINARY_DIR}${EXTRA_BINARY_PREFIX}/content/videos)

#configure_file(content/videos/Uncle.mp4 content/videos COPYONLY)

qt_add_qml_module(${TARGET_NAME}
    URI spir-v
    VERSION 1.0
    SOURCES timer.h
    QML_FILES
        Glitch.qml
        Main.qml
        Page1.qml Page2.qml
        Roll.qml ShaderToy.qml
    RESOURCES
        "content/qt-logo.png"
        "content/VCR_OSD_MONO_1.001.ttf"
        "content/images/BurkeScience.jpg"
        "content/images/BurkeScience.png"
)


qt6_add_shaders(${TARGET_NAME} "shaders"
    # Was just a stab in the dark based on [5.2] but helps
    # (resolves unsigned int legacy error)
    GLSL "300es,330"

    DEBUGINFO

    BATCHABLE
    PRECOMPILE
    OPTIMIZED
    PREFIX
        # Remember this has to match up to URI!
        "/qt/qml/spir-v"
    FILES
        # [2.6]
        "content/shaders/distorted-tv.frag"

        # [2.1] is original shader toy
        # since we're updating fragColor and not position
        # I conclude this is a texture / fragment shader not a vertext
        # [13]
        "content/shaders/glitchy.frag"

        "content/shaders/glitchy.vert"

        # [2.4] is original shader toy
        # DEBT: Works, temporarily disabling as we try to get Android to work
        #"content/shaders/image.frag"
    )

set_target_properties(${TARGET_NAME} PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

target_link_libraries(${TARGET_NAME}
    PRIVATE Qt::Quick Qt::ShaderTools
)

install(TARGETS ${TARGET_NAME}
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
