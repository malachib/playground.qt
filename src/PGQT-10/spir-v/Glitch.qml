import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

ShaderToy {
    fragmentShader: "content/shaders/glitchy.frag.qsb"

    // TODO: Does not appear to be participating yet
    vertexShader: "content/shaders/glitchy.vert.qsb"

    property real glitch_percent: 0.5
    property real duration: 5
    property bool digital: false

    //onGlitch_percentChanged: console.log("glitch %", glitch_percent)
}
