import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

ShaderToy {
    property real bottomStaticOpt: 1;
    property real scalinesOpt: 1;
    property real rgbOffsetOpt: 1;
    property real horzFuzzOpt: 1;
    property real vertJerkOpt: 1;
    property real vertMovementOpt: 1;

    fragmentShader: "content/shaders/distorted-tv.frag.qsb"
}
