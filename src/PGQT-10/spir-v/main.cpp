#include <chrono>

#include <QDebug>
#include <QFontDatabase>

#include <QLoggingCategory>

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "timer.h"


using namespace std::chrono_literals;

void ShaderToyTimer::start()
{
    qDebug() << "ShaderToyTimer::start";

    if(timerId_ != -1) killTimer(timerId_);

    auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(1s / fps_);
    timerId_ = startTimer(interval, Qt::PreciseTimer);
    interval_in_s_ = interval.count();
    interval_in_s_ /= 1000;
}


void ShaderToyTimer::timerEvent(QTimerEvent* e)
{
    iTime_ += interval_in_s_;
    iFrame_++;
    emit iTimeChanged();
    emit iFrameChanged();
}


int main(int argc, char *argv[])
{
    QLoggingCategory cat("main");
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    // [21.1]
    QString appPath = QGuiApplication::applicationDirPath();
    engine.rootContext()->setContextProperty("applicationDirPath", appPath);

    qCDebug(cat) << "appPath" << appPath;

    // [22] gives us some good advice for how to construct URI.
    // Due to QTP0001 prepending /qt/qml though I would like to change that
    int result = QFontDatabase::addApplicationFont(":/qt/qml/spir-v/content/VCR_OSD_MONO_1.001.ttf");

    qDebug() << "Font load result:" << result;

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("spir-v", "Main");

    return app.exec();
}
