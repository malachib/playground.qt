import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

RowLayout {
    id: root

    ShaderToyTimer {
        id: timer
        Component.onCompleted: start()
    }

    // Needed since ShaderEffect can't source directly from 'theItem'
    // but can otherwise stay invisible
    ShaderEffectSource {
        id: theSource
        sourceItem: theItem

        // NOTE: This is an important line, the 'image' shader doesn't work without it
        // (presumably it can't figure out the original size)
        //anchors.fill: theItem

        // NOTE: Grabbing from qt_TexCoord0 rather than gl_FragCoord appears to factor
        // in mirroring
        //textureMirroring: ShaderEffectSource.NoMirroring

        visible: false
    }

    Item {
        id: theItem
        //anchors.fill: parent
        //Layout.alignment: Qt.AlignVCenter
        //Layout.fillWidth: true
        //Layout.fillHeight: true
        //Layout.preferredWidth: parent.width
        //Layout.preferredHeight: parent.height
        Layout.preferredHeight: imageLogo.height
        Layout.preferredWidth: imageLogo.width

        Image {
            //anchors.fill: parent
            id: imageLogo
            fillMode: Image.PreserveAspectFit

            source: "content/qt-logo.png"
        }

        ColumnLayout {


            Text {
                // DEBT: Not quite sure at the moment how to do a 0-1 float here
                text: "Hello: glitch=" + Math.round(glitchSlider.value * 100);
                color: "white"
                font.pointSize: 32
            }


            Slider {
                id: glitchSlider
                //Layout.fillHeight: true
                from: 0.0
                to: 1.0
                value: 0.1
                //stepSize: 0.05
                onValueChanged: {
                    //console.log("change", value)
                    //glitch.glitch_percent = value
                }
            }
        }

        //visible: false
    }

    Glitch {
        //width: 160
        //height: 160
        //anchors.fill: theSource
        //width: theSource.width
        //height: theSource.height
        Layout.preferredWidth: theItem.width
        Layout.preferredHeight: theItem.height
        Layout.fillHeight: true
        timer: timer

        glitch_percent: glitchSlider.value

        source: theSource

        MouseArea {
            anchors.fill: parent
            onClicked: parent.digital = !parent.digital
        }
    }

    Roll {
        Layout.preferredWidth: theItem.width
        Layout.preferredHeight: theItem.height
        Layout.fillHeight: true

        timer: timer
        source: theSource

        MouseArea {
            anchors.fill: parent
            onClicked: {
                parent.scalinesOpt = !parent.scalinesOpt
                parent.horzFuzzOpt = !parent.horzFuzzOpt
                parent.rgbOffsetOpt = !parent.rgbOffsetOpt
                //console.log("Roll: scalinesOpt=", parent.scalinesOpt)
            }
        }
    }
}
