#version 440

// Trying some clues from [5.3], qt_TexCoor0 doesn't seem to vary
layout(location = 0) in vec2 qt_TexCoord0;
//layout(location = 0) in vec2 texCoord0;
//layout(location = 1) in vec2 fragCoord;
layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform buf {
    // NOTE: Without qt_Matrix, we don't seem to get called at all
    mat4 qt_Matrix;

    float qt_Opacity;

    //float iTime;
    //int iFrame;
    vec3 iResolution;
};

layout(binding = 1) uniform sampler2D iChannel0;

// See [2.4] for original

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    // get pixel information from uv location
    vec4 texColor = texture(iChannel0, uv);
    
    // add code here

    // Output to screen
    fragColor = vec4(texColor);
}

void main()
{
    int __x = int(qt_TexCoord0.x * 100);
    bool blip = (__x % 10) == 0;
    if(blip)
        fragColor = vec4(1, 0, 0, 1);
    else
        mainImage(fragColor, qt_TexCoord0);

    return;

    //mainImage(fragColor, fragCoord);
    //fragColor = vec4(1, 1, 0, 1);
    //int __x = int(fragCoord.x);
    //int __x = int(texCoord0.x) + 1;
    //int __y = int(fragCoord.y * 100);
    int __y = 0;
    float blipG = (__y % 10) == 0 ? 1 : 0;
    fragColor = vec4(blip ? 1 : 0, blipG, 0, 1);
}