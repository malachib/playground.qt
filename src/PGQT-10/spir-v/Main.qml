import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

//import QtQuick3D
//import "spir-v"

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-10: Hello Glitch")
    color: "lightgray"

    /*
    Page1 {
        anchors.fill: parent
        visible: false
    } */

    Page2 {
        anchors.fill: parent
        visible: true
    }

    /*
    ShaderEffect {
        //Layout.preferredWidth: 200
        //Layout.preferredHeight: 200;
        visible: false
        width: 160
        height: 160

        readonly property vector3d iResolution: Qt.vector3d(width, height, 1.0)
        readonly property variant iChannel0: theSource

        fragmentShader: "content/shaders/image.frag.qsb"
    }
    */
}
