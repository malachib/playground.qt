import QtQuick 2.0

// Using example from https://qmlbook.github.io/en/ch09/index.html
Rectangle {
    width: 480; height: 240
    color: '#1e1e1e'


    Row {
        anchors.centerIn: parent
        spacing: 20
        Image {
            id: sourceImage
            width: 80; height: width
            source: 'assets/Tulips.jpg'
        }
        /*
        ShaderEffect {
            id: randomEffect
            vertexShader: "rand.glsl"
        } */

        ShaderEffect {
            id: effect
            width: 80; height: width
            property variant source: sourceImage
        }
        ShaderEffect {
            id: effect2
            width: 200; height: width
            // the source where the effect shall be applied to
            property variant source: sourceImage

            property real frequency: 8
            property real amplitude: 0.1
            property real time: 0.0
            property real random: 0.0

            NumberAnimation on time {
                from: 0; to: Math.PI*2; duration: 1000; loops: Animation.Infinite
            }

            // default vertex shader code
            vertexShader: "
                uniform highp float time;
                uniform highp mat4 qt_Matrix;
                attribute highp vec4 qt_Vertex;
                attribute highp vec2 qt_MultiTexCoord0;
                varying highp vec2 qt_TexCoord0;

                varying highp float current_scan_y;
                varying highp float current_offset_x;

                float rand(vec2 co){
                    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
                }

                void main() {
                    qt_TexCoord0 = qt_MultiTexCoord0;
                    gl_Position = qt_Matrix * qt_Vertex;

                    if(qt_TexCoord0.y != current_scan_y)
                    {
                        current_scan_y = qt_TexCoord0.y;
                        //current_offset_x = rand(qt_TexCoord0);
                        //current_offset_x = current_scan_y;
                    }

                    //qt_TexCoord0.x += sin(time) * 0.01;

                    // accidental random, looks like since this variable isnt set it wiggles.  Not reliable I think
                    //qt_TexCoord0.x += current_offset_x;

                    //qt_TexCoord0.x += rand(qt_TexCoord0) * time * 0.1;
                }"
            // default fragment shader code
            fragmentShader: "
                varying highp vec2 qt_TexCoord0;
                uniform sampler2D source;
                uniform lowp float qt_Opacity;
                uniform highp float frequency;
                uniform highp float amplitude;
                uniform highp float time;
                uniform float height;

                float rand(vec2 co){
                    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
                }

                void main() {
                    highp vec2 pulse = sin(time - frequency * qt_TexCoord0);
                    highp vec2 coord = qt_TexCoord0;
                    //coord = coord + amplitude * vec2(pulse.x, -pulse.x);
                    gl_FragColor = texture2D(source, coord) * qt_Opacity;

                    // pretty easy and nifty scanline sim
                    float scan_on = mod(floor(qt_TexCoord0.y * height / 2.0), 2.0);

                    if(scan_on == 0.0) gl_FragColor *= 0.7;
                }"
        }
    }
}
