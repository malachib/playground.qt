import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            ColumnLayout  {
                spacing: 2

                anchors.fill: parent

                Rectangle  {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: '#FF0000'
                    Label {
                        color: '#00FF00'
                        text: qsTr("Second page")
                        anchors.centerIn: parent
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: '#7F007F'
                    Shader1 {
                        anchors.centerIn: parent
                    }
                }
            }
        }

        Page1 {
        }

    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("First")
        }
        TabButton {
            text: qsTr("Second")
        }
    }
}
