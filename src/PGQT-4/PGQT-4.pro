TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc \
    ../../ext/QtQuickCarGauges/qml_resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = ../../ext/QtQuickCarGauges/qml

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    ../../ext/QtQuickCarGauges/qml/GraphGauge.qml \
    ../../ext/QtQuickCarGauges/qml/RoundGauge.qml
