#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QJniEnvironment>

// DEBT: "under development and is subject to change."
#include <QtCore/private/qandroidextras_p.h>

#include "shared/worker.h"
#include "main.h"

// Some guidance from [6]

class ServiceConnection : public QAndroidServiceConnection
{
public:
    void onServiceConnected(const QString& name,
        const QAndroidBinder& serviceBinder) override
    {
    }

    void onServiceDisconnected(const QString& name) override
    {

    }
};

ServiceConnection connection;

void bindToService()
{
    auto activity = QJniObject(QNativeInterface::QAndroidApplication::context());
    QAndroidIntent intent(activity.object(), "qt/playground/pgqt_65/Service");
    //QAndroidIntent intent("qt.playground.pgqt_65.Service.BIND");
    // Old [2.2] approach doesn't work, need slashes
    //QAndroidIntent intent(activity.object(), "qt.playground.pgqt_65.Service");
    QtAndroidPrivate::bindService(intent, connection, QtAndroidPrivate::BindFlag::AutoCreate);
}

void unbind()
{
    // https://developer.android.com/reference/android/content/Context#unbindService(android.content.ServiceConnection)

    auto activity = QJniObject(QNativeInterface::QAndroidApplication::context());

    // Curiously, this doesn't resolve to callVoidMethod which apparently is needed, even
    // when specifying trailing "V" return type
    //QJniObject result = activity.callObjectMethod

    activity.callMethod<void>(
        "unbindService",
        "(Landroid/content/ServiceConnection;)V",
        connection.handle().object());
}

QThread workerThread;

void Session::test()
{
    unbind();
}


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    auto worker = new TimerWorker;
    auto ui_thd = new TimerWorker;
    worker->suffix_ = "(poc3 w)";
    ui_thd->suffix_ = "(poc3 u)";
    worker->moveToThread(&workerThread);

    QObject::connect(&workerThread, &QThread::started, worker, &TimerWorker::init);
    QObject::connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    workerThread.start();
    ui_thd->init();

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/" TARGET_URI "/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    // Appears to work well
    bindToService();

    engine.load(url);

    return app.exec();
}
