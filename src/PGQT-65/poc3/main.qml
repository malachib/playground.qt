import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import PGQT_65.poc3

Window {
    width: 640
    height: 480
    visible: true

    title: qsTr("PGQT-65 poc3")

    RowLayout {
        Button {
            text: "Unbind";
            onClicked: Session.test()
        }
    }
}
