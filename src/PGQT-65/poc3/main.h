#pragma once

#include <QObject>
#include <QtQml>

class Session : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    Session(QObject* parent = nullptr) : QObject(parent)    {}

    Q_INVOKABLE void test();
};
