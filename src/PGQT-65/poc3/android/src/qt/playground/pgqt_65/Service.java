package qt.playground.pgqt_65;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.qtproject.qt.android.bindings.QtService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.os.Build;
import android.os.IBinder;

// Adapted from poc2
// Guidance from [6.2]

public class Service extends android.app.Service
{
    private static final String TAG = "qt.playground.pgqt_65.Service";
    private static final String CHANNEL_ID = "PGQT-65 Channel";

    // DEBT: In real life we wouldn't want this to be static. for proof of concept, it's OK
    static NotificationManager manager_;

    private IBinder binder_ = new Binder();

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(TAG, "Creating service");
    }

    void createNotificationChannel()
    {
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
       {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "PGQT-65 Service Channel",
                    // This we expect to do a heads-up notifcation, maybe [5]
                    NotificationManager.IMPORTANCE_HIGH
            );
            manager_ = getSystemService(NotificationManager.class);
            manager_.createNotificationChannel(serviceChannel);
       }
    }

    public static void standaloneNotification(Context context, String message)
    {
        try
        {
            Notification notification = new Notification.Builder(context, CHANNEL_ID)
                .setContentTitle("PGQT-65")
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_SOUND)
                // DEBT: Use a different icon here!
                .setSmallIcon(android.R.drawable.ic_media_next)
                //.setColor(Color.GREEN)
                // "For Android 8.0 and later, instead set the channel importance" [5.1]
                // .setPriority(Notification.PRIORITY_HIGH)
                .build();

            Log.i(TAG, "standaloneNotification");
            manager_.notify(0, notification);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    void evaluateIncomingIntent(Intent intent)
    {
        if(intent != null && intent.getAction() != null)
        {
            Log.d(TAG, "evaluateIncomingIntent");
        }
    }

/*
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        int ret = super.onStartCommand(intent, flags, startId);
        //Context context = getApplicationContext();
        Context context = this;

        evaluateIncomingIntent(intent);
        createNotificationChannel();

        Log.i(TAG, "onStartCommand");

        // Seems to work ok, just be sure to do notification channel (probably for permissions)
        // otherwise Android gets super mad and disconnects while deploying
        standaloneNotification(context, "Testing 1 2 3");

        // returning START_NOT_STICKY as per [3.1]
        return START_NOT_STICKY;
        //return ret;
    }   */

    @Override
    public void onDestroy()
    {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.i(TAG, "onBind");
        return binder_;
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.i(TAG, "in onUnbind");
        return true;
    }

    public class Binder extends android.os.Binder
    {
        Service getService()
        {
            return Service.this;
        }
    }
}
