# Overview

Collection of Qt Android Service tests & proof of concepts

# 1. Goals

## 1.1. poc1

This proof of concept seeks how far into service-like behavior we can push without making a true Android Service.

### 1.1.1. spin-up

Using the now-familiar "Create Templates" button from Kit's build steps.
For completeness, checking "Copy the Gradle files to Android directory" though I am unsure how necessary that truly is.

As usually, encountering issue 3.1.  Ignoring it.

## 1.2. poc2

Seeks how far we can get with the most basic service

## 1.3. poc3

Seeks how far we can get with bound service [5.3] which is said to not be subject to idle shutdown.
Unsurprisingly utilizes Android binder [6]

Note that we do this "in process", not creating a 2nd process for the Service itself.  The goal being both
app+service both never shut down due to a soft and deliberate deadly embrace.

## 1.4. poc4

Foreground service.  Already have done this a few times, so only a placeholder/baseline for further experimentation

## 1.5. poc5

## 1.6. poc6

## 1.7. poc7

Flavor of poc2 focusing on BLE on background thread

# 2. Prerequisites & Infrastructure

## 2.1. Infrastructure

### 2.1.1. Android Device: Pixel 3

### 2.1.2. Debian host: Qt v6.6.2

Set to:

* Android Clang arm64-v8a
* build-tools 33.0.2
* platform SDK android-33
* ANDROID_PLATFORM android-23
* NDK 25.1.8937393

### 2.1.3. Debian host: Qt v6.7.0

Set to:

* Android Clang arm64-v8a
* build-tools 33.0.2
* platform SDK android-33
* ANDROID_PLATFORM android-23
* NDK 26.1.10909125

## 2.2. Prerequisites

# 3. Opinions & Observations

## 3.1. Qt Creator: "Project file not Updated"

"Could not update the project file ..."

I've seen this error countless times, yet it seems to update everything fine.

## 3.2. JNI on non-main thread

It's been said that JNI MUST be used on main thread [3]
That would torpedo poc2 from working well, since even with a QThread helping out its only way to signal service would route through the potentially dormant main UI thread

The claim turns out to be false, however.  QJNIEnvironment itself inherently sets up on whatever thread is running, immediately opposing above notion.

## 3.3. Background Service: App Auto Destory

As expected, poc2 suffers from auto app destroy.  Evidenced by:

```
Stopping service due to app idle: u0a457 -1m3s951ms qt.playground.pgqt_65/.Service
```

Interestingly, while diagnosing 4.2.1. the runtime seems to be quite extended.  I'm accustomed to seeing ~30s timeout but seeing well over 5m now and still running (poc2)

In logs

## 3.4. logcat warnings

### 3.4.1. no bubble up

Apparently notification mechanism connects to the chat-bubble style feature.  We are definitely not interested in using that, so ignoring this warning

### 3.4.2. no heads up

This one we want to remedy

## 3.5. BLE operation off main thread

This works extremely well.  Especially when coupled with QObject's thread-hopping signal/slot features.
QML does not like consuming off-thread QObjects, but that's not BLE's fault.

## 3.6. Background Service: Binding approach

It seems there are 3 different approaches for binding to a service [6.1]:

* `AIDL`
* `Binder`
* `Messenger`

Since we are using binding purely to manage the lifetime of our service (poc3), I expect any of these will do.
Therefore, I opt for what appears to be the simplest, which is `Binder`

## 3.7. App "freezing"

On Pixel 7, Android App Cache Freezing feature ends up pseudo-hibernating poc3.  Bummer.  Seems to happen after
about 10 minutes

Seen right in logs:

```
ActivityManager         system_server                        D  freezing 8438 qt.playground.pgqt_65
```

```
ActivityManager         system_server                        D  sync unfroze 8438 qt.playground.pgqt_65 for 1
```

## 3.8. Main thread still running

On Pixel 7, unlike on Pixel 3, main UI thread appears to keep running even when app is backgrounded

# 4. Troubleshooting

## 4.1. Heads-up won't display

Not sure why, but despite high priority notification setting, heads-up isn't showing.
This discussion [5.2] could be revealing

## 4.2. Service Startup

### 4.2.1. same process warning

```
W Qt JAVA : A QtService tried to start in the same process as an initiated QtActivity. That is not supported. This results in the service functioning as an Android Service detached from Qt.
```

QtService seems to be particularly useful to load Qt libraries.  However, for same-process service
this is already happening [2.1].  They say that "either works" for QtService or android.app.Service,
but if you try the former you get the above warning.

## 4.3. Windows Compilation

Verified prerequisites matching 2.1.2. are present

A whole host of compilation problems (hundreds of lines), starting with:

```
ERROR:C:\Projects\playground\playground.qt\src\PGQT-65\build-poc7-Android_Qt_6_6_2_Clang_arm64_v8a-Debug\android-build\build\intermediates\javac\debug\classes\org\qtproject\qt\android\bindings\QtLoader$2.class: D8: java.lang.NullPointerException: Cannot invoke "String.length()" because "<parameter1>" is null
ERROR:C:\Projects\playground\playground.qt\src\PGQT-65\build-poc7-Android_Qt_6_6_2_Clang_arm64_v8a-Debug\android-build\build\intermediates\javac\debug\classes\org\qtproject\qt\android\bindings\QtLoader$1.class: D8: java.lang.NullPointerException: Cannot invoke "String.length()" because "<parameter1>" is null
org.gradle.workers.WorkerExecutionException: There were multiple failures while executing work items
	at org.gradle.workers.internal.DefaultWorkerExecutor.workerExecutionException(DefaultWorkerExecutor.java:221)
	at org.gradle.workers.internal.DefaultWorkerExecutor.await(DefaultWorkerExecutor.java:201)
	at com.android.build.gradle.internal.tasks.DexArchiveBuilderTaskDelegate.doProcess(DexArchiveBuilderTaskDelegate.kt:220)
    ...
```

I suspect the Eclipse flavor of Java, because I'm prejudice that way

# 5. Results

## 5.1. poc1

As long as one doesn't "swipe terminate" the app, merely switching to a different android app doesn't interrupt QThread worker. Cool.

## 5.2. poc2

Does everything poc1 does plus it's able to perform notifications without app being in foreground.
Subject to idle shutdown as per 3.3.

## 5.3. poc3

Pixel 3: Works great
Pixel 7: Exhibits cache shutdown as per 3.7.

## 5.4. poc4

## 5.5. RESERVED
## 5.6. RESERVED

## 5.7. poc7

BLE works great on background thread.  I suspect Tracelet's initial connections in the first place to controller.

# Terminology
