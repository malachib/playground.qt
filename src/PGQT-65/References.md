1. https://doc.qt.io/qt-6.5/qthread.html
2. https://doc.qt.io/qt-6.5/android-services.html
    1. https://doc.qt.io/qt-5/qtandroidextras-services-servicesameprocess-example.html
    2. https://doc.qt.io/qt-5/qtandroidextras-services-servicebinder-example.html
3. https://stackoverflow.com/questions/55284006/how-to-integrate-qt-library-inside-android-native-project
4. https://stackoverflow.com/questions/12900695/how-to-obtain-jni-interface-pointer-jnienv-for-asynchronous-calls
5. https://developer.android.com/develop/ui/views/notifications
    1. https://developer.android.com/develop/ui/views/notifications/build-notification
    2. https://stackoverflow.com/questions/53206103/android-notification-not-appearing-as-heads-up
    3. https://developer.android.com/develop/background-work/services/bound-services
6. https://stackoverflow.com/questions/8341667/bind-unbind-service-example-android
    1. https://developer.android.com/develop/background-work/services/aidl
    2. https://www.truiton.com/2014/11/bound-service-example-android/
