#include <QDebug>

#include "worker.h"

void TimerWorker::timerEvent(QTimerEvent*)
{
    qDebug() << "Timer" << (suffix_ ? suffix_ : "") << ++counter_;
}

void TimerWorker::init()
{
    startTimer(timeout_);
}
