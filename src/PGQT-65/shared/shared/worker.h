#include <QObject>

class TimerWorker : public QObject
{
    int counter_ = 0;

protected:
    void timerEvent(QTimerEvent*) override;

public:
    void init();

    int timeout_ = 2000;
    const char* suffix_ = nullptr;
};
