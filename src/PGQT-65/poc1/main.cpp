#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTimer>

#include "worker.h"

static int counter = 0;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QThread workerThread;

    const QUrl url(u"qrc:/" TARGET_URI "/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url, &workerThread](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);

        // Preferred approach due to ease of signal/slot management
        // "you are free to connect the Worker's slots to any signal, from any object, in any thread" [1]
        auto worker = new Worker;
        worker->moveToThread(&workerThread);

        QObject::connect(&workerThread, &QThread::started, worker, &Worker::doWork);
        QObject::connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        workerThread.start();

        // Works nicely, but signals/slots are easier to manage with above approach
        //auto worker = new WorkerThread(obj);
        //worker->start();

        // As expected, main thread based timer only runs if app is in foreground
        auto timer = new QTimer(obj);
        QObject::connect(timer, &QTimer::timeout, obj, []
            { qDebug() << "UI worker:" << ++counter; });
        timer->start(1000);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
