#include <QDebug>
#include <QObject>
#include <QThread>

class Worker : public QObject
{
    Q_OBJECT

public:
    bool running_ = true;

public slots:
    void doWork()
    {
        int counter = 0;

        // DEBT: Crude signal mechanism
        while(running_)
        {
            qDebug() << "Worker running:" << ++counter;
            thread()->msleep(1000);
        }
    }
};


class WorkerThread : public QThread
{
    Q_OBJECT

    bool running_ = true;

    void run() override
    {
        int counter = 0;

        // DEBT: Crude signal mechanism
        while(running_)
        {
            qDebug() << "Worker running:" << ++counter;
            thread()->wait(1000);
        }
    }

public:
    WorkerThread(QObject* parent) : QThread(parent) {}
};

