#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QPermission>

#include "worker.h"

JavaVM* g_vm;

QThread workerThread;

// DEBT: Keep an eye on this
static QJniEnvironment jni_env;

// NOTE: Qt QJniEnvironment "Constructs a new JNI Environment object and attaches the current thread to the Java VM."
// so it looks like [3] "calls to/from JNI have to run from the main thread" MAY not be true.  What would
// above comment mean if it were?

// Adapted from [4]
JNIEnv* Worker::attach_jni() {
    JNIEnv* g_env;
    qDebug() << "Phase 1";
    // double check it's all ok
    int getEnvStat = g_vm->GetEnv((void **)&g_env, JNI_VERSION_1_6);
    if (getEnvStat == JNI_EDETACHED)
    {
        qDebug() << "Phase 1.1";
        std::cout << "GetEnv: not attached" << std::endl;
        if (g_vm->AttachCurrentThread(&g_env, NULL) != 0)
        {
            std::cout << "Failed to attach" << std::endl;
        }
        else
            return g_env;
    } else if (getEnvStat == JNI_OK) {
        qDebug() << "Phase 1.2";
        return g_env;
    } else if (getEnvStat == JNI_EVERSION) {
        qDebug() << "Phase 1.3";
        std::cout << "GetEnv: version not supported" << std::endl;
    }

    return g_env;
}

// lifted from PGQT-44
static void start_service()
{
    auto activity = QJniObject(QNativeInterface::QAndroidApplication::context());
    QAndroidIntent serviceIntent(activity.object(),
                                 "qt/playground/pgqt_65/Service");
    QJniObject result = activity.callObjectMethod(
        "startService",
        "(Landroid/content/Intent;)Landroid/content/ComponentName;",
        serviceIntent.handle().object());
}


void setup(QGuiApplication& app)
{
    //g_vm = jni_env.javaVM();
    //QPermission p()
    // TODO: Look into bubble up permission in this way vs AndroidManifest.xml because we prefer the interactivity of it
    // note that we aren't immediately interested in this permission as per README 3.4.1
    /*
    QtAndroidPrivate::B p;
    app.requestPermission(
        p,
        &app,
        [&](const QPermission& permission)
        {
            qDebug() << "requestAccess: " << permission;
        }); */
    start_service();
}


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/" TARGET_URI "/main.qml"_qs);

    setup(app);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);

        // Preferred approach due to ease of signal/slot management
        // "you are free to connect the Worker's slots to any signal, from any object, in any thread" [1]
        auto worker = new Worker;
        worker->moveToThread(&workerThread);

        QObject::connect(&workerThread, &QThread::started, worker, &Worker::doWork);
        QObject::connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        workerThread.start();

    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
