#include <QDebug>
#include <QObject>
#include <QThread>

#include <QJniObject>

#include <iostream>

#include <QtCore/private/qandroidextras_p.h>


class Worker : public QObject
{
    Q_OBJECT

    static JNIEnv* attach_jni();

    int counter_ = 0;

public:
    bool running_ = true;

public slots:
    void doWork()
    {
        //attach_jni();
        // DEBT: Crude signal mechanism
        while(running_)
        {
            qDebug() << "Worker running:" << ++counter_;
            thread()->msleep(1000);
            notify(QString::number(counter_));
        }
    }

    void notify(const QString& v)
    {
        auto javaString = QJniObject::fromString(v);
        QJniEnvironment env;
        jclass javaClass = env.findClass("qt/playground/pgqt_65/Service");
        jmethodID methodId = env.findStaticMethod(javaClass,
            "standaloneNotification",
            "(Landroid/content/Context;Ljava/lang/String;)V");

        QJniObject::callStaticMethod<void>(javaClass, methodId,
            QNativeInterface::QAndroidApplication::context(),
            javaString.object<jstring>());
    }
};
