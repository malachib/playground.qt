#include <QBluetoothPermission>

#include "worker.h"

void Worker::timerEvent(QTimerEvent*)
{
    qDebug() << "Timer:" << ++counter_;
}

void Worker::init()
{
    startTimer(1000);
}


/*
QLowEnergyService* Worker::setupListener()
{
    QObject* p = this;
    QBluetoothUuid uuid(QBluetoothUuid::ServiceClassUuid::BatteryService);
    QBluetoothUuid descriptor(QBluetoothUuid::CharacteristicType::BatteryLevel);

    QLowEnergyService* service = controller_->createServiceObject(uuid, p);

    Q_ASSERT_X(service, "setupListener", "service is null");

    QObject::connect(service, &QLowEnergyService::stateChanged, p, [=]
        (QLowEnergyService::ServiceState state)
        {
            qDebug() << "service" << service->serviceName() << " state:" << state;

            if(state == QLowEnergyService::ServiceState::RemoteServiceDiscovered)
            {
                //for(auto c : service->characteristics())    qDebug() << "Characteristic:" << c.name();

                QLowEnergyCharacteristic chr = service->characteristic(descriptor);

                if(!chr.isValid())  qWarning() << "Invalid charateristic";
                QLowEnergyDescriptor notification = chr.descriptor(
                QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
                if(!notification.isValid())
                {
                    qWarning() << "Invalid notification";
                    return;
                }

                QObject::connect(service, &QLowEnergyService::characteristicChanged, p,
                    [=](const QLowEnergyCharacteristic&, const QByteArray& v)
                {
                    float level = v.at(0);
                    QString s("Battery: ");

                    s += QString::number(level);
                    s += "%";
                    qDebug() << s;
                    notify(s);
                });

                service->writeDescriptor(notification, QByteArray::fromHex("0100"));
            }
        });

    service->discoverDetails();

    return service;
}
*/


void Worker::setupBas()
{
    bas_ = new pgqt::ble::gatt::client::BAS(this);
    Q_ASSERT_X(bas_->create(controller_), "setupBas", "service is null");

    connect(bas_, &pgqt::ble::gatt::client::BAS::levelUpdated, this, [this](uint8_t level)
        {
            QString s("BATT (worker): ");

            s += QString::number(level);
            s += "%";
            qDebug() << s;
            notify(s);
        });
}

