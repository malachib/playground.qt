#pragma once

#include <QDebug>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QObject>
#include <QTimer>
#include <QThread>

#include <pgqt/ble/bas.h>

class BLE : public QObject
{
    Q_OBJECT

protected:
    QLowEnergyController* controller_;
    // DEBT: Only doing virtual approach temporarily as we split apart BLE and Worker
    virtual void setupBas() = 0;
    pgqt::ble::gatt::client::BAS* bas_;

public:
    BLE(QObject* parent = nullptr) : QObject(parent)    {}

private slots:
    void deviceConnected();
    void deviceDisconnected();
    void serviceDiscovered(const QBluetoothUuid& gatt);

public slots:
    void deviceDiscovered(const QBluetoothDeviceInfo&);
};

bool isTracelet(const QBluetoothDeviceInfo&);

