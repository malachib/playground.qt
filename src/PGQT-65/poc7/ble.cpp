#include "ble.h"

#include <pgqt/ble/beacon.h>


void BLE::deviceDiscovered(const QBluetoothDeviceInfo& info)
{
    if(isTracelet(info))
    {
        qDebug() << "Tracelet identified: " << info.address();

        QLowEnergyController* controller = QLowEnergyController::createCentral(info);

        controller_ = controller;

        connect(controller, &QLowEnergyController::connected,
                this, &BLE::deviceConnected);
        connect(controller_, &QLowEnergyController::serviceDiscovered,
                this, &BLE::serviceDiscovered);
        connect(controller, &QLowEnergyController::disconnected,
                this, &BLE::deviceDisconnected);

        controller->connectToDevice();
    }
}


void BLE::deviceConnected()
{
    qDebug() << "deviceConnected";
    controller_->discoverServices();
}

void BLE::deviceDisconnected()
{

}


void BLE::serviceDiscovered(const QBluetoothUuid& gatt)
{
    qDebug() << "serviceDiscovered:" << gatt;

    if(gatt == QBluetoothUuid::ServiceClassUuid::BatteryService)
    {
        //setupListener();
        setupBas();
    }
}


// NOTE: Copy/pasted from Tracelet ESP firmware, might change
#define TRACELET_IBEACON_UUID_NAMESPACE \
0xBB, 0xBB, 0xBC, 0x7F, 0xAC, 0xC8, 0x52, 0x72, 0xE1, 0x3B



bool isTracelet(const QBluetoothDeviceInfo& info)
{
    const QByteArray beacon = info.manufacturerData(0x004C);  // Apple

    static constexpr uint8_t ns[] { TRACELET_IBEACON_UUID_NAMESPACE };

    // FIX: Beware, endian issues
    auto b = (pgqt::ble::gatt::Beacon*)beacon.data();
    return !beacon.isNull() && std::memcmp(b->uuid, ns, sizeof(ns)) == 0;
}


