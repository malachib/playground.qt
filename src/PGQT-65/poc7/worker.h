#include <QDebug>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QObject>
#include <QTimer>
#include <QThread>


#include <QJniObject>

#include <iostream>

#include <QtCore/private/qandroidextras_p.h>

#include "ble.h"

// DEBT: Make BLE a has-a not an is-a
class Worker : public BLE
{
    Q_OBJECT

    int counter_ = 0;

    QLowEnergyService* setupListener();

protected:
    void timerEvent(QTimerEvent*) override;
    void setupBas() override;

public:

public slots:
    void init();

    void notify(const QString& v)
    {
        auto javaString = QJniObject::fromString(v);
        QJniEnvironment env;
        jclass javaClass = env.findClass("qt/playground/pgqt_65/Service");
        jmethodID methodId = env.findStaticMethod(javaClass,
            "standaloneNotification",
            "(Landroid/content/Context;Ljava/lang/String;)V");

        QJniObject::callStaticMethod<void>(javaClass, methodId,
            QNativeInterface::QAndroidApplication::context(),
            javaString.object<jstring>());
    }
};


