cmake_minimum_required(VERSION 3.19)

project(PGQT-65.poc7 VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 6.4 COMPONENTS Quick Bluetooth REQUIRED)

set(TARGET_NAME app${PROJECT_NAME})

STRING(REPLACE "-" "_" TARGET_URI ${PROJECT_NAME})

add_subdirectory(../../../ext/pgqt-util/src/pgqt-util pgqt-util)

qt_add_executable(${TARGET_NAME}
    ble.cpp ble.h main.cpp worker.cpp worker.h
)

qt_add_qml_module(${TARGET_NAME}
    URI ${TARGET_URI}
    VERSION 1.0
    QML_FILES main.qml

)

STRING(REPLACE "." "/" TARGET_URI ${TARGET_URI})
target_compile_definitions(${TARGET_NAME} PRIVATE TARGET_URI=\"${TARGET_URI}\")

set_target_properties(${TARGET_NAME} PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE

    # 25MAR24 MB manually adding this
    QT_ANDROID_PACKAGE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/android
)

target_link_libraries(${TARGET_NAME}
    PRIVATE Qt6::Quick Qt6::Bluetooth pgqt-util)

install(TARGETS ${TARGET_NAME}
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
