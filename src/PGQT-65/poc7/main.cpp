#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QPermission>

#include "worker.h"

JavaVM* g_vm;

QThread workerThread;

// lifted from PGQT-44
static void start_service()
{
    auto activity = QJniObject(QNativeInterface::QAndroidApplication::context());
    QAndroidIntent serviceIntent(activity.object(),
                                 "qt/playground/pgqt_65/Service");
    QJniObject result = activity.callObjectMethod(
        "startService",
        "(Landroid/content/Intent;)Landroid/content/ComponentName;",
        serviceIntent.handle().object());
}


void setup(QGuiApplication& app)
{
    start_service();
    QBluetoothPermission p;

    p.setCommunicationModes(QBluetoothPermission::Access);
    qApp->requestPermission(
        p,
        &app,
        [&](const QPermission& permission)
        {
            qDebug() << "requestAccess: " << permission;
        });
}


QBluetoothDeviceDiscoveryAgent* scan(QObject* parent, Worker* worker)
{
    auto agent = new QBluetoothDeviceDiscoveryAgent(parent);

    agent->setLowEnergyDiscoveryTimeout(5000);

    QObject::connect(agent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            worker, &Worker::deviceDiscovered);
    QObject::connect(agent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
                     parent, [=](const QBluetoothDeviceInfo& info)
    {
        if(isTracelet(info))
        {
            qDebug() << "Tracelet identified (main thread): " << info.address();

            // NOTE: I suspect this of needing to happen first on worker thread, but that is unproven
            QLowEnergyController* controller = QLowEnergyController::createCentral(info);

            QObject::connect(controller, &QLowEnergyController::connected,
                    parent, [=]{ controller->discoverServices(); });
            // DEBT: For true apples-to-apples, we should hang off serviceDiscovered
            QObject::connect(controller, &QLowEnergyController::discoveryFinished,
                    parent, [=]
            {
                auto bas = new pgqt::ble::gatt::client::BAS(parent);
                Q_ASSERT_X(bas->create(controller), "setupBas", "service is null");

                QObject::connect(bas, &pgqt::ble::gatt::client::BAS::levelUpdated, parent, [](uint8_t level)
                {
                    QString s("BATT (main): ");

                    s += QString::number(level);
                    s += "%";
                    qDebug() << s;
                });
            });

            controller->connectToDevice();
        }
    });

    agent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    return agent;
}


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/" TARGET_URI "/main.qml"_qs);

    setup(app);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);

        // Preferred approach due to ease of signal/slot management
        // "you are free to connect the Worker's slots to any signal, from any object, in any thread" [1]
        auto worker = new Worker;
        worker->moveToThread(&workerThread);

        QObject::connect(&workerThread, &QThread::started, worker, &Worker::init);
        QObject::connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        workerThread.start();

        // NOTE: Purposely doing this outside the worker to make sure no magic side effects occur
        // between agent and QLowEnergyController
        scan(obj, worker);

    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
