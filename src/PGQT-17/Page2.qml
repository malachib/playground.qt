import QtQuick 2.0
import QtGraphicalEffects 1.0
//import QtQuick.Controls 2.0
import QtMultimedia 5.3

// bringing in https://stackoverflow.com/questions/39903639/how-to-apply-opacity-mask-to-a-qml-item
// as it seems like a good pre-set example; then upgrade it to a full image (or video!)
Item {
    Rectangle {
        id: background
        anchors.fill: parent

        MediaPlayer {
            id: video_source
            source: "qrc:/assets/GoldWaveBlue.mp4"
            autoPlay: true
            loops: MediaPlayer.Infinite
        }

        VideoOutput {
            id: video
            source: video_source
        }
    }

    Rectangle {
        id: foreground
        anchors.fill: parent
        color: "black"
    }

    Text {
        id: txt
        anchors.centerIn: parent
        text: "Test"
        font.pointSize: 160
        font.bold: true
        color: "black"
    }

    OpacityMask {
        anchors.fill: txt
        source: background
        maskSource: txt
    }

}
