#include "mainwindow.h"
#include <QApplication>

#include <QPainter>
#include <QSvgRenderer>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    /*
    QSvgRenderer renderer(QString(":/radiation-warning"));
    bool isValid = renderer.isValid();
    QImage image(500, 500, QImage::Format_ARGB32);
    image.fill(0xaaA08080); // partly transparent red-ish background

    QPainter painter(&image);
    renderer.render(&painter);

    image.save("test.png");
    */
    MainWindow w;
    w.show();

    return a.exec();
}
