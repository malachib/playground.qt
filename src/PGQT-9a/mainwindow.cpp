#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtSvg>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    svgWidget = new QSvgWidget(this);
    svgWidget->load(QString(":/radiation-warning"));
    this->setCentralWidget(svgWidget);

    timer = new QTimer(this);

    //connect(timer, SIGNAL(timeout()), this, SLOT(exit()));
    //timer->setSingleShot(true);
    //timer->start(5000);

    QTimer::singleShot(5000, this, SLOT(exit()));
}

MainWindow::~MainWindow()
{
    delete timer;
    delete svgWidget;
    delete ui;
}

void MainWindow::exit()
{
    QApplication::quit();
}
