import QtQuick

// DEBT: Configure CMakeLists.txt to clean up this warning
// NOTE: Runtime doesn't complain, so it probably IS finding this import space
import example.mylib

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    // FIX: "Mistake is not a type"
    Mistake {

    }
}
