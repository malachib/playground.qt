#include <QObject>
#include <QtQml/qqmlregistration.h>

#if defined(LIB_COMPILING)
#  define LIB_EXPORT Q_DECL_EXPORT
#else
#  define LIB_EXPORT Q_DECL_IMPORT
#endif

class
        LIB_EXPORT   // NOTE: Does not seem to make a difference
        MyType : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(int answer READ answer CONSTANT)

    // NOTE: Examples from [1], [2] are misleading as they indicate
    // no definition for answer.  We provide one, deviating from example
public:
    int answer() const { return 0; }
};
