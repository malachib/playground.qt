#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QtPlugin>

#include "mytype.h"

// [6], [6.1] and [6.2] all suggest we should do this.  It doesn't compile.
// Furthermore, PGQT-299 is not about a plugin so much as a "regular" lib
// which holds QML
//Q_IMPORT_PLUGIN(example_mylibPlugin)


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Links OK
    MyType myType;

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/PGQT_29/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
