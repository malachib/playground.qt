#include <QtQml/qqmlextensionplugin.h>

#include "lib.h"

// "It's composed of the module URI with Plugin appended to it" [1.1], [1.2]
// that means lib1 + Plugin
// "defaults to the URI with dots replaced by underscores, then Plugin appended" [2.2]
Q_IMPORT_QML_PLUGIN(lib1Plugin)

namespace lib1 {

Lib1 lib{"hi2u"};

const char* Lib1::get_id() const
{
    return id;
}

}
