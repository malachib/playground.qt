1# Overview

Document v0.1

# Detail

## 1. Guidance (Qt5)

## 2. Guidance (Qt6)

### 2.1. PGQT-29 "qt6"

Following initially code laid out in [1]

### 2.2. PGQT-29 POC2 (qmldir approach)

Following guidance at [7.1] and [7.2]

QML_IMPORT_PATH guidance from [8] and [8.1]

### 2.3. PGQT-29 POC3 (qt_add_qml_module approach)

Meeting some success here - guidance from [1.1] which was later rolled into [1.2]
Depends heavily on 6.2+ API [2.2]

### 2.4. PGQT-29 POC4 (qt_add_qml_module diagnostic)

For some reason, notos-lib mk2 dies when trying to use qt_add_qml_module.  Catastrophically, I might add.  POC3 here works fine, so POC4 is an attempt
to recreate the issue and starts as a duplicate of POC3

# References

1. https://www.qt.io/blog/introduction-to-the-qml-cmake-api
   1. https://www.qt.io/blog/qml-modules-in-qt-6.2
   2. https://doc.qt.io/qt-6.5/qtqml-writing-a-module.html
2. https://doc.qt.io/qt-6.2/cmake-build-reusable-qml-module.html
   1. https://doc.qt.io/qt-6/cmake-build-reusable-qml-module.html
   2. https://doc.qt.io/qt-6.5/qt-add-qml-module.html
3. https://doc.qt.io/qt-6/sharedlibrary.html
4. https://gitlab.com/kelteseth/qt6-cmake-qml-plugin-example-project
5. https://gitlab.com/basic53/DemoApp
6. https://doc.qt.io/qt-6/qtwidgets-tools-plugandpaint-app-example.html
   1. https://doc.qt.io/qt-6/qtplugin.html
   2. https://code.qt.io/cgit/qt/qtbase.git/tree/examples/widgets/tools/plugandpaint/app/main.cpp?h=6.5
7. https://doc.qt.io/qt-6.5/qtqml-modules-topic.html
   1. https://doc.qt.io/qt-6.5/qtqml-modules-identifiedmodules.html
   2. https://doc.qt.io/qt-5/qtqml-modules-qmldir.html
8. https://doc.qt.io/qtcreator/creator-qml-modules-with-plugins.html
   1. https://stackoverflow.com/questions/34369967/how-to-use-qml-import-path-with-qt-cmake-project
9. https://stackoverflow.com/questions/72378116/qml-module-is-not-installed
