#pragma once

namespace lib1 {

class Lib1
{
    const char* id;

public:
    Lib1(const char* id) : id{id} {}

    // A virtual method excercises the linker a fair bit harder than your average call
    virtual const char* get_id() const;
};

extern Lib1 lib;

}

