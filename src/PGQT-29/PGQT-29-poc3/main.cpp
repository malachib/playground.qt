#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <lib.h>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Take steps to motivate true vtable activation rather than an inline optimization
    lib1::Lib1* lib = &lib1::lib;

    qDebug() << "main: " << lib->get_id();

    QQmlApplicationEngine engine;
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("PGQT-29-poc3", "Main");

    return app.exec();
}
