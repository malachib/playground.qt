import QtQuick 2.0
import WeatherCategory 1.0

// python-invoked test
Rectangle {
    id: pytest
    color: 'black'

    Weather {
        id: w1
    }


    Text {
        color: "white"
        id: current_temp
        //text: w1.current_summary
        text: w1.current.summary
    }
}
