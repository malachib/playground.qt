#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from PyQt5.QtCore import QObject, QUrl, QThreadPool, pyqtSignal, pyqtProperty
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QPainter, QColor, QPen
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine


class DataPoint(QObject):

    #datapointChanged = pyqtSignal(DataPoint, arguments=['datapoint'])

    _summary = "Hi2u"

    def __init__(self, parent=None):
        super().__init__(parent)

    @pyqtProperty('QString', constant=True)
    def summary(self): # , notify=datapointChanged):
        return self._summary


# This is the type that will be registered with QML.  It must be a
# sub-class of QObject.
class Weather(QObject):
    forecastChanged = pyqtSignal(DataPoint, arguments=['forecast'])

    def __init__(self, parent=None):
        super().__init__(parent)

        self._datapoint = DataPoint()

    @pyqtProperty(DataPoint, notify=forecastChanged)
    def current(self):
        #return DataPoint()
        return self._datapoint

app = QApplication(sys.argv)

qmlRegisterType(Weather, 'WeatherCategory', 1, 0, 'Weather')
qmlRegisterType(DataPoint, 'WeatherCategory', 1, 0, 'DataPoint')

# Create the QML user interface.
view = QQuickView()
view.setSource(QUrl('PyTest.qml'))
view.setGeometry(100, 100, 400, 240)
view.setColor(QColor(0, 0, 0))
view.show()

app.exec_()
