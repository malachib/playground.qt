#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QCoapReply>

// IIRC, this sets up QtCoap namespace as accessible from QML.  However,
// we don't use any of its enums etc. in QML, so this function sits dormant for now.
[[maybe_unused]]
static void init_coap_namespace()
{
    // Guidance from
    // https://stackoverflow.com/questions/20089196/how-to-access-c-enum-from-qml
    // DEBT: Almost definitely QtCoap already has a provision to do this
    // NOTE: Doesn't really seem to help
    qmlRegisterUncreatableMetaObject(
      QtCoap::staticMetaObject, // meta object created by Q_NAMESPACE macro
      "QtCoap",                 // import statement (can be any string)
      1, 0,                          // major and minor version of the import
      "QtCoap",                 // name in QML (does not have to match C++ name)
      "Error: only enums"            // error in case someone tries to create a MyNamespace object
    );
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/PGQT_28/main.qml"_qs);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
