/**
 *
 * References:
 *
 * 1. https://forum.qt.io/topic/97612/get-single-item-and-it-s-property-of-qabstractlistmodel-in-qml/8
 * 2. https://stackoverflow.com/questions/50281239/qt-define-global-function-for-qml
 * 3. https://doc.qt.io/qt-6/qtqml-cppintegration-definetypes.html
 * 4. https://forum.qt.io/topic/52621/qabstractitemmodel-property-buinding
 */
#include "utility.h"

// General notion from [2] with refinement from [3]

/*
Utility::Utility(QObject *parent)
    : QObject{parent}
{

}
*/

QString Utility::test() const
{
    return "got here";
}


// Big help from [4]
QVariant Utility::get(const QAbstractItemModel* _model,
                         const QByteArray& roleName,
                         int row,
                         int column,
                         const QModelIndex& parent) const
{
    const QAbstractItemModel& model = *_model;
    int role = model.roleNames().key(roleName, -1);

    if (role < 0) return QVariant();

    return model.data(model.index(row, column, parent), role);
}

QHash<int, QByteArray> EnumModelBase::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[KeyRole] = "key";
    roles[ValueRole] = "value";

    return roles;
}


QVariant EnumModelBase::data(const QModelIndex& index, int role) const
{
    int row = index.row();

    switch(role)
    {
        case KeyRole: return metaEnum.key(row);
        case ValueRole: return metaEnum.value(row);
        default: return QVariant();
    }

    metaEnum.value(index.row());
}
