#include "coapquery.h"
#include "utility.h"

#include <QtDebug>

CoapQueryController::CoapQueryController(QObject *parent)
    : QObject{parent},
      counter_{0},
      client(QtCoap::SecurityMode::NoSecurity, this)
{
    timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, &CoapQueryController::timeout);
    timer->start(1000);

    //connect(&client, &QCoapClient::finished, this, &TestClass::onFinished);

    EnumModelBase test(QMetaEnum::fromType<QtCoap::Method>());

    int rowCount = test.rowCount();
    QVariant data = test.data(test.index(0, 0), EnumModelBase::KeyRole);
    QVariant data2 = test.data(test.index(1, 0), EnumModelBase::KeyRole);
    QVariant data3 = test.data(test.index(2, 0), EnumModelBase::ValueRole);
    QVariant data4 = test.data(test.index(2, 0), EnumModelBase::KeyRole);
    QVariant data5 = test.data(test.index(2, 0), EnumModelBase::KeyRole);
}

CoapQueryController::~CoapQueryController()
{
    delete timer;
}


void CoapQueryController::timeout()
{
    setCounter(counter_ + 1);
}

void CoapQueryController::go(const QAbstractListModel* model, const CoapQueryData* cqd)
{
    QHash<int, QByteArray> roles = model->roleNames();
    auto rowCount = model->rowCount();
    int indexRole = -1,
            valueRole = -1;

    // DEBT: Probably there is a helper function that does this, creates a new hash/QHash
    for(auto j = roles.begin(); j != roles.end(); ++j)
    {
        const int i = j.key();
        const QByteArray& role = j.value();

        if(role == "value")
        {
            valueRole = i;
        }
        else if(role == "index")
        {
            indexRole = i;
        }
    }

    qInfo() << "CoapQueryController::go rowCount=" << rowCount << ", " << cqd->port();

    QUrl url;

    url.setHost(cqd->host());
    url.setPort(cqd->port());
    url.setPath("v1/test");

    QCoapRequest request(url, QCoapMessage::Type::NonConfirmable);

    for(int row = 0; row < rowCount; ++row)
    {
        QModelIndex i = model->index(row);

        // consider using 'itemData'
        //QVariant d = model->data(i, roles. ["index"]);
        QMap<int, QVariant> d = model->itemData(i);

        for(auto j = d.begin(); j != d.end(); ++j)
        {
            //const QByteArray& role = roles[j.key()];

            //qDebug() << "TEST: " << role.toStdString().data();

            if(j.key() == valueRole)
            {
                qDebug() << "value: " << j.value();
            }
            else if(j.key() == indexRole)
            {
            }
        }

        // DEBT: Somehow kvp doesn't match 'j' type above.  Look into why
        /*
        for(auto kvp : d)
        {
            int key = kvp.key();
        } */
    }
}
