#pragma once

#include <QAbstractItemModel>
#include <qqml.h>
#include <QObject>

class Utility : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    //explicit Utility(QObject *parent = nullptr);
    Utility(QObject* parent = nullptr) : QObject(parent) {}
    //Utility() : QObject() {}

    Q_INVOKABLE QString test() const;

    // NOTE: It's odd some scenarios call for a pointer and others a reference.
    // If one uses QAbstractItemModel&, a vague segfault occurs
    Q_INVOKABLE QVariant get(const QAbstractItemModel* model,
                             const QByteArray& roleName,
                             int row,
                             int column = 0,
                             const QModelIndex& parent = QModelIndex()) const;

//signals:
};

// DEBT: I looked a lot, but couldn't find anything like this, so rolling my own.
// I still think one is floating around out there somewhere
class EnumModelBase : public QAbstractItemModel
{
    Q_OBJECT

protected:
    QMetaEnum metaEnum;

public:
    EnumModelBase(const QMetaEnum& metaEnum, QObject* parent = nullptr) :
        QAbstractItemModel(parent),
        metaEnum{metaEnum}
    {
    }

    enum Roles {
        KeyRole = Qt::UserRole + 1,
        ValueRole
    };


    int rowCount(const QModelIndex& = QModelIndex()) const override
    {
        return metaEnum.keyCount();
    }

    int columnCount(const QModelIndex& = QModelIndex()) const override
    {
        return 1;

    }

    QVariant data(const QModelIndex &index, int role) const override;

    QModelIndex index(int row, int column,
                              const QModelIndex& = QModelIndex()) const override
    {
        return createIndex(row, column);
    }

    QModelIndex parent(const QModelIndex&) const override
    {
        return QModelIndex();
    }

    QHash<int, QByteArray> roleNames() const override;
};


// "template classes not supported by Q_OBJECT"
// and probably QML_SINGLETON etc as well
/*
template <typename TEnum>
class EnumModel : public EnumModelBase
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    EnumModel(QObject* parent = nullptr) :
        EnumModelBase(QMetaEnum::fromType<TEnum>(), parent) {}

}; */
