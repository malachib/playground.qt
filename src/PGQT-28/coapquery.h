#ifndef COAPQUERY_H
#define COAPQUERY_H

#include <QCoapClient>
#include <QCoapReply>

#include <QAbstractListModel>
#include <QObject>
#include <QQuickItem>
#include <QTimer>
#include <qqml.h>

class CoapQueryData : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    int port_ = QtCoap::Port::DefaultPort;
    QString host_;
    QtCoap::Method method_;
    QByteArray payload_;

    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QtCoap::Method method READ method WRITE setMethod NOTIFY methodChanged)
    Q_PROPERTY(QByteArray payload READ payload WRITE setPayload NOTIFY payloadChanged)

public:
    int port() const { return port_; }

    void setPort(int v)
    {
        // This comparison is necessary to prevent
        // "Binding loop detected for property "value" "
        if(port_ == v) return;

        port_ = v;
        emit portChanged();
    }

    QString host() const { return host_; }
    void setHost(const QString& v)
    {
        if(host_ == v) return;

        host_ = v;
        emit hostChanged();
    }

    QtCoap::Method method() const { return method_; }
    void setMethod(QtCoap::Method v)
    {
        if(method_ == v) return;

        method_ = v;
        emit methodChanged();
    }

    const QByteArray& payload() const { return payload_; }
    void setPayload(const QByteArray& v)
    {
        if(payload_ == v) return;       // DEBT: Needs optimization

        payload_ = v;
        emit payloadChanged();
    }

signals:
    void portChanged();
    void hostChanged();
    void methodChanged();
    void payloadChanged();
};


class CoapQueryController : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QTimer* timer;
    int counter_;
    QCoapClient client;

    Q_PROPERTY(int counter READ counter WRITE setCounter NOTIFY counterChanged)

public:
    explicit CoapQueryController(QObject *parent = nullptr);
    ~CoapQueryController();

    int counter() const { return counter_; }
    // DEBT: Making writable only for reference.  Remove writability once we get some
    // other writable properties for reference
    void setCounter(int counter)
    {
        counter_ = counter;
        emit counterChanged(counter);
    }

    void timeout();

    Q_INVOKABLE void go(const QAbstractListModel* model, const CoapQueryData* data);

signals:
    void counterChanged(int);
};

#endif // COAPQUERY_H
