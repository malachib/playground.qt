#ifndef COAPOPTIONDATA_H
#define COAPOPTIONDATA_H

#include <QAbstractListModel>
#include <QCoapOption>

#include <QObject>
#include <QQuickItem>

#include <QCoapReply>

#include "utility.h"

const char* description(QCoapOption::OptionName);

class CoapOptionData : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QCoapOption::OptionName optionType_;

    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)

public:
    explicit CoapOptionData(QObject *parent = nullptr);

    QString name() const
    {
        return description(optionType_);
    }

    int type() const { return optionType_; }
    void setType(int t)
    {
        optionType_ = (QCoapOption::OptionName)t;
        emit typeChanged();
        emit nameChanged();
    }

signals:
    void typeChanged();
    void nameChanged();
};

class CoapOptionElement : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QCoapOption::OptionName type_;

    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(int type READ type CONSTANT)

public:
    explicit CoapOptionElement(QCoapOption::OptionName n, QObject *parent = nullptr);

    QString name() const
    {
        return description(type_);
    }

    int type() const
    {
        return type_;
    }
};

// Guidance from
// https://stackoverflow.com/questions/36742982/how-to-access-value-from-a-qml-combo-box-which-is-fetching-values-from-table-mod
// https://wiki.qt.io/How_to_use_QAbstractListModel

// Raw available options
class CoapOptionsModel : public QAbstractItemModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    enum Roles {
        NameRole = Qt::UserRole + 1,
        TypeRole
    };

    CoapOptionsModel(QObject* parent = nullptr) :
        QAbstractItemModel(parent) {}

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QModelIndex index(int row, int column,
                              const QModelIndex& = QModelIndex()) const override
    {
        // https://doc.qt.io/qt-6/qabstractitemmodel.html#index
        // "When reimplementing this function in a subclass, call createIndex() to generate model indexes that other components can use to refer to items in your model."
        return createIndex(row, column);
    }

    QModelIndex parent(const QModelIndex&) const override
    {
        // https://doc.qt.io/qt-6/qabstractitemmodel.html#parent
        // "If the item has no parent, an invalid QModelIndex is returned."
        return QModelIndex();
    }

    QHash<int, QByteArray> roleNames() const override;
};

// Which options we have selected and what values are in them
class SelectedCoapOptionsModel : public QAbstractListModel
{

};


class CoapMethodEnumModel : public EnumModelBase
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    CoapMethodEnumModel(QObject* parent = nullptr) :
        EnumModelBase(QMetaEnum::fromType<QtCoap::Method>(), parent)
    {}
};

#endif // COAPOPTIONDATA_H
