#include "coapoptiondata.h"

CoapOptionData::CoapOptionData(QObject *parent)
    : QObject{parent}
{

}

const char* description(QCoapOption::OptionName n)
{
    switch(n)
    {
        case QCoapOption::IfMatch: return "IfMatch";
        case QCoapOption::IfNoneMatch: return "IfNoneMatch";
        case QCoapOption::Etag: return "Etag";

        case QCoapOption::UriHost: return "UriHost";
        case QCoapOption::UriPath: return "UriPath";
        case QCoapOption::UriPort: return "UriPort";
        case QCoapOption::ContentFormat: return "ContentFormat";

        default: return nullptr;
    }
}

CoapOptionElement::CoapOptionElement(QCoapOption::OptionName n, QObject *parent)
    : QObject{parent},
      type_{n}
{

}

// column 0 is numeric type,
// column 1 is human readable name

struct CoapOptionMetadata
{
    const QCoapOption::OptionName name;
    const char* description;
    // DEBT: Use actual QtCoap enum for this.
    // for the time being, it's string (0), opaque (1) or int (2), or nothing (-1)
    int type;

    CoapOptionMetadata(QCoapOption::OptionName name, int type = -1) :
        name(name),
        description(::description(name)),
        type(type)
    {}
};

static CoapOptionMetadata available_options[]
{
    CoapOptionMetadata(QCoapOption::IfMatch),
    CoapOptionMetadata(QCoapOption::UriHost),
    CoapOptionMetadata(QCoapOption::Etag),
    CoapOptionMetadata(QCoapOption::IfNoneMatch),
    CoapOptionMetadata(QCoapOption::UriPort),
    CoapOptionMetadata(QCoapOption::UriPath),
    CoapOptionMetadata(QCoapOption::ContentFormat),
};

static constexpr int rowCount()
{
#if __cplusplus >= 201703L
    return std::size(available_options);
#else
    return sizeof(available_options) / sizeof(CoapOptionMetadata);
#endif
}


int CoapOptionsModel::rowCount(const QModelIndex&) const
{
    return ::rowCount();
}

int CoapOptionsModel::columnCount(const QModelIndex&) const
{
    return 1;
}


QVariant CoapOptionsModel::data(const QModelIndex& index, int role) const
{
    int row = index.row();

    if(row < 0 || row > ::rowCount())
    {
        // Typically we'd only need to check against -1, but data binding might push
        // something else bad in here.
        return QVariant();
    }

    CoapOptionMetadata& d = available_options[row];

    switch(role)
    {
        case TypeRole:  return d.type;
        case NameRole:  return QString(d.description);
    }

    return QString("");
}


QHash<int, QByteArray> CoapOptionsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[NameRole] = "name";
    roles[TypeRole] = "type";

    return roles;
}
