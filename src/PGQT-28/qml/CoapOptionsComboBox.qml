import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

import PGQT_28

ComboBox {
    id: optionComboBox
    model: CoapOptionsModel
    textRole: "name"
}
