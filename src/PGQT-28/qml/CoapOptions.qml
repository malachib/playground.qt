import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

import PGQT_28

Item {
    Layout.alignment: Qt.AlignTop | Qt.AlignLeft
    //anchors.fill: parent

    Rectangle {
        id: debugRectangle
        color: 'red'
    }

    property ListModel options: options
    property ListView view: optionsListView

    ListModel {
        id: options

        ListElement {
            index: 0
            value: ""
        }

        ListElement {
            index: 1
            value: ""
        }

        ListElement {
            index: 2
            value: "abc"
        }

        ListElement {
            index: 3
            value: "abc"
        }
    }

    Component {
        id: listViewDelegate

        Rectangle {
            id: rect
            width: row.width + 2    // +2 gets us a bit of extra selection bar visibility
            height: row.height
            color: ListView.isCurrentItem ? "gray" : "white"

            required property int index
            required property string value
            required property variant model

            RowLayout {
                id: row
                opacity: 0.8

                CoapOptionsComboBox {
                    id: optionComboBox

                    Layout.fillWidth: true
                    Layout.preferredWidth: 100
                    height: textEdit.height

                    currentIndex: index

                    onCurrentIndexChanged: rect.model.index = currentIndex

                    // "Invalid grouped property access: Property "model" with type "QVariant", which is not a value type"
                    /*
                    Binding {
                        rect.model.number: currentIndex
                    } */
                }

                /*
                Text {
                    Layout.preferredWidth: 130

                    //text: name + " (" + optionComboBox.currentIndex + ")"
                    // DEBT: This is a very roundabout way to get at things, too much I think
                    // guidance from
                    // https://forum.qt.io/topic/55987/qml-abstractitemmodel-access-by-row-and-role/4
                    text: " (" +
                          // gets the textual name of the selected available option
                          rawOptions.data(
                            rawOptions.index(optionComboBox.currentIndex, 0), 257) +
                          ")"
                } */

                // guidance from https://stackoverflow.com/questions/26170972/textedit-line-background-color
                FocusScope {
                    id: root

                    Layout.preferredWidth: 200
                    Layout.maximumWidth: 250
                    Layout.fillWidth: true

                    height: textEdit.height

                    //property alias text: textEdit.text

                    Rectangle {
                        color: root.focus ? 'plum' : 'yellow'
                        height: textEdit.cursorRectangle.height
                        width: root.width
                        y: textEdit.cursorRectangle.y
                    }

                    TextInput {
                        width: parent.width
                        id: textEdit
                        focus: true
                        text: value

                        onTextChanged: model.value = text
                    }

                }
            }
        }
    }

    RowLayout {
        spacing: 6

        ListView {
            id: optionsListView

            Layout.fillWidth: true
            Layout.minimumWidth: 200
            Layout.preferredWidth: 350  // NOTE: row.width doesn't work here, presumably because it's a semi-runtime creature

            Layout.preferredHeight: 400

            model: options
            delegate: listViewDelegate
        }

        ColumnLayout {

            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true

            Button {
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true
                Layout.minimumWidth: 20
                Layout.preferredWidth: 50

                text: "+"
                onClicked: options.append({"name": "test", "type": 10})
            }

            Button {
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true
                Layout.minimumWidth: 20
                Layout.preferredWidth: 50

                text: "-"
                onClicked: options.remove(optionsListView.currentIndex)
            }

        }

        /*
        ListView {
            id: debugListView

            Layout.fillWidth: true
            Layout.minimumWidth: 75
            Layout.preferredWidth: 200
            Layout.preferredHeight: optionsListView.height

            model: options
            delegate: Component {
                Text {

                    required property int index
                    required property string value

                    text: Utility.get(CoapOptionsModel, "name", index) + " (" + index + "): " + value
                    //text: '(' + Utility.test() + index + "): " + value
                    //text: '(' + index + "): " + value
                }
            }
        } */
    }
}
