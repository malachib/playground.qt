import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

import PGQT_28

ListView {
    id: debugListView

    Layout.fillWidth: true
    Layout.minimumWidth: 75
    Layout.preferredWidth: 200

    delegate: Component {
        Text {

            required property int index
            required property string value

            text: Utility.get(CoapOptionsModel, "name", index) + " (" + index + "): " + value
            //text: '(' + Utility.test() + index + "): " + value
            //text: '(' + index + "): " + value
        }
    }
}
