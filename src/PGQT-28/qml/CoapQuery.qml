import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.10

import PGQT_28

RowLayout {
    anchors.fill: parent
    spacing: 2

    CoapQueryData {
        id: coapQueryData
    }

    GridLayout {
        Layout.alignment: Qt.AlignTop
        Layout.fillWidth: true
        Layout.minimumWidth: 50
        Layout.preferredWidth: 200

        columns: 2

        Text {
            text: "Host"
        }

        TextField {
            id: txtHost
            text: coapQueryData.host

            ToolTip.delay: 250
            ToolTip.timeout: 5000   // doesn't seem to be working
            ToolTip.visible: hovered
            ToolTip.text: qsTr("Without coap://")

            onTextChanged: coapQueryData.host = text
        }

        Text { text: "Port" }

        SpinBox {
            id: spinPort
            from: 1

            // Guidance from
            // https://stackoverflow.com/questions/28250710/qt-5-4-qml-prevent-binding-loop

            // NOTE: Isn't really a binding, so our runvtime init of CoapQueryData doesn't get picked up
            //Component.onCompleted: coapQueryData.port

            value: coapQueryData.port

            to: 60000

            onValueChanged: coapQueryData.port = value

            // Encountering errors:
            // QQmlExpression: Expression qrc:/PGQT_28/CoapQuery.qml:47:17 depends on non-NOTIFYable properties: QQmlBind::value
            // qrc:/PGQT_28/CoapQuery.qml:47:17: Unable to assign [undefined] to int
            /*
            Binding {
                coapQueryData.port: value
            } */
        }

        Text {
            text: "Method: "
        }

        ComboBox {
            id: cmbMethod
            // FIX: Not able to get at 'Method' enum, despite it being Q_ENUM_NS
            //model: QtCoap.Method
            model: CoapMethodEnumModel
            textRole: "key"
        }

        Text {
            text: "Payload: "
        }

        TextEdit {
            id: txtPayload
            text: coapQueryData.payload

            onEditingFinished: coapQueryData.payload = text
       }

        Text {
            text: qsTr("Counter: ") + controller.counter
        }

        Button {
            text: "Go"
            onClicked: controller.go(options.options, coapQueryData)
        }
    }

    CoapOptions {
        id: options

        Layout.fillWidth: true
        Layout.minimumWidth: 100
        Layout.preferredWidth: 500
    }

    CoapQueryController {
        id: controller
    }

    CoapOptionsDebug {
        model: options.options

        Layout.alignment: Qt.AlignTop
        Layout.fillWidth: true

        //height: options.optionsListView.height
        height: 400
    }
}
