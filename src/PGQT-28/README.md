Adapting

https://doc.qt.io/qt-6/qtcoap-simplecoapclient-example.html

# References

https://doc.qt.io/qt-6/qtcoap-index.html

# Guidance

## Prerequisites

Manual installation of https://github.com/qt/qtcoap is required.

Build the module, then in the output cmake build folder, run:

`cmake -P cmake_install.cmake`

Unconfirmed, but likely it's important to check out the exact tag
version of qtcoap which matches qt version application is targeting

At time of this writing, that's 6.3.1

## C++ integration

https://scythe-studio.com/en/blog/how-to-integrate-qml-and-c-expose-object-and-register-c-class-to-qml
