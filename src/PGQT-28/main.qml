import QtQuick

Window {
    width: 800
    height: 480
    visible: true
    title: qsTr("Hello World")

    CoapQuery {
        x: 50
        y: 50
        width: 600
        height: 300
    }
}
