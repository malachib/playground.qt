# Overview

# 1. Targets

## 1.1. Android

Primary target and goal

### 1.1.1. Design Goals

To log in using native Android Google account selector and request permission to a GCP service such as drive storage.

## 1.2. Desktop Linux

### 1.2.1. Design Goals

To see how far we can get doing aa Google OAuth as similarly as possible to 1.1.

# 2. RESERVED

# 3. Observations

## 3.1. Desktop OAuth2

Highly related, but not the same [1].  Still, interesting and relevant.

# 4. Troubleshooting

# References

1. https://www.qt.io/blog/2017/01/25/connecting-qt-application-google-services-using-oauth-2-0