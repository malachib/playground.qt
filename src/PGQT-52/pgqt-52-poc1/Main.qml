import QtQuick

import QtLocation

// Semi guidance from
// https://stackoverflow.com/questions/53112393/qml-openstreetmap-custom-tiles
// https://doc.qt.io/qt-6.5/location-plugin-osm.html
// https://forum.sailfishos.org/t/using-qml-map-osm-not-working/7261/6
// https://forum.qt.io/topic/103823/qt-location-osm-api-key-required/6
// https://stackoverflow.com/questions/61689939/qtlocation-osm-api-key-required
// https://bugreports.qt.io/browse/QTBUG-115742
// https://manage.thunderforest.com/dashboard

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-52: poc1")

    // TODO: See if this initializes before plugin
    property string variety2;

    Plugin {
        id: plugin
        name: "osm"

        // If adding a variety, be sure to append /
        // FIX: This doesn't work, I think PluginParameter initializes before even the property does
        // May need technique outlined here https://stackoverflow.com/questions/59801435/dynamically-change-custom-host-url-of-osm-plugin-in-a-qml-map
        //property string variety: "cycle/"
        //property string api_key: ""

        // Somehow needs to be ala
        // "http://a.tile.thunderforest.com/cycle/%z/%x/%y.png?apikey=YOUR_API_KEY"
        PluginParameter {
            name: "osm.mapping.custom.host"

            // NOTE: OSM plugin auto-appends x/y/z if .png isn't suffix, and that screws up apikey which silently
            // fails authentication (only Wireshark revealed it)
            value: "http://tile.thunderforest.com/landscape/%z/%x/%y.png?apikey=1cdc3e9e4a08407d8dac0c4effc3b784&fake=.png"

            // NOTE: This actually works and flips into landscape mode
            //value: "http://tile.thunderforest.com/landscape/"
            //value: "http://tile.thunderforest.com/landscape/%z/%x/%y.png"

            // NOTE: Goes into 100% cached mode
            //value: "XYZ"
        }

        PluginParameter {
            name: "osm.mapping.copyright"
            value: "(c) 2023 PGQT-51: poc1"
        }
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: plugin

        activeMapType: supportedMapTypes[supportedMapTypes.length - 1]

        DragHandler {
            id: drag
            onTranslationChanged: (delta) => map.pan(-delta.x, -delta.y)
        }
    }
}
