cmake_minimum_required(VERSION 3.16)

project(PGQT_61_sibling VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(QT_QML_OUTPUT_DIRECTORY  ${CMAKE_BINARY_DIR})

find_package(Qt6 6.4 COMPONENTS Quick REQUIRED)

if(Qt6Core_VERSION_MINOR GREATER_EQUAL 5)
    qt_policy(SET QTP0001 NEW)
endif()

qt_add_library(PGQT_61_brother STATIC)
qt_add_qml_module(PGQT_61_brother
    URI PGQT_61_brother
    VERSION 1.0
    QML_FILES BrotherItem.qml
    # DEBT: Needed for < Qt6.5 compat
    RESOURCE_PREFIX /qt/qml
)

add_library(PGQT-61::brother ALIAS PGQT_61_brotherplugin)

# TODO: Try URI "PGQT-61/sister" notwithstanding concern 4.2.

qt_add_library(PGQT_61_sister STATIC)
qt_add_qml_module(PGQT_61_sister
    URI PGQT_61_sister
    VERSION 1.0
    QML_FILES SisterItem.qml
    # DEBT: Needed for < Qt6.5 compat
    RESOURCE_PREFIX /qt/qml
)


add_library(PGQT-61::sister ALIAS PGQT_61_sisterplugin)

add_library(${PROJECT_NAME} STATIC)
target_link_libraries(${PROJECT_NAME} PUBLIC
    PGQT-61::brother PGQT-61::sister
)

add_library(PGQT-61::sibling ALIAS ${PROJECT_NAME})
