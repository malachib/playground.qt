import QtQuick
import QtQuick.Layouts

import PGQT_61_parent as Parent
import PGQT_61_child as Child

Item {
    id: root

    GridLayout {
        rows: 2
        columns: 2

        Parent.MyItem {
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
        }

        Parent.MyItemControls {
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
        }

        Child.MyItem {
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
        }

        Child.MyItemControls {
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
        }
    }
}
