import QtQuick
import QtQuick.Layouts

import PGQT_61_parent as Parent
import PGQT_61_child as Child
import PGQT_61_brother as Brother
import PGQT_61_sister as Sister

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-61")

    GridLayout {
        columns: 4
        rows: 3
        rowSpacing: 20
        columnSpacing: 20

        Parent.MyItem {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Parent.MyItemControls {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Child.MyItem {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Child.MyItemControls {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Parent.MyItem2 {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Brother.BrotherItem {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }

        Sister.SisterItem {
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }
    }
}
