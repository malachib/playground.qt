# Overview

# 1. Targets & Goals

Not specified, but implicitly we prioritize Desktop Linux first with Android coming in second.

## 1.1. Overall Design Goals

The functional goal is real time telemetry between multiple PGQT-60 clients.
It is expected Firebase Cloud Messaging is the way we'll do this

## 1.2. poc1

## 1.3. poc2

# 2. RESERVED

# 3. Observations & Opinions

So far this all seems *really involved* if C++ is in the picture.  And, C++ looks supported, but just barely

## 3.1. Overlap with PGQT-54 and PGQT-64

What is unique about this poc is that we expect a real time push down from Firebase.
Neither PGQT-54 or PGQT-64 specifically address that.  However, PGQT-64 in particular
might employ gRPC (or similar) which IIRC is set up with that capability.

## 3.2. Firebase official SDK

Noteworthy is Android support [2.1]
CMakeLists.txt is present.  Cool.

A little concerned it seems that C++ FCM might only be on iOS and Android [2.3.2]
This is corroborated by [6.1]

Old example [2.4] is interesting but it looks abandoned

## 3.3. Firestore vs FCM

There seems to be an overlap in behaviors of these two.  Need more research.

## 3.4. QtFirebase (Larpon)

Located at https://github.com/Larpon/QtFirebase it appears to be just outdated enough to be hung onto Qt5.  Will avoid, for now

# 4. RESERVED

# References

1. https://github.com/Larpon/QtFirebase
2. https://firebase.google.com/
    1. https://github.com/firebase/firebase-cpp-sdk
    2. https://firebase.google.com/docs/cpp/setup?platform=android
    3. https://firebase.google.com/docs/cloud-messaging
        1. https://firebase.google.com/docs/reference/cpp/namespace/firebase/messaging
        2. https://firebase.google.com/docs/cloud-messaging/cpp/client
    4. https://github.com/firebase/quickstart-cpp
3. https://stackoverflow.com/questions/47794997/how-can-i-work-with-fcm-using-cloud-firestore
4. https://github.com/qt/qtcloudmessaging
5. https://www.reddit.com/r/QtFramework/comments/12ce0l8/how_to_add_firebase_push_notification_in_qt_qml/
6. https://medium.com/androiddevelopers/how-to-integrate-firebase-into-your-native-c-android-game-aa0d76eeba23
    1. https://medium.com/firebase-developers/how-to-add-firebase-to-your-c-desktop-game-4a177703186e