#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <lua.hpp>

// Works great!  I thought I read somewhere LuaJIT runs with WASM, but no luck there it wasn't hardware
// fpu support which appears to be at odds with WASM here.


const char code[] =
R"###(

function f (x, y)
    return (x + y)
end

)###";

void init()
{
    lua_State* L = luaL_newstate();

    luaL_openlibs(L);
    int ret = luaL_loadstring(L, code);

    qDebug() << "init: phase1" << ret;

    // "priming" call
    ret = lua_pcall(L, 0, 0, 0);

    lua_getglobal(L, "f");
    lua_pushnumber(L, 3);
    lua_pushnumber(L, 4);

    //luaL_execresult(L, 0);
    // 'print' worked, but this one is unhappy
    ret = lua_pcall(L, 2, 1, 0);

    if(ret != 0)
    {
        qWarning() << "init: lua_pcall err" << lua_tostring(L, -1);
        return;
    }

    int z = lua_tonumber(L, -1);
    lua_pop(L, 1);// pop returned value

    qDebug() << "init: phase2" << ret << "z:" << z;

    lua_close(L);
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    init();

    QQmlApplicationEngine engine;
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("PGQT-63", "Main");

    return app.exec();
}
