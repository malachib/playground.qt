import QtQuick 2.0

SequentialAnimation {
    id: wiggleAnimation
    running: false

    PropertyAnimation {
            to: 1
            easing {
                type: Easing.OutBack
                overshoot: 10
            }
            duration: 1000
    }

    PropertyAnimation {
            to: -1
            easing {
                type: Easing.InBack
                overshoot: 10
            }
            duration: 1000
    }

    loops: Animation.Infinite
}
