import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import "string_formatting.js" as StringFormatting

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property int countdownInitializer: 10
    property int countdown: 10 // TODO: assign this once I know it's not BINDING it

    Timer {
        id: appTimer
    }

    onBeforeRendering: {
        //swipeView.currentIndex = 2
    }

    Component.onCompleted: {
        console.log("Loaded")
        appTimer.interval = 1000;
        appTimer.repeat = true;
        appTimer.triggered.connect(function () {
            if(--countdown == 0)
            {
                console.log("Complete")
                Qt.exit(0)
            }
        })
        appTimer.start()
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        /*
        Shader1 {
            id: shader1
        } */

        /*
        Shader2 {

        } */

        Page {
            id: testPage
            Rectangle {
                anchors.fill: parent
                color: "#101010"
            }

            Component
            {
                id: greenRect

                Rectangle {
                    color: "transparent"
                    border.color: "#20FF20"
                    anchors.margins: 10
                    opacity: 0.6
                    antialiasing: true
                }
            }

            Rectangle {
                id: wiggly1
                color: "transparent"
                border.color: "#20FF20"
                anchors.margins: 10
                anchors.fill: parent
                opacity: 0.5
                antialiasing: true

                WiggleAnimation on rotation {
                    id: _w1
                }
            }

            Rectangle {
                id: wiggly2
                color: "transparent"
                border.color: "#20FF20"
                anchors.margins: 10
                anchors.fill: parent
                opacity: 0.7
                rotation: -3
                antialiasing: true

                WiggleAnimation on rotation {
                    id: _w2
                }

            }

            Loader {
                sourceComponent: greenRect
                id: wiggly3
                anchors.fill: parent
                anchors.margins: 20
                WiggleAnimation on rotation { id: _w3 }
            }


            Label {
                id: countdownLabel
                text: qsTr("00:") + StringFormatting.zero_pad(countdown, 2)
                font.pointSize: 20
                font.family: "Helvetica"
                color: "gray"
                anchors.margins: {
                    //rightPadding: 200
                }

                anchors.centerIn: parent

                ParallelAnimation {
                    running: true

                    NumberAnimation {
                        target: countdownLabel
                        property: "font.pointSize"
                        to: 250
                        duration: 1000 * countdownInitializer
                        easing.type: Easing.OutQuart
                        easing.amplitude: 2.0
                        //easing.type: Easing.bezierCurve
                    }
                    NumberAnimation {
                        target: countdownLabel
                        property: "opacity"
                        to: 0.1
                        duration: 1000 * countdownInitializer
                    }
                }
            }

            Timer {
                id: timer
            }

            Component.onCompleted: {
                _w1.start()
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    timer.interval = 1000;
                    timer.repeat = false;
                    timer.triggered.connect(function () {
                        _w2.start()
                    })

                    timer.start();

                    _w3.start()

                }
            }
        }

        Page1 {
        }

        Page {
            Loader {
                source: "PrettyThing1.qml"
                asynchronous: true
                visible: status == Loader.Ready
                x: 100
                y: 100

            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("First")
        }
        TabButton {
            text: qsTr("Second")
        }
        TabButton {
            text: qsTr("Third")
        }
    }
}
