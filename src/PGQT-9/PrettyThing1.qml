import QtQuick 2.0

// OK honestly, this is pretty damn ugly.  I was hoping for a spirograph-like situation
// but instead I got this... interesting stuff though
Item {
    id: notSoPrettyThing

    Rectangle {
        id: redRectangle
        color: "red"
        width: 100
        height: 50
        //opacity: 0.1

        PropertyAnimation on rotation {
            running: true
            from: 0
            to: 360
            loops: Animation.Infinite
            duration: 3000
        }

        PathView
        {
            //rotation: 3
            id: pathView
            anchors.fill: parent
            //model: ContactModel {}
            model: 300

            delegate: Rectangle {
                antialiasing: true
                id: dot
                width: 2
                height: 2
                color: "black"
            }

            path: Path {
                startX: 20
                startY: 0

                PathCubic {
                    x: 180
                    y: 0
                    control1X: -10; control1Y: 90
                    control2X: 210; control2Y: 90
                }
            }
        }
    }

    Path {
        startX: 0; startY: 0
        PathQuad { x: 200; y: 0; controlX: 100; controlY: 150 }
    }

    Canvas
    {
        anchors.left: redRectangle.right
        //color: "blue"

        id:canvas
        width:320
        height:280
        property color strokeStyle:  Qt.darker(fillStyle, 1.4)
        property color fillStyle: "#b40000" // red
        property int lineWidth: lineWidthCtrl.value
        property bool fill: true
        property bool stroke: true
        property real alpha: 1.0
        //property real scale : scaleCtrl.value
        //property real rotate : rotateCtrl.value
        property real scale : 1.0
        property real rotate : 1.0

        antialiasing: true

        onLineWidthChanged:requestPaint();
        onFillChanged:requestPaint();
        onStrokeChanged:requestPaint();
        onScaleChanged:requestPaint();
        onRotateChanged:requestPaint();

        onPaint: {

            var ctx = canvas.getContext('2d');
            var originX = 85
            var originY = 75
            ctx.save();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.translate(originX, originX);
            ctx.globalAlpha = canvas.alpha;
            ctx.strokeStyle = canvas.strokeStyle;
            ctx.fillStyle = canvas.fillStyle;
            ctx.lineWidth = canvas.lineWidth;

            ctx.translate(originX, originY)
            ctx.scale(canvas.scale, canvas.scale);
            ctx.rotate(canvas.rotate);
            ctx.translate(-originX, -originY)

            ctx.beginPath();
            ctx.moveTo(75,40);
            ctx.bezierCurveTo(75,37,70,25,50,25);
            ctx.bezierCurveTo(20,25,20,62.5,20,62.5);
            ctx.bezierCurveTo(20,80,40,102,75,120);
            ctx.bezierCurveTo(110,102,130,80,130,62.5);
            ctx.bezierCurveTo(130,62.5,130,25,100,25);
            ctx.bezierCurveTo(85,25,75,37,75,40);
            ctx.closePath();
            if (canvas.fill)
                ctx.fill();
            if (canvas.stroke)
                ctx.stroke();
            ctx.restore();
        }
    }

    Rectangle {
        width: 400; height: 240
        color: "white"
        opacity: 0.5

        ListModel {
            id: appModel
            /*
            ListElement { name: "Music"; icon: "pics/AudioPlayer_48.png" }
            ListElement { name: "Movies"; icon: "pics/VideoPlayer_48.png" }
            ListElement { name: "Camera"; icon: "pics/Camera_48.png" }
            ListElement { name: "Calendar"; icon: "pics/DateBook_48.png" }
            ListElement { name: "Messaging"; icon: "pics/EMail_48.png" }
            ListElement { name: "Todo List"; icon: "pics/TodoList_48.png" }
            ListElement { name: "Contacts"; icon: "pics/AddressBook_48.png" } */
            ListElement { name: "Logo"; icon: "qt-logo.png" }
            ListElement { name: "Cloud"; icon: "cloud.jpg" }
        }

        Component {
            id: appDelegate
            Item {
                width: 100; height: 100
                scale: PathView.iconScale

                Image {
                    width: 50
                    height: 50
                    id: myIcon
                    y: 20; anchors.horizontalCenter: parent.horizontalCenter
                    source: icon
                }
                Text {
                    anchors { top: myIcon.bottom; horizontalCenter: parent.horizontalCenter }
                    text: name
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: view.currentIndex = index
                }
            }
        }

        Component {
            id: appHighlight
            Rectangle { width: 80; height: 80; color: "lightsteelblue" }
        }

        PathView {
            id: view
            anchors.fill: parent
            highlight: appHighlight
            preferredHighlightBegin: 0.5
            preferredHighlightEnd: 0.5
            focus: true
            model: appModel
            delegate: appDelegate
            path: Path {
                startX: 10
                startY: 50
                PathAttribute { name: "iconScale"; value: 0.5 }
                PathQuad { x: 200; y: 150; controlX: 50; controlY: 200 }
                PathAttribute { name: "iconScale"; value: 1.0 }
                PathQuad { x: 390; y: 50; controlX: 350; controlY: 200 }
                PathAttribute { name: "iconScale"; value: 0.5 }
            }
        }
    }
}
