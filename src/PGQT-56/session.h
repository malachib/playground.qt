#pragma once

#include <QObject>
#include <QtQml>

class Session : public QObject
{
    Q_OBJECT
    //QML_ELEMENT // NOTE: I don't think I am really using this, since we are doing a singleton registration anyway

    Q_PROPERTY(double value READ value CONSTANT)

public:
    Session(QObject* parent) : QObject(parent) {}

    double value() const;
};
