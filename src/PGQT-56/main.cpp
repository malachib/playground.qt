#include <QJSEngine>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "session.h"

// All works:
// 1. WASM
// 2. Android
// 3. GCC Linux

void init()
{
    QJSEngine engine;
    QJSValue v = engine.evaluate("1 + 2");
    qDebug() << "init: v=" << v.toNumber();
}

double Session::value() const
{
    QJSEngine engine;
    QJSValue v = engine.evaluate("1 + 2");
    return v.toNumber();
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    init();

    QQmlApplicationEngine engine;

    auto session = new Session(engine.rootContext());

    qmlRegisterSingletonInstance("PGQT_56", 1, 0, "Session", session);

    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("PGQT-56", "Main");

    return app.exec();
}
