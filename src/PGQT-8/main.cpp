#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QShortcutEvent>
#include <QShortcut>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    //new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), app.primaryScreen(), SLOT(close()));

    return app.exec();
}
