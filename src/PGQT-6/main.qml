import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.15

import "qml"

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-6: Hello World")

    ColumnLayout {

        anchors.fill: parent

        TabBar {
            Layout.fillWidth: true
            id: bar

            TabButton {
                text: "Sprites"
            }

            TabButton {
                text: "Images"
            }

            TabButton {
                text: "Vectors"
            }
        }


        StackLayout {
            Layout.fillWidth: true

            currentIndex: bar.currentIndex

            Item {
                id: spriteTab
            }
            Images {
                id: imagesTab
            }
            Item {
                id: vectorsTab
            }
        }

    }
}
