import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.15

Item {

    Image {
        anchors.fill: parent
        source: "qrc:/images/qt-logo.png"

        fillMode: Image.PreserveAspectFit

        SequentialAnimation on anchors.margins {
            loops: Animation.Infinite
            PropertyAnimation {
                to: 40
                easing.type: Easing.InOutQuad
                duration: 1000
            }
            PropertyAnimation {
                to: 0
                easing.type: Easing.InOutQuad
                duration: 1000
            }
        }
    }
}
