# Unmanaged flavor

From
https://qmlnet.github.io/setup/unmanaged-hosting/

Updated by:
https://github.com/qmlnet/qmlnet/tree/develop/samples/hosting

## Issues

Somehow I'm having trouble excluding the Qml.Net folder during poc2.csproj compilation

## Things to know

### MINGW

Has issue

```
D:/Qt/Tools/mingw810_64/bin/../lib/gcc/x86_64-w64-mingw32/8.1.0/../../../../x86_64-w64-mingw32/bin/as.exe: debug\QQmlApplicationEngine.o: too many sections (54000)
C:\Users\Malachi\AppData\Local\Temp\cc2EKfU6.s: Assembler messages:
C:\Users\Malachi\AppData\Local\Temp\cc2EKfU6.s: Fatal error: can't write 23 bytes to section .text of debug\QQmlApplicationEngine.o: 'File too big'
D:/Qt/Tools/mingw810_64/bin/../lib/gcc/x86_64-w64-mingw32/8.1.0/../../../../x86_64-w64-mingw32/bin/as.exe: debug\QQmlApplicationEngine.o: too many sections (54000)
C:\Users\Malachi\AppData\Local\Temp\cc2EKfU6.s: Fatal error: can't close debug\QQmlApplicationEngine.o: File too big
```