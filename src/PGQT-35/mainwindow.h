#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "cards/deck.h"

#include <games/matchcontrollercore.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void initLayout(QWidget& l);

    games::Base* game;
    cards::AppState appState;

    void initMatch();
    void initController(games::ControllerBase* controller);

public slots:
    void newCardsPack();
private slots:
    void on_checkPlainBack_stateChanged(int arg1);
    void cardsBlockStateChange(bool blocked);
    void matchStateChange(games::MatchControllerCore::States);
};
#endif // MAINWINDOW_H
