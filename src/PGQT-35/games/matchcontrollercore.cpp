#include "games/matchcontrollercore.h"
#include "cards/util.h"
#include <utility/statemachine.hpp>

// DEBT: Phase out any Qt dependency here - almost there
#include <QDebug>

namespace games {

void initMatch(cards::GameModel& gameModel, games::MatchModel& model)
{
    gameModel.create("match", &model);
    gameModel.deal(model, 52, false);
    model.sort();
}



bool MatchControllerCore::clear_invalid()
{
    if(!assert_state(States::invalid)) return false;

    switch(substate_.invalid)
    {
        case InvalidStates::already_showing:
            // Clear invalid state and let them try a 2nd guess again
            state(States::first_guess);
            return true;

        case InvalidStates::already_matched:
            // Clear invalid already-matched state and let them try a first guess again
            set_to_idle();
            return true;

        default:
            return false;
    }
}



bool MatchControllerCore::first_guess(const Card::Location& location)
{
    if(!assert_state(States::idle)) return false;

    if(model.isAlreadyMatched(id_at(location)))
    {
        state(States::invalid, InvalidStates::already_matched);
        return true;
    }

    first_guess_location = location;
    state(States::first_guess);
    return true;
}


// NOTE: Consider breaking out second_guess from is_match, since
// this low level state machine can't notify of inbetween state
// changes
bool MatchControllerCore::second_guess(const Card::Location& location)
{
    if(is_invalid()) return true;
    if(!assert_state(States::first_guess)) return false;

    if(location == first_guess_location)
    {
        state(States::invalid, InvalidStates::already_showing);
        return true;
    }
    if(model.isAlreadyMatched(id_at(location)))
    {
        state(States::invalid, InvalidStates::already_showing);
        return true;
    }

    model.addGuess();
    second_guess_location = location;
    state(States::second_guess);

    return true;
}


// Not yet used, not sure I want to commit to queuing 2nd guess data also
bool MatchControllerCore::match()
{
    if(is_invalid()) return true;
    if(!assert_state(States::second_guess)) return false;

    const Card::Identity& first_guess = model.at(first_guess_location.position).identity;
    const Card::Identity& second_guess = model.at(second_guess_location.position).identity;
    bool matched = MatchModel::is_match(first_guess, second_guess);

    state(matched ? States::matched : States::not_matched);

    if(matched)
        model.addMatched(first_guess, second_guess);

    return true;
}


bool MatchControllerCore::reset_guessed()
{
    if(is_invalid()) return true;
    if(!assert_state(States::not_matched)) return false;

    model.flip(first_guess_location.position);
    model.flip(second_guess_location.position);

    set_to_idle();

    return true;
}



}
