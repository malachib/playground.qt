#pragma once

#include <QDebug>

#include <cards/deck.h>

// DEBT: Put this elsewhere
inline QDebug operator <<(QDebug debug, const cards::Card::Identity& id)
{
    QDebugStateSaver saver(debug);
    debug.noquote() << QString::fromStdString(to_string(id));
    return debug;
}


