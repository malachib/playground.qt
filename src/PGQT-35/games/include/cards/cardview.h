// See README.md for references

#pragma once

#include "cards_pack_view.h"
#include <cards/deck.h>

#include "appstate.h"
#include "util.h"

#include <QPushButton>
#include <QVariant>

namespace cards {

// Anticipating non-board views into card too, like private player hands or deck facing up for example
// This is base class for widgets namespace flavor
struct CardView : public QObject
{
    typedef cards::Card Card;

    Q_OBJECT

protected:
    cards::AppState& appState;

    CardView(cards::AppState& appState, QObject* parent = nullptr) : QObject(parent),
        appState{appState}
    {

    }

    inline CardsPackViewModel& cardsPack() const { return appState.cardsPack; }

public:
    // Since cards can move around in the deck model, draw() may be called manually
    // when that happens (i.e. sort, reset, shuffle, etc)
    // NOTE: For models which themselves generate notifications, one SHOULD NOT call this
    // but instead hook UI to model notifications directly
    virtual void draw() = 0;

signals:
    void clicked(Card::Location location);
};

}
