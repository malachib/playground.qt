#pragma once

#include <QObject>
#include <QVariant>

#include <cards/deck.h>
#include "cards_pack_view.h"
#include <algorithm>
#include <random>

namespace cards {

struct AppState : QObject
{
    Q_OBJECT

    // Debug tools
    bool iconMode_ = false;
    bool forceCardsUp_ = false;

    Q_PROPERTY(bool iconMode MEMBER iconMode_ NOTIFY iconModeChanged)
    Q_PROPERTY(bool forceCardsUp MEMBER forceCardsUp_ NOTIFY forceCardsUpChanged)

    // Production use
    bool blockCardFlip_ = false;
    Q_PROPERTY(bool blockCardFlip MEMBER blockCardFlip_ NOTIFY blockCardFlipChanged)

public:
    CardsPackViewModel cardsPack;
    // DEBT: UNUSED - doing operations like reset, shuffle, etc. here in the deck proper
    // might let us move below 'board' out to the particular game controllers/models
    std::vector<Card> deck;

    bool iconMode() const { return iconMode_; }
    bool forceCardsUp() const { return forceCardsUp_; }
    bool blockCardFlip() const { return blockCardFlip_; }

    void setIconMode(bool v) { setProperty("iconMode", v); }
    void setForceCardsUp(bool v) { setProperty("forceCardsUp", v); }
    void setBlockCardFlip(bool v) { setProperty("blockCardFlip", v); }

signals:
    void iconModeChanged();
    void forceCardsUpChanged();
    void blockCardFlipChanged(bool blocked);
};

}
