#pragma once

#include <QObject>

#include <cards/cardsmodel.h>
#include <cards/container.h>

namespace cards {

struct QtModelNotifier :
        QObject,
        cards::CardsModel::Notifier
{
    Q_OBJECT

    void* context_;

public:
    QtModelNotifier(void* context = nullptr, QObject* parent = nullptr) :
        QObject(parent),
        context_{context}
    {}

signals:
    void allUpdated();
    void changed(int position, int size, ModelChanges type);

public:
    void update() override
    {
        emit allUpdated();
    }

    void change(int position, int size, ModelChanges type) override
    {
        emit changed(position, size, type);
    }
};


struct QtGameModelNotifier :
        QObject,
        GameModel::Notifier
{
    Q_OBJECT

private:
    void addSection(const ContainerModel& container, int section) override;

public:
    QtGameModelNotifier(QObject* parent = nullptr);

    std::shared_ptr<CardsModel::Notifier> createChild(void* context = nullptr) override;

    // DEBT: A little confusing, 'gameModelNotifier' is the 1:1 notifier
    // UNUSED
    void connectGameModel(QtModelNotifier& gameModelNotifier);
    void connectContainer(QtModelNotifier& containerNotifier);

private slots:
    // UNUSED
    void gameModelChanged(int pos, int size, ModelChanges type);
    void containerChanged(int pos, int size, ModelChanges type);

signals:
    void containerAdded(const ContainerModel& container);
    void sectionAdded(const ContainerModel& container, int section);
    void cardsUpdated(const ContainerModel& container, int section);
    void cardStateUpdated(Card::Location location, const Card& card);
};

}
