#pragma once

#include "../cardview.h"
#include <cards/cardsmodel.h>

#include <QDebug>

namespace cards { namespace widgets {

namespace literals {

constexpr const char* boardPosition = "boardPosition";

}


// Anticipating non-board views into card too, like private player hands or deck facing up for example
struct CardViewBase : public cards::CardView
{
    Q_OBJECT

protected:
    QPushButton* button;

    CardViewBase(cards::AppState& appState, QPushButton* button, QObject* parent) :
        cards::CardView(appState, parent),
        button{button}
    {

    }

    // internal call to manually update text of button from specified card.  Used
    // for non-icon diagnostic mode
    void updateText(const Card& card);

    // DEBT: Was named 'draw' but that collides with inherited 'draw()'
    void draw_card(const Card& card);
};



// Represents a view into the card at a particular position on a board
struct CardView : public CardViewBase
{
    typedef CardViewBase base_type;

    Q_OBJECT

protected:
    CardsModel& model_;

    void connectClick()
    {
        connect(button, &QAbstractButton::clicked, this, &CardView::click);
    }

public:
    // false = mode where CardView itself actually flips card state
    // true = where mechanism listening for our special 'clicked' signal changes card state
    static const bool legacyMode = false;

    CardView(cards::AppState& appState, CardsModel& model, int boardPosition, QPushButton* button, bool _legacyMode = false) :
        CardViewBase(appState, button, button->parent()),
        model_{model}
    {
        button->setProperty(literals::boardPosition, boardPosition);

        if(!_legacyMode)
            connectClick();
    }

    std::size_t boardPosition() const
    {
        return button->property(literals::boardPosition).toInt();
    }

    // Check to see if there is a card at this position.  For later on scenarios
    // where multiple boards are involved and number of cards can come and go
    // In that case, however, we may expect a controller to remove the entire button
    bool empty() const { return model_.card_count() < boardPosition(); }

    const Card& card() const { return model_.at(boardPosition()); }

    // Since cards can move around in the deck, draw() must be called manually
    // when that happens (i.e. sort, reset, shuffle, etc)
    void draw() override;

private slots:
    void click();
};


struct CardViewLegacy : CardView
{
    Q_OBJECT

public:
    CardViewLegacy(cards::AppState& appState, CardsModel& model, int boardPosition, QPushButton* button) :
        CardView(appState, model, boardPosition, button, true)
    {
        connectFlip();
    }

private:
    // DEBT: Kinda sloppy
    bool flipConnected_;


public:
    bool flipConnected() const { return flipConnected_; }

    void connectFlip()
    {
        flipConnected_ = true;
        connect(button, &QPushButton::clicked, this, &CardViewLegacy::flip);
    }

    void disconnectFlip()
    {
        flipConnected_ = false;
        disconnect(button, &QPushButton::clicked, this, &CardViewLegacy::flip);
    }

signals:
    void flipped(bool newState);
    void showingChanged(bool newState);

public slots:
    void flip();
};


// Guidance from [4]
inline QDebug operator <<(QDebug debug, const CardView& cardView)
{
    QDebugStateSaver saver(debug);
    std::string card = to_string(cardView.card());
    debug.noquote() << QString::fromStdString(card);
    return debug;
}

}}
