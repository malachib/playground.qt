#pragma once

#include "cardview.h"

namespace cards { namespace widgets {

/// Helper method to only flip if our own flag permits it
/// DEBT: Want to move this out of ControllerBase altogether so that we are not widget-specific
template <typename F>
static cards::widgets::CardViewLegacy* try_flip(QObject* sender, F flip)
{
    auto target = (cards::widgets::CardViewLegacy*) sender;

    if(target->flipConnected())
    {
        flip(*target);
        return target;
    }

    return nullptr;
}


}}
