#pragma once

struct MainWindow;

namespace games {

template <bool widgetsMode = true>
struct Match;


struct ControllerBase;
struct Base;

namespace widgets {

struct BoardViewBase;

}

}
