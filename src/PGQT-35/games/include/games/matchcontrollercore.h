#pragma once

#include "matchmodel.h"
#include <utility/statemachine.h>

namespace games {

void initMatch(cards::GameModel& gameModel, games::MatchModel& model);


namespace match {

enum class States
{
    idle,           ///< waiting for user input
    first_guess,    ///< user just made first guess
    second_guess,   ///< user just made second guess
    matched,        ///< first + second guess matched
    not_matched,    ///< first + second guess didn't match

    invalid         ///< user clicked in a rule-breaking area
};


}


// DEBT: Needs better name.  This is the non-qt-specific portion of the controller
struct MatchControllerCore : protected StateMachine<match::States>
{
    typedef StateMachine<match::States> base_type;
    typedef cards::Card Card;
    using base_type::States;
    using base_type::Notifier;

    MatchModel& model;

    enum class MatchedStates
    {
        matched,
        not_matched
    };

    enum class GuessStates
    {
        first_guess,
        second_guess
    };

    // DEBT: already_showing serves double duty as 'already_matched' during second guess phase
    enum class InvalidStates
    {
        already_matched,        ///< user has clicked (somehow) on an already matched card during 'idle' state
        already_showing         ///< user has clicked (somehow) on an already showing card during 'first_guess' state
    };

private:
    union
    {
        // Not used - attempted to do so and code got very complicated
        //MatchedStates matched;
        //GuessStates guessed;

        InvalidStates invalid;
        int raw;

    }   substate_;

    Card::Location
        first_guess_location,
        second_guess_location;

    inline bool state(States s) { return base_type::state(s); }

    inline void state(InvalidStates v)
    {
        // DEBT: No comparison against previous one because crossing union boundaries make that extra work
        substate_.invalid = v;
    }

    inline bool state(States s, InvalidStates ss)
    {
        state(ss);
        return state(s);
    }

    const Card::Identity& id_at(const Card::Location& location) const
    {
        return model.at(location.position).identity;
    }

public:
    InvalidStates getInvalidState() const { return substate_.invalid; }

    States state() const
    {
        return base_type::state();
    }

    // DEBT: Utility function just for external debug processes
    // Breaks no rules, but feels sloppy anyway
    const Card::Location& first() const { return first_guess_location; }

    // helper method indicating whether we're in invalid state
    bool is_invalid() const { return state() == States::invalid; }

    // when not_matched, issue reset_guessed to flip cards back down and
    // go into idle state
    bool reset_guessed();

    bool set_to_idle()
    {
        return state(States::idle);
    }

    bool first_guess(const Card::Location& location);
    bool second_guess(const Card::Location& location);
    bool match();

    // After ending up in an invalid state, call this to get back into
    // a ready state
    bool clear_invalid();

    MatchControllerCore(MatchModel& model, Notifier& notifier) :
        base_type{notifier},
        model{model}
    {
        set_to_idle();
    }


    MatchControllerCore(cards::GameModel& gameModel, MatchModel& model, Notifier& notifier) :
        base_type{notifier},
        model{model}
    {
        initMatch(gameModel, model);
        set_to_idle();
    }
};


}
