#pragma once

#include "base.h"

#include <QDebug>

namespace games {

struct DiagnosticController //: ControllerBase
{
    //Q_OBJECT

public:
    DiagnosticController(cards::AppState& appState, ModelBase& model, QObject* parent = nullptr)
        //:
        //ControllerBase(appState, model, parent)
    {}

    void evaluate(const cards::Card& card)
    {
        static int diamondCardsShowing = 13;

        bool showing = card.state.showing;

        if(card.identity.suit == cards::Suits::Diamond)
        {
            if(showing)
                ++diamondCardsShowing;
            else
                --diamondCardsShowing;

            qDebug() << "diamond cards showing: " << diamondCardsShowing;
        }
    }
};

}
