#pragma once

#include "matchcontroller.h"
#include "widgets/boardviewbase.h"
#include "widgets/matchcontroller.h"


namespace games {

namespace widgets {

typedef BoardViewBase MatchView;

}

struct MatchBase : Base
{
protected:
    cards::QtGameModelNotifier gameNotifier_;
    // NOTE: Be very careful! notifier must appear before gameModel_ otherwise vtable isn't initialied yet!
    cards::GameModel gameModel_;
    MatchModel model_;

    MatchBase(BoardViewBase* view, ControllerBase* controller) :
        Base(view, controller),
        gameModel_{gameNotifier_},
        model_{gameNotifier_.createChild()}
    {
        // DEBT: Feels incorrect that outside of the controller is where we deal the cards
        initMatch(gameModel_, model_);
    }

public:
    MatchModel& model() { return model_; }
    cards::GameModel& gameModel() { return gameModel_; }

    cards::QtGameModelNotifier& notifier() { return gameNotifier_; }
};


template <>
struct Match<true> : MatchBase
{
private:
    widgets::MatchView view_;
    widgets::MatchController controller_;

public:
    Match(cards::AppState& appState, QObject* parent = nullptr) :
        MatchBase(&view_, &controller_),
        view_(parent),
        controller_(appState, model_, parent)
    {
        // Can't do this in base clase because view_(parent) must init first
        view_.initConnections(gameNotifier_);
    }

    widgets::MatchView& view() { return view_; }
    widgets::MatchController& controller() { return controller_; }
};


template <>
struct Match<false> : Base
{
private:
    widgets::MatchView view_;   // FIX: Need different non widgets view here
    cards::QtGameModelNotifier gameNotifier_;
    MatchModel model_;
    cards::GameModel gameModel_;
    MatchController controller_;

public:
    Match(cards::AppState& appState, QObject* parent = nullptr) :
        Base(&view_, &controller_),
        view_(parent),
        model_{gameNotifier_.createChild()},
        gameModel_{gameNotifier_},
        controller_(appState, model_, parent)
    {}

    MatchModel& model() { return model_; }
    widgets::MatchView& view() { return view_; }
    MatchController& controller() { return controller_; }
};


}
