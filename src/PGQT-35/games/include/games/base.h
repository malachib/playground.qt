#pragma once

#include "fwd.h"
#include "../cards/appstate.h"
#include "../cards/notifier.h"
#include <cards/cardsmodel.h>

#include <QObject>

#include <QtCore/qglobal.h>

// NOTE: exports not used yet, but probably should be ... ?
#if defined(LIBRARY_MODE)
#  define CARDGAMES_EXPORT Q_DECL_EXPORT
#else
#  define CARDGAMES_EXPORT Q_DECL_IMPORT
#endif


namespace games {


struct BoardViewBase : QObject
{
    Q_OBJECT

public:
    BoardViewBase(QObject* parent = nullptr) : QObject(parent) {}

    virtual void initConnections(cards::QtGameModelNotifier&) = 0;
    virtual void initConnections(ControllerBase&) = 0;
};

// This mildly modifies definition of CardsModel to more of a 'board' of cards
struct ModelBase : cards::CardsModel
{
    ModelBase(std::shared_ptr<Notifier> notifier) : cards::CardsModel(notifier) {}

    // Since we are responding to a card's flipped state update, we call this
    // flipped and not flip
    //virtual void flipped(const Card&) = 0;
    virtual void reset() = 0;
};


// Depends on CardView (not CardViewBase)
struct ControllerBase : QObject
{
    Q_OBJECT

protected:
    cards::AppState& appState_;
    ModelBase& model_;

    ControllerBase(cards::AppState& appState, ModelBase& model, QObject* parent = nullptr) :
        QObject(parent),
        appState_{appState},
        model_{model}
    {
        //connect(&notifier, &QtNotifier::cardsUpdated, this, &ControllerBase::cardsUpdated);
    }

    inline const cards::Card& card_at(const cards::Card::Location& location) const
    {
        // DEBT: Assert/filter out bad location.id
        return model_.at(location.position);
    }

public:
    ModelBase& model() { return model_; }
    cards::AppState& appState() { return appState_; }

    // NOTE: Not yet used - current 'reset' calls sort directly
    virtual void reset() { model_.reset(); }

signals:
    // DEBT: Obsolete, consider phasing these out completely.  For now, if one does use this,
    // use it only for updates and procedures NOT handled by below cardsUpdated signal
    void sorted();
    void shuffled();

public slots:
    void sort();
    void shuffle();
};



struct Base
{
protected:
    BoardViewBase* view_;
    ControllerBase* controller_;

    Base(BoardViewBase* view, ControllerBase* controller) :
        view_{view},
        controller_{controller}
    {}

public:
    virtual ~Base() {}

    BoardViewBase& view() { return *view_; }
    const BoardViewBase& view() const { return *view_; }

    ModelBase& model() { return controller_->model(); }
    const ModelBase& model() const { return controller_->model(); }

    ControllerBase& controller() { return *controller_; }
};



}
