#pragma once

#include "../fwd.h"
#include "cards/widgets/cardview.h"
#include "../base.h"

#include <QGridLayout>
#include <QObject>

namespace games { namespace widgets {

// For games with one unified board, such as 'match' or perhaps solitaire
struct BoardViewBase : games::BoardViewBase
{
    Q_OBJECT

protected:

    typedef std::unique_ptr<cards::CardView> card_pointer;

    // Maintains a list of pointers to buttons, so is a positional creature and never
    // itself gets shuffled
    std::vector<card_pointer> board_;

public:
    BoardViewBase(QObject* parent = nullptr) : games::BoardViewBase(parent) {}

    std::vector<card_pointer>& board() { return board_; }

    // Do 4 x 13 card layout
    template <typename F>
    void initLayout(QGridLayout&, ControllerBase&, F&& callback);
    /*
    void initLayout(QGridLayout& l, ModelBase& m, ControllerBase& c)
    {
        initLayout(l, m, c, [](CardView& v){});
    } */

    void initConnections(cards::QtGameModelNotifier&) override;
    void initConnections(ControllerBase&) override;

public slots:
    ///
    /// Issue 'draw' on all the CardViews this view manages
    ///
    void draw() const
    {
        for(const card_pointer& cardView : board_)
            cardView->draw();
    }

private slots:
    void updateCardState(cards::Card::Location location);
    void updateCards(const cards::ContainerModel& container, int section);
};

}}
