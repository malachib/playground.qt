#pragma once

#include "../matchcontroller.h"
#include "../../cards/widgets/cardview.h"
#include "../../cards/widgets/util.h"


// TODO: If we're keeping old widget-specific MatchController code, move it here
namespace games { namespace widgets {

struct MatchController : games::MatchController
{
    Q_OBJECT

    typedef games::MatchController base_type;

public:
    MatchController(cards::AppState& appState, MatchModel& model, QObject* parent = nullptr) :
        base_type(appState, model, parent)
    {
    }

private:
    // NOTE: Have to use different name from slot
    void guess_card(cards::widgets::CardViewLegacy& card);

    cards::widgets::CardViewLegacy* first_guess;
    cards::widgets::CardViewLegacy* second_guess;

    void set_to_idle()
    {
        core.set_to_idle();

        first_guess = nullptr;
        second_guess = nullptr;
    }

signals:
    void flipped(const cards::widgets::CardView& card);
    void failedMatchTimedOut();

    // -- the following signals are more stand-ins for model updates

    // A particular card at a particular location has a state change (probably flipped over)
    void cardStateUpdated(cards::Card::Location location, const cards::Card& card);

public slots:
    // Legacy version
    void guess()
    {
        cards::widgets::try_flip(sender(), [&](cards::widgets::CardViewLegacy& t){ guess_card(t); });
    }

private slots:
    void failedMatchTimeout();
};

}}
