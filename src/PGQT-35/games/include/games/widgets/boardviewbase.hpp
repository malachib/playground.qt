#pragma once

#include "boardviewbase.h"
#include "../base.h"

namespace games { namespace widgets {

template <typename F>
void BoardViewBase::initLayout(QGridLayout& l, ControllerBase& controller, F&& cardViewCallback)
{
    // Maintains a list of pointers to buttons, so is a positional creature and never
    // itself gets shuffled
    std::vector<card_pointer>& boardView = board_;
    ModelBase& model = controller.model();

    for(int suit = 0; suit < 4; suit++)
    {
        for(int rank = 0; rank < 13; rank++)
        {
            int boardPosition = boardView.size();

            // NOTE: This would be a little more interesting if we weren't suspicious of vector's
            // underlying store changing around
            //const Card& card = deck[counter];

            auto button = new QPushButton;
            cards::widgets::CardView* cardView;

            if(cards::widgets::CardView::legacyMode)
                cardView = new cards::widgets::CardViewLegacy(controller.appState(), model, boardPosition, button);
            else
                cardView = new cards::widgets::CardView(controller.appState(), model, boardPosition, button);

            // DEBT: I really hate hardcoding this size, auto size would be so much better.
            QSize size(50, 70);

            button->setFixedSize(size);
            button->setIconSize(size);

            cardView->draw();

            //button->resize(150, 50);

            l.addWidget(button, suit, rank);

            cardViewCallback(cardView);

            // If we are to use vector, then we are forced to new up CardView into pointers
            // since QObject doesn't permit copy/move
            boardView.emplace_back(cardView);
        }
    }
}

}}
