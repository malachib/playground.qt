#pragma once

#include "matchcontrollercore.h"

#include <QDebug>
#include <QVariant>

namespace games {

// NOTE: Can't use template mechanism here.
// DEBT: Probably should be using QStateMachine
struct QtMatchStateNotifier :
        QObject,
        MatchControllerCore::Notifier
{
    Q_OBJECT

    typedef MatchControllerCore::States States;

    States oldState;

signals:
    void stateChanged(States state, States oldState);

public:
    QtMatchStateNotifier(QObject* parent = nullptr) : QObject(parent) {}

    void state(States s) override
    {
        qDebug() << "QtMatchStateNotifier: state=" << (int)s;
        emit stateChanged(s, oldState);
        oldState = s;
    }
};



// Represents a view into the card at a particular position in the deck
struct MatchController : public ControllerBase
{
    typedef cards::Card Card;

    Q_OBJECT

public:
    // DEBT: Intermediate usage here as we transition state machine into
    // core
    typedef MatchControllerCore::States States;

    Q_PROPERTY(States state READ state NOTIFY stateChanged)

protected:
    QtMatchStateNotifier notifier_;
    MatchControllerCore core;

public:
    const MatchModel& model() const { return core.model; }
    const QtMatchStateNotifier& notifier() const { return notifier_; }

    States state() const { return core.state(); }

    Q_ENUM(States)

    MatchController(cards::AppState& appState, MatchModel& model, QObject* parent = nullptr) :
        ControllerBase(appState, model, parent),
        notifier_(parent),
        core{model, notifier_}
    {
        // Wired directly to another signal, seems to work
        connect(&notifier_, &QtMatchStateNotifier::stateChanged, this, &MatchController::stateChanged);
    }

signals:
    // DEBT: Should we utilize QStateMachine instead here?
    // DEBT: Keeping around purely for the benefit of QML peering
    void stateChanged(States state, States oldState);

public slots:
    void guess(cards::Card::Location location);

private slots:
    void failedMatchTimeout();
};


}
