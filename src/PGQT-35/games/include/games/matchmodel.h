#pragma once

#include <set>

#include "base.h"

namespace games {

struct MatchModel : ModelBase
{
    typedef cards::Card Card;
    typedef Card::Identity Identity;

private:
    std::set<Identity> matches;
    unsigned guesses_ = 0;

public:
    MatchModel(std::shared_ptr<Notifier> notifier) : ModelBase(notifier) {}

    void reset() override
    {
        matches.clear();
        guesses_ = 0;
    }

    static bool is_match(const Identity& lhs, const Identity& rhs)
    {
        return lhs.rank == rhs.rank;
    }

    void addGuess() { ++guesses_; }

    unsigned guesses() const { return guesses_; }

    void addMatched(Identity first, Identity second)
    {
        matches.insert(first);
        matches.insert(second);
    }

    // NOTE: Does not consider multiple decks
    bool isAlreadyMatched(Identity card) const
    {
        return matches.find(card) != std::end(matches);
    }

    int match_count() const
    {
        return matches.size() / 2;
    }

    /*
    void flipped(const Card& card) override
    {
        const Card::Identity& id = card.identity;

        guess(id);
    } */
};

}
