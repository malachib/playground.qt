// For references, see README.md

#include "cards/widgets/cardview.h"
#include "cards_pack_view.h"

namespace cards { namespace widgets {


void CardView::click()
{
    emit clicked(Card::Location{0, (uint16_t)boardPosition()});
}

void CardViewLegacy::flip()
{
    // NOTE: This works and is nifty, but our boardPosition() accessor is slightly better
    //int deckPosition = sender()->property("deckPosition").toInt();

    // Due to [1] and [2] we are not using blockSignals but instead our own manual flavor of it
    if(appState.blockCardFlip()) return;

    // NOTE: This line is somewhat problematic, it suggests we are a CardViewModel and not just a CardView.
    // This is how we came to be 'CardViewLegacy'
    bool showing = model_.flip(boardPosition());

    draw();

    emit flipped(showing);
    emit showingChanged(showing);
}

void CardView::draw()
{
    base_type::draw_card(card());
}

void CardViewBase::updateText(const Card& card)
{
    bool showing = card.state.showing || appState.forceCardsUp();

    QString text = QString::fromStdString(to_string(card, showing));

    button->setText(text);

    if(!showing)
        button->setStyleSheet("color: darkgray");
    else if(card.identity.suit == cards::Suits::Diamond ||
        card.identity.suit == cards::Suits::Heart)
        button->setStyleSheet("color: red");
    else
        button->setStyleSheet("color: black");
}

void CardViewBase::draw_card(const Card& card)
{
    if(appState.iconMode())
    {
        QIcon icon = cardsPack().icon(card, appState.forceCardsUp());

        button->setIcon(icon);
        button->setText(QString());
    }
    else
    {
        button->setIcon(QIcon());
        updateText(card);
    }
}

}}

