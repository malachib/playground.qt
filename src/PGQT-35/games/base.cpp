#include <QPushButton>

#include "games/base.h"
#include "games/widgets/boardviewbase.h"

namespace games {

namespace widgets {

void BoardViewBase::updateCardState(cards::Card::Location location)
{
    card_pointer& cardView = board_[location.position];

    cardView->draw();
}


void BoardViewBase::updateCards(const cards::ContainerModel& container, int section)
{
    // NOTE: Overzealous filtering by container id and section
    if(container.id == 1 && section == 0)
        draw();
}


void BoardViewBase::initConnections(cards::QtGameModelNotifier& notifier)
{
    connect(&notifier, &cards::QtGameModelNotifier::cardStateUpdated,
            this, &BoardViewBase::updateCardState);
    connect(&notifier, &cards::QtGameModelNotifier::cardsUpdated,
            this, &BoardViewBase::updateCards);
}

void BoardViewBase::initConnections(ControllerBase& controller)
{
    connect(&controller.appState(), &cards::AppState::iconModeChanged, this, &BoardViewBase::draw);
    connect(&controller.appState(), &cards::AppState::forceCardsUpChanged, this, &BoardViewBase::draw);
}

}

void ControllerBase::sort()
{
    model_.sort();
    emit sorted();
}

void ControllerBase::shuffle()
{
    model_.shuffle();
    emit shuffled();
}


}
