#include "cards/notifier.h"


namespace cards {

// TODO: A bit of a mystery how to properly get to the Qt-notifiers within these containers and sections

QtGameModelNotifier::QtGameModelNotifier(QObject* parent) : QObject(parent)
{
}

std::shared_ptr<CardsModel::Notifier> QtGameModelNotifier::createChild(void* context)
{
    return std::shared_ptr<CardsModel::Notifier>(new QtModelNotifier(context, parent()));
}

void QtGameModelNotifier::connectGameModel(QtModelNotifier& gameModelNotifier)
{
    connect(&gameModelNotifier, &QtModelNotifier::changed, this, &QtGameModelNotifier::gameModelChanged);
}


void QtGameModelNotifier::connectContainer(QtModelNotifier& containerNotifier)
{
    connect(&containerNotifier, &QtModelNotifier::changed, this, &QtGameModelNotifier::containerChanged);
}

void QtGameModelNotifier::containerChanged(int pos, int size, ModelChanges type)
{

}

void QtGameModelNotifier::gameModelChanged(int pos, int size, ModelChanges type)
{

}


void QtGameModelNotifier::addSection(const ContainerModel& container, int section)
{
    emit sectionAdded(container, section);

    const CardsModel* cardsModel = container.sections[section];
    // DEBT: Kinda nasty right here, presumes this particular notifier type is in play.  At least
    // we assert down below, making this not a FIX
    auto notifier = dynamic_cast<const QtModelNotifier*>(&cardsModel->notifier());

    Q_ASSERT(notifier);

    QObject::connect(notifier, &QtModelNotifier::allUpdated,
        [=]()
    {
        emit cardsUpdated(container, section);
    });

    QObject::connect(notifier, &QtModelNotifier::changed,
            [&, cardsModel](int position, int size, ModelChanges type)
    {
        // DEBT: Handle this at runtime, it won't be just 1 update mode forever
        Q_ASSERT(size == 1);
        Q_ASSERT(type == ModelChanges::update);

        Card::Location location{(uint16_t)container.id, (uint16_t)position, (uint16_t)section};
        const Card& card = cardsModel->at(position);
        emit cardStateUpdated(location, card);
    });
}

}
