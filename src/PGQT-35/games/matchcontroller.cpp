#include "games/widgets/matchcontroller.h"
#include "games/diagnostic.h"

#include <QTimer>

namespace games {



// DEBT: State change emits feel verbose, but I don't see any cleaner way of doing
// this without turning 'core' into a Qt thing.
// DEBT: Also, these presume state did change, need to fix that
void MatchController::guess(cards::Card::Location location)
{
    switch(core.state())
    {
        case States::not_matched:
            // Not matched mode we wait in a timeout and prohibit clicks, so just abort
            qDebug() << "MatchController: eating clicks during timeout";
            return;

        case States::invalid:
            core.clear_invalid();
            guess(location);
            return;

        case States::idle:
            Q_ASSERT(core.first_guess(location));

            if(core.is_invalid())
            {
                // DEBT: Presumes the reason for invalid state
                qDebug() << "MatchController: this location already selected, aborting guess";
                return;
            }
            break;

        case States::first_guess:
            Q_ASSERT(core.second_guess(location));

            if(core.is_invalid())
            {
                // DEBT: Presumes the reason for invalid state
                qDebug() << "MatchController: this location already selected, aborting guess";
                return;
            }

            Q_ASSERT(core.match());

            if(state() == States::not_matched)
                QTimer::singleShot(3000, this, &MatchController::failedMatchTimeout);
            else if(state() == States::matched)
            {
                const Card::Identity& first_guess = model().at(core.first().position).identity;
                const Card::Identity& second_guess = model().at(location.position).identity;

                qDebug() << "MatchController: match found " << first_guess << " and " << second_guess;

                core.set_to_idle();
            }

            break;

        default:
            qWarning() << "MatchController: erroneous state: " << (int)core.state();
            return;
    }

    // Change card state which through GameNotifier emits a signal
    model_.flip(location.position);
}



void MatchController::failedMatchTimeout()
{
    qDebug() << "MatchController: failedMatchTimeout";

    // Flip both cards back down again after failed guess
    // Set game state to now accept first click again
    core.reset_guessed();
}


// DEBT: Move this to its own .cpp file
namespace widgets {

// Card is already flipped by the time we reach here
void MatchController::guess_card(cards::widgets::CardViewLegacy& card)
{
    DiagnosticController(appState(), model_).evaluate(card.card());

    Card::Card::Location location{0, (uint16_t)card.boardPosition()};

    if(core.state() == States::idle)
    {
        core.first_guess(location);
        first_guess = &card;
    }
    else if(core.state() == States::first_guess)
    {
        Q_ASSERT(core.second_guess(location));

        bool matched = core.state() == States::matched;

        if(!matched)
        {
            second_guess = &card;
            // we're now in States::not_matched
            // Prohibit card selection in general
            appState().setBlockCardFlip(true);
            QTimer::singleShot(3000, this, &MatchController::failedMatchTimeout);
        }
        else
        {
            qInfo() << "MatchController: match found " << *first_guess << " and " << card;
            set_to_idle();
        }
    }
    else
    {
        // TODO: Look into whether we need other state handler here

        qWarning() << "MatchController: erroneous state: " << (int)core.state();
        return;
    }

    // disconnect flip whether or not we have a match -
    // we'll re-enable it 3s later if no match
    card.disconnectFlip();

    emit cardStateUpdated(location, card.card());
    emit flipped(card);
}


void MatchController::failedMatchTimeout()
{
    qDebug() << "widgets::MatchController: failedMatchTimeout";

    // Allow cards in general to flip again
    appState().setBlockCardFlip(false);

    Q_ASSERT(first_guess->card().state.showing);
    Q_ASSERT(second_guess->card().state.showing);

    // Flip face down cards back up again from failed guess
    first_guess->flip();
    second_guess->flip();

    // Reconnect signaling so that user can click again
    // DEBT: general block card flip already blocking these cards, we could re-connect them sooner
    first_guess->connectFlip();
    second_guess->connectFlip();

    // Set game state to now accept first click again
    set_to_idle();

    emit failedMatchTimedOut();
}

}

}
