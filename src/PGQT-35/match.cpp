#include "match.h"
#include "ui_match.h"

Match::Match(games::Match<true>& game, QWidget *parent) :
    QWidget(parent),
    controller{game.controller()},
    ui(new Ui::Match)
{
    ui->setupUi(this);

    ui->labelNoMatchFound->hide();

    games::widgets::MatchController& controller = game.controller();

    // NOTE: 'flip' path only activates if we are in legacy mode.  If not for that edge case,
    // we could instead use non-widget MatchController
    connect(&controller, &games::widgets::MatchController::flipped, this, &Match::flip);
    // NOTE: 'failedMatchTimeout' path same story as flip
    connect(&controller, &games::widgets::MatchController::failedMatchTimedOut, this, &Match::failedMatchTimeout);

    // NOTE: legacy mode where controller emits update messages.  Not used now
    connect(&controller, &games::widgets::MatchController::cardStateUpdated, this, &Match::updateCardState);

    connect(&controller.notifier(), &games::QtMatchStateNotifier::stateChanged, this, &Match::updateControllerState);
    connect(&game.notifier(), &cards::QtGameModelNotifier::cardStateUpdated, this, &Match::updateCardState);
}

Match::~Match()
{
    delete ui;
}


void Match::updateGameStats()
{
    const games::MatchModel& m = controller.model();
    QString guesses = "Guesses: " + QVariant(m.guesses()).toString();
    QString matches = "Matches: " + QVariant(m.match_count()).toString();

    ui->labelGuesses->setText(guesses);
    ui->labelMatches->setText(matches);

    if(controller.state() == games::MatchController::States::not_matched)
    {
        ui->labelNoMatchFound->show();
    }
}

void Match::flip(const cards::widgets::CardView&)
{
    updateGameStats();
}

void Match::updateCardState(cards::Card::Location, const cards::Card&)
{
    updateGameStats();
}

void Match::failedMatchTimeout()
{
    ui->labelNoMatchFound->hide();
}

void Match::updateControllerState(games::MatchController::States state, games::MatchController::States oldState)
{
    switch(oldState)
    {
        case games::MatchController::States::not_matched:
            failedMatchTimeout();
            break;

        default:
            break;
    }
}
