#ifndef MATCH_H
#define MATCH_H

#include <QWidget>

#include <games/matchview.h>

namespace Ui {
class Match;
}

class Match : public QWidget
{
    Q_OBJECT

    games::MatchController& controller;

public:
    explicit Match(games::Match<true>& game, QWidget *parent = nullptr);
    ~Match();

private:
    Ui::Match *ui;

    void updateGameStats();

public slots:
    void updateCardState(cards::Card::Location location, const cards::Card& card);
    void updateControllerState(games::MatchController::States state, games::MatchController::States oldState);

    // legacy calls
    void flip(const cards::widgets::CardView& card);
    void failedMatchTimeout();
};

#endif // MATCH_H
