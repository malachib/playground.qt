#pragma once

#include <memory>

// DEBT: Move this out to some kind of data/utility namespace & project
namespace cards {

template <typename TPosition = int>
struct ModelNotifier;

enum class ModelChanges
{
    create,
    update,
    delete_
};

template <typename TPosition>
struct ModelNotifierTraits
{
    // sizes
    static constexpr TPosition min() { return 1; }

    // positions
    static constexpr TPosition end() { return -1; }
};

template <typename TPosition>
struct ModelNotifier
{
    typedef ModelNotifierTraits<TPosition> traits_type;

    virtual void update() {}
    virtual void change(TPosition position,
                        TPosition size,
                        ModelChanges change)
    {}

    inline void update(TPosition position)
    {
        change(position, traits_type::min(), ModelChanges::update);
    }

    /// \brief create helper method to invoke 'change'
    /// \param position - defaults to end()
    /// \param size
    inline void create(TPosition position = traits_type::end(), TPosition size = traits_type::min())
    {
        change(position, size, ModelChanges::create);
    }

protected:
    constexpr explicit ModelNotifier() {}    // Singleton pattern

public:
    const static std::shared_ptr<ModelNotifier> singleton;
};


// https://stackoverflow.com/questions/3229883/static-member-initialization-in-a-class-template
// Key takeaway here is (from above):
// "doesn't it violate the one definition rule?"
// "No, not if we're talking templates"
// NOTE: This might be slowing down compilation a bit
template<typename TPosition>
const std::shared_ptr<ModelNotifier<TPosition>> ModelNotifier<TPosition>::singleton(
        new ModelNotifier<TPosition>);


}
