#pragma once

#include <QDebug>

#include "statemachine.h"

namespace games {

template <class TState>
bool StateMachine<TState>::assert_state(States v) const
{
    if(state() == v) return true;

    qCritical() << "MatchControllerCore: erroenous state - found" << (int)state() << "expected" << (int)v;
    return false;
}


}
