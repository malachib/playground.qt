#pragma once

// DEBT: Move this out to some kind of data/utility namespace & project
namespace games {

template <class TState>
struct StateMachineNotifier
{
    virtual void state(TState) {};
};

template <class TState>
struct StateMachine
{
    typedef TState States;
    typedef StateMachineNotifier<States> Notifier;

private:
    Notifier& notifier_;
    States state_;

public:
    StateMachine(Notifier& notifier, TState initialState = TState()) :
        notifier_{notifier} {}

    constexpr States state() const
    {
        return state_;
    }

    inline bool state(States v)
    {
        if(state_ == v) return false;

        state_ = v;
        notifier_.state(v);
        return true;
    }

    bool assert_state(States v) const;
};


}
