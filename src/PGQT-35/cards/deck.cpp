#include "cards/deck.h"

#include "cards/container.h" // DEBT: Only here to test compilation

namespace cards {

void initDeck(std::vector<Card>& deck, std::random_device& rd, bool initialShowing)
{
    std::mt19937 gen(rd());
    std::discrete_distribution<> d({90, 10});   // Only about 10% of cards are bent

    for(int suit = 0; suit < 4; suit++)
    {
        for(unsigned rank = 0; rank < 13; rank++)
        {
            Card::Traits traits{(bool)d(gen), 0};
            Card card(Card::Identity{(Suits)suit, rank}, traits);

            card.state.showing = initialShowing;

            deck.push_back(card);
        }
    }
}


const char* to_string(Suits suit, bool abbrev)
{
    if(abbrev)
    {
        switch(suit)
        {
            case Suits::Diamond:    return "D";
            case Suits::Spade:      return "S";
            case Suits::Club:       return "C";
            case Suits::Heart:      return "H";
        }
    }
    else
    {
        switch(suit)
        {
            case Suits::Diamond:    return "Diamond";
            case Suits::Spade:      return "Spade";
            case Suits::Club:       return "Club";
            case Suits::Heart:      return "Heart";
        }
    }

    return "err";
}


std::string to_string(const Card::Identity& identity)
{
    std::string s;

    switch(identity.rank)
    {
        case 0:
            s += 'A';
            break;

        case 10:
            s += 'J';
            break;

        case 11:
            s += 'Q';
            break;

        case 12:
            s += 'K';
            break;

        default:
            s += std::to_string(identity.rank + 1);
    }

    s += ':';
    s += to_string((Suits)identity.suit, true);

    return s;
}

std::string to_string(const Card& card, bool forceShowing)
{
    std::string s;

    if(card.state.showing || forceShowing)
        s = to_string(card.identity);
    else
        s = "down";

    if(card.traits.bent)
        s += "'";

    return s;
}

}
