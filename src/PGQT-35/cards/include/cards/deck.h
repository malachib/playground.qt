#pragma once

#include <random>
#include <string>
#include <vector>

namespace cards {

enum class Suits
{
    Diamond,
    Spade,
    Club,
    Heart
};


struct Card
{
    // Primary read-only identity of a card - used for operator overload equality comparisons
    struct Identity
    {
        const Suits suit;
        const unsigned rank;

        // DEBT: Just using magic number.  If we like this, formalize it more
        bool joker() const { return rank == 15; }

        struct encoded_type
        {
            const unsigned suit : 3;
            const unsigned rank : 4;

            bool operator <(const encoded_type& compare_to) const
            {
                if(suit == compare_to.suit)
                    return rank < compare_to.rank;

                return suit < compare_to.suit;
            }
        };

        bool operator <(const Identity& compare_to) const
        {
            if(suit == compare_to.suit)
                return rank < compare_to.rank;
            else return (int) suit < (int) compare_to.suit;
        }

        encoded_type encode() const
        {
            return encoded_type{(unsigned)suit, rank};
        }

        Identity(Suits suit, unsigned rank) : suit{suit}, rank{rank} {}

        Identity(const Identity& copy_from) = default;
        Identity& operator=(const Identity& copy_from)
        {
            return * new (this) Identity(copy_from);
        }

        Identity(encoded_type encoded) :
            suit((Suits)encoded.suit),
            rank(encoded.rank)
        {}
    };

    // Read only qualities of a card which may or may not be considered secondary identity
    struct Traits
    {
        const bool bent;
        const unsigned deck;

        Traits(const Traits& copy_from) = default;
        Traits& operator=(const Traits& copy_from)
        {
            return * new (this) Traits(copy_from);
        }
    };

    struct State
    {
        bool showing;

        bool flip() { return showing = !showing; }
    };

    Identity identity;
    State state;
    Traits traits;

    bool operator <(const Card& compare_to) const
    {
        return identity < compare_to.identity;
    }

    struct Location
    {
        // which container model this card is associated with
        uint16_t id;
        // what position in the model's section (usually board position) card resides
        uint16_t position;
        // what section - subgroup of the model - that the card resides in
        uint16_t section;

        bool operator ==(const Location& compare_to) const
        {
            return id == compare_to.id &&
                    position == compare_to.position &&
                    section == compare_to.section;
        }
    };

    Card(Identity identity, Traits traits) :
        identity{identity},
        traits{traits}
    {

    }

    // NOTE: Superflous, not needed.  Just keeping around in case
    Card(const Card& copy_from) = default;

    // NOTE: Needed for shuffle to work (std::swap uses it)
    Card& operator=(const Card& copy_from) = default;
};


const char* to_string(Suits suit, bool abbrev);
std::string to_string(const Card::Identity& identity);
std::string to_string(const Card& card, bool forceShowing = false);

void initDeck(std::vector<Card>& deck, std::random_device& rd, bool initialShowing);

}
