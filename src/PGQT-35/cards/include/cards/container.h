#pragma once

#include <memory>

#include "cardsmodel.h"

namespace cards {

// EXPERIMENTAL below this line

struct ContainerModel
{
    const int id;
    const std::string name;

    // We expect is-a vs has-a, so use pointers here instead of values
    std::vector<CardsModel*> sections;

    std::shared_ptr<ModelNotifier<>> notifier;

    ContainerModel(std::shared_ptr<ModelNotifier<>> notifier, int id, std::string name) :
        id{id},
        name{name},
        notifier{notifier}
    {
        // FIX: Compiler error here
        //sections.emplace_back(initialShowing);
    }

    void deal(int from_section, CardsModel& cardsModel, int count, bool showing)
    {
        sections[from_section]->deal(cardsModel, count, showing);
    }

    void deal(int from_section, int to_section, int count, bool showing)
    {
        deal(from_section, *sections[to_section], count, showing);
    }

    int add(CardsModel* section)
    {
        sections.push_back(section);
        notifier->create();
        return sections.size() - 1;
    }
};

// NOTE: Not designed to host multiple games since it doesn't remove dynamically
class GameModel
{
public:
    struct Notifier
    {
        virtual void addSection(const ContainerModel&, int section) {}

        virtual std::shared_ptr<ModelNotifier<>> createChild(void* = nullptr)
        {
            return ModelNotifier<>::singleton;
        }
    };


private:
    // Game model always has a deck of cards
    CardsModel cardsDeck;
    std::vector<ContainerModel> containers;
    Notifier& notifier;
    std::shared_ptr<ModelNotifier<>> const modelNotifier_;

public:
    ModelNotifier<>& modelNotifier() const { return *modelNotifier_; }

    GameModel(Notifier& notifier) :
        cardsDeck(notifier.createChild(this)),
        notifier{notifier},
        modelNotifier_{notifier.createChild(this)}
    {
        // location #0, section #0 is always the deck - typically 52 cards.
        ContainerModel& deck = create("deck");
        deck.add(&cardsDeck);
        cardsDeck.initDeck(false);
    }

    ~GameModel()
    {

    }

    ContainerModel& container(int id)
    {
        return containers[id];
    }

    ContainerModel& deck() { return containers[0]; }

    CardsModel& section(int id, int section)
    {
        return *containers[id].sections[section];
    }

    const Card& card(Card::Location location)
    {
        return section(location.id, location.section).at(location.position);
    }

    ContainerModel& create(std::string name, CardsModel* initialModel = nullptr)
    {
        containers.emplace_back(notifier.createChild(), containers.size(), name);
        ContainerModel& container = containers.back();
        modelNotifier().create();
        if(initialModel)
        {
            int sectionId = container.add(initialModel);
            // DEBT: Instead utilize Container's notifier directly
            notifier.addSection(container, sectionId);
        }
        return container;
    }

    void deal(CardsModel& toModel, int count, bool showing)
    {
        deck().deal(0, toModel, count, showing);
    }
};

}
