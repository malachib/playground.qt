#pragma once

#include <algorithm>
#include <memory>
#include <vector>

#include "deck.h"
#include <utility/modelnotifier.h>

namespace cards {

struct CardsModel
{
    typedef ModelNotifier<> Notifier;

private:
    std::vector<Card> cards_;
    std::random_device rd;
    std::shared_ptr<Notifier> notifier_;

public:
    CardsModel(std::shared_ptr<Notifier> notifier) : notifier_{notifier} {}
    virtual ~CardsModel() = default;

    // DEBT: Exposing this here is obnoxious, but necessary for QtGameModelNotifier to peer in
    Notifier& notifier() const { return *notifier_; }

    const std::vector<Card>& cards() const { return cards_; }

    int card_count() const { return cards_.size(); }

    void set_showing(int position, bool showing)
    {
        cards_[position].state.showing = showing;
        notifier().update(position);
    }

    bool flip(int position)
    {
        bool showing = cards_[position].state.flip();
        notifier().update(position);
        return showing;
    }

    void sort()
    {
        std::sort(cards_.begin(), cards_.end());
        notifier().update();
    }

    void shuffle()
    {
        std::mt19937 g(rd());

        // DEBT: The notion of shuffling should actually be in deck, like we started with.  However,
        // to do that we also need a 'deal' mechanism to move it from the deck to the board
        std::shuffle(cards_.begin(), cards_.end(), g);
        notifier().update();
    }

    /*
    inline Card& at(decltype(cards_)::size_type position)
    {
        return cards_[position];
    } */

    inline const Card& at(decltype(cards_)::size_type position) const
    {
        // FIX: Why is this giving us grief?
        //return cards.at[position];
        return cards_[position];
    }

    // EXPERIMENTAL
    // NOTE: Due to vector behavior, we can only deal from the "top" (back) of the deck
    void deal(CardsModel& deal_to, unsigned count, bool showing = false)
    {
        while(count--)
        {
            cards_.back().state.showing = showing;
            deal_to.cards_.push_back(cards_.back());
            cards_.pop_back();
        }

        deal_to.notifier().update();
        notifier().update();
    }

    // DEBT: Don't like this much specificity here
    void initDeck(bool initialShowing = false)
    {
        cards::initDeck(cards_, rd, initialShowing);
        notifier().update();
    }
};

}
