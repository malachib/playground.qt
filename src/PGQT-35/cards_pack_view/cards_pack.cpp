#include "cards_pack.h"

using namespace cards;

std::string CardsPack::cardFrontName(cards::Suits suit, int rank)
{
    std::string name = cards::to_string(suit, false);

    name[0] = std::tolower(name[0]);

    if(suit != cards::Suits::Diamond)   name += "s";
    name += "_";
    name += std::to_string(rank);
    name += ".png";
    return name;
}


std::string CardsPack::cardName(const cards::Card& card, const BackStyle& backStyle, bool forceCardsUp)
{
    bool showing = card.state.showing || forceCardsUp;
    std::string fileName;

    if(showing)
    {
        Card::Identity id = card.identity;

        fileName = CardsPack::cardFrontName(id.suit, id.rank + 1);
    }
    else if(card.identity.joker())
    {
        // DEBT: Untested
        fileName = CardsPack::cardJokerName();
    }
    else
    {
        fileName = CardsPack::cardBackName(backStyle);
    }

    return fileName;
}
