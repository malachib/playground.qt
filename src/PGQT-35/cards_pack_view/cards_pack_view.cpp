#include "cards_pack_view.h"

#include <QIcon>
#include <QLabel>
#include <QPainter>

CardsPackViewModel::CardsPackViewModel(QObject* parent) : QObject(parent)
#if FEATURE_CARDSPACK_SVG
    ,
    svgRenderer(parent)
#endif
{
    resourcePrefix = ":/png/large/";

    // Technically works, but a lot of
    // things need to happen to extract card images from this vector
    //loadVector();
    //debugShowVector();
}

#if FEATURE_CARDSPACK_SVG
void CardsPackViewModel::debugShowVector()
{
    // Guidance from:
    // - https://stackoverflow.com/questions/10079011/qpixmap-and-svg
    // - https://stackoverflow.com/questions/8551690/how-to-render-a-scaled-svg-to-a-qimage
    auto pm = new QPixmap(svgRenderer.defaultSize());
    pm->fill(0xaaA08080);
    QPainter painter(pm);
    svgRenderer.render(&painter, pm->rect());

    auto myLabel = new QLabel();
    myLabel->setPixmap(*pm);
    myLabel->show();
}


void CardsPackViewModel::loadVector()
{
    QString svgName = ":/vector/";

    svgName += CardsPack::svgName();

    bool success = svgRenderer.load(svgName);
    QSize defaultSize = svgRenderer.defaultSize();
    Q_ASSERT(success);
    Q_ASSERT(defaultSize.width() == 1120);
    Q_ASSERT(defaultSize.height() == 524);
}
#endif

// DEBT: No vector vs raster - but that code isn't ready yet anyway everything is raster
QString CardsPackViewModel::name(const Card& card, bool forceCardsUp) const
{
    std::string fileName = CardsPack::cardName(card, cardBackStyle(), forceCardsUp);

    return QString::fromStdString(fileName);
}

QIcon CardsPackViewModel::raster_icon(const Card& card, bool forceCardsUp) const
{
    QString resourceName = resourcePrefix + name(card, forceCardsUp);

    return QIcon(resourceName);
}


QIcon CardsPackViewModel::vector_icon(const Card& card, bool forceCardsUp) const
{
    return QIcon();
}

QIcon CardsPackViewModel::icon(const Card& card, bool forceCardsUp) const
{
    if(vectorMode_)
        return vector_icon(card, forceCardsUp);
    else
        return raster_icon(card, forceCardsUp);
}
