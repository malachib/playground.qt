#pragma once

// DEBT: This should be found via a 'Cards' CMakeLists.txt itself
#include <cards/deck.h>

struct CardsPack
{
    enum Colors
    {
        Red,
        Black,
        Blue,
        Grey
    };

    struct BackStyle
    {
        Colors color = CardsPack::Blue;
        bool plain = true;

        bool operator !=(const BackStyle compare_to)
        {
            return color != compare_to.color || plain != compare_to.plain;
        }
    };

    static const char* to_string(Colors color)
    {
        switch(color)
        {
            case Colors::Red: return "red";
            case Colors::Black: return "black";
            case Colors::Blue: return "blue";
            case Colors::Grey: return "grey";
            default: return "N/A";
        }
    }

    static std::string cardJokerName(Colors color = Black)
    {
        std::string name = "joker_";

        name += to_string(color);

        return name;
    }

    static std::string cardFrontName(cards::Suits suit, int rank);

    static std::string cardBackName(Colors color = Blue, bool plain = true)
    {
        std::string name = "back_";

        name += to_string(color);
        name += plain ? "_2.png" : "_1.png";

        return name;
    }

    static std::string cardBackName(BackStyle s)
    {
        return cardBackName(s.color, s.plain);
    }

    static std::string cardName(const cards::Card& card, const BackStyle& backStyle, bool forceCardsUp = false);
    static std::string cardName(const cards::Card& card)
    {
        return cardName(card, BackStyle{});
    }

    static const char* svgName()
    {
        return "cards.svg";
    }
};
