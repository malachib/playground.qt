#pragma once

#include "cards_pack.h"

#include <QObject>
#include <QtSvg/QSvgRenderer>
#include <QVariant>

typedef CardsPack::BackStyle CardBackStyle;

// NOTE: Not making any discernable difference
// DEBT: Document why we are using this
Q_DECLARE_METATYPE(CardBackStyle);

#define FEATURE_CARDSPACK_SVG 0


// NOTE: QObject is overdoing things a bit
struct CardsPackViewModel : QObject
{
    typedef cards::Card Card;

    Q_OBJECT

    QString resourcePrefix;
    CardBackStyle cardBackStyle_;
    bool vectorMode_ = false;

    Q_PROPERTY(CardBackStyle cardBackStyle MEMBER cardBackStyle_ NOTIFY cardBackStyleChanged)

private:
#if FEATURE_CARDSPACK_SVG
    QSvgRenderer svgRenderer;

    void loadVector();
    void debugShowVector();

#endif
    QIcon vector_icon(const Card&, bool force_showing = false) const;
    QIcon raster_icon(const Card&, bool force_showing = false) const;

public:
    CardsPackViewModel(QObject* parent = nullptr);

    const CardBackStyle& cardBackStyle() const { return cardBackStyle_; }
    void cardBackStyle(CardBackStyle& v)
    {
        setProperty("cardBackStyle", QVariant::fromValue(v));
    }

    QString name(const Card&, bool force_showing = false) const;
    /// Build icon from name provided by CardsPack.  Assumes presence in QRC/resourcePrefix
    QIcon icon(const Card&, bool force_showing = false) const;

signals:
    void cardBackStyleChanged();
    void cardSizeChanged();
};
