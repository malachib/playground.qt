# Overview

Parity project with student's CS 335 card games

## TODO

### QProperty

Look into property bindings to potentially ease code fatness in
mainwindow constructor [3]

# References

1. https://www.qtcentre.org/threads/37730-Blocksignals-for-all-widgets-within-a-QDialog
2. https://forum.qt.io/topic/129364/blocking-signals-to-child-widgets/7
3. https://www.qt.io/blog/property-bindings-in-qt-6
4. https://embeddeduse.com/2019/07/28/printing-custom-qt-types-with-qdebug/