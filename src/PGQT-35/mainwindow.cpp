// For references, see README.md

#include <set>
#include <vector>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <cards/cardview.h>
#include <cards/container.h>
#include "games.h"
#include <games/widgets/boardviewbase.hpp>

#include "match.h"

#include <QtDebug>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //initLayout(*ui->gridLayoutWidget);

    ui->tabWidget->setTabText(0, "General");

    initMatch();

    connect(&appState.cardsPack, &CardsPackViewModel::cardBackStyleChanged, this, &MainWindow::newCardsPack);
    connect(&appState, &cards::AppState::blockCardFlipChanged, this, &MainWindow::cardsBlockStateChange);

    // Below this line is the pseudo databinding portion
    // ---

    /*
    connect(ui->checkIconMode, &QCheckBox::stateChanged,
            [=](bool checked)
    {
        appState.iconMode(checked);
    }); */

    connect(ui->checkIconMode, &QCheckBox::stateChanged, &appState, &cards::AppState::setIconMode);
    connect(ui->checkForceCardsUp, &QCheckBox::stateChanged, &appState, &cards::AppState::setForceCardsUp);

    ui->checkForceCardsUp->setCheckState(appState.forceCardsUp() ?
                                             Qt::CheckState::Checked :
                                             Qt::CheckState::Unchecked);

    ui->checkPlainBack->setChecked(appState.cardsPack.cardBackStyle().plain);
}


void MainWindow::initController(games::ControllerBase* controller)
{
    game->view().initConnections(*controller);

    connect(ui->buttonShuffle, &QAbstractButton::clicked, controller, &games::ControllerBase::shuffle);
    connect(ui->buttonReset, &QAbstractButton::clicked, controller, &games::ControllerBase::sort);
}


void MainWindow::initMatch()
{
    // Widget mode
    auto match = new games::Match<true>(appState, this);
    games::widgets::MatchController& controller = match->controller();
    Match* tab = new Match(*match, this);

    // DEBT: Would prefer to return this, and even maybe add it to a vector
    game = match;

    ui->tabWidget->addTab(tab, tr("Match"));

    match->view().initLayout(*ui->gridLayout, controller,
        [&](cards::widgets::CardView* cardView)
    {
        if(cards::widgets::CardView::legacyMode)
            // DEBT: This casting is error prone, make a different call to initLayout altogether for legacy mode
            connect((cards::widgets::CardViewLegacy*)cardView, &cards::widgets::CardViewLegacy::flipped,
                    &controller, &games::widgets::MatchController::guess);
        else
            connect(cardView, &cards::CardView::clicked, &controller, &games::MatchController::guess);
    });

    connect(&controller.notifier(), &games::QtMatchStateNotifier::stateChanged, this, &MainWindow::matchStateChange);

    initController(&controller);
}


void MainWindow::initLayout(QWidget& l)
{
    // DEBT: Temporary as we get layout widget itself to resize
    l.setStyleSheet("background-color:lightgray");

    // EXPERIMENTAL
    // Nothing below this line seems to affect anything
    QSizePolicy sizePolicy;
    sizePolicy.setHorizontalPolicy(QSizePolicy::Policy::Maximum);
    sizePolicy.setHorizontalStretch(2);

    l.setSizePolicy(sizePolicy);
    l.setSizeIncrement(QSize(1, 0));
}


// EXPERIMENTAL
union matching_type
{
    struct
    {
        unsigned lhs : 6;
        unsigned rhs : 6;
    }   index;

    uint16_t raw;

    bool operator <(const matching_type& compare_to) const
    {
        return raw < compare_to.raw;
    }
};


void MainWindow::newCardsPack()
{
    // DEBT: We may end up not using games::widgets::BoardViewBase at some point.  Be careful.
    // i.e. as always, casting like this is dangerous
    const auto& view = (games::widgets::BoardViewBase&) game->view();

    // Not necessary, but nice to not even redraw if we aren't doing icons in the first place
    if(appState.iconMode())
        view.draw();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete game;
}

// When our custom block all cards mechanism flips on and off, this gets called
void MainWindow::cardsBlockStateChange(bool blocked)
{
    QWidget* w = ui->centralwidget;

    if(blocked)
        w->setStyleSheet("background-color: salmon");
    else
        w->setStyleSheet("");
}

void MainWindow::matchStateChange(games::MatchControllerCore::States state)
{
    switch(state)
    {
        case games::MatchController::States::not_matched:
            cardsBlockStateChange(true);
            break;

        case games::MatchController::States::idle:
            cardsBlockStateChange(false);
            break;

        default: break;
    }
}


void MainWindow::on_checkPlainBack_stateChanged(int checked)
{
    CardBackStyle s = appState.cardsPack.cardBackStyle();
    s.plain = checked;
    appState.cardsPack.cardBackStyle(s);
}

