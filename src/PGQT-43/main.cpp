#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQuickView>
#include <QQmlContext>
#include <QtQuick/QQuickView>

#include "main.h"

// https://forum.qt.io/topic/144293/qml-qlist-qobject-won-t-work-with-setcontextproperty

// Similar issues:
// 1. https://stackoverflow.com/questions/48349672/qt-listview-doesnt-show-c-model-content

// Alternative guidance (looking like I may no longer be able to avoid it):
// 1. https://qml.guide/data-models-deconstructed-part-2-qabstractlistmodel/


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/PGQT_43/Main.qml"_qs);

    QQmlContext* context = engine.rootContext();
    Session session;

    // EXPERIMENTAL, made no difference, except for intellisense when adding main.*
    // via qt_add_executable
    qmlRegisterType<DataObject>("PGQT_43", 1, 0, "DataObject");
    qmlRegisterType<Session>("PGQT_43", 1, 0, "Session");

    context->setContextProperty("session", &session);

    // NOTE: This won't work because 'Main' has no property of session2.  This implies
    // engine flows through to first child and strongly suggests that one can only set
    // "properties" if they exist already, unlike "context property"
    // See [2] for inspiration here
    //engine.setInitialProperties({{"session2", QVariant::fromValue(&session)}});

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    qDebug() << "main: phase 1";

    session.setDebug("Hi2u");

    QObject* root = engine.rootObjects().first();
    auto child1 = root->findChild<QObject*>("myView");

    QStringList _dataList = {
        "Item 1",
        "Item 2",
        "Item 3",
        "Item 4"
    };

    QList<QObject*> dataList;
    for(const QString& s : _dataList)
    {
        auto o = new DataObject(root);
        o->setName(s);
        o->setColor(s);
        dataList.append(o);
    }

    // NOTE: This is what canonical example does and it DOES work, but we want to use context
    //child1->setProperty("model", QVariant::fromValue(dataList));

    // TODO: This is what we want to make work
    session.setItems(dataList);

    qDebug() << "main: phase 2";

    return app.exec();
}
