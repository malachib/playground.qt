import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import PGQT_43 1.0

Window {
    id: idMain
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ColumnLayout {

        MyView {
            id: myView

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: 100

            model: session.items

            // Needed for 'findChild' to work
            objectName: "myView"
        }

        Text {
            text: "debug: " + session.debug
        }
    }

    Connections {
        target: session

        function onItemsChanged(items) {
            console.info("session: onItemsChanged");
            // Certainly APPEARS to be a QList of QObject/DataObject
            //console.info("session: onItemsChanged", items);
            //myView.model = items;
        }
    }

    Binding {
        // "works" as well as binding directly in 'myView'
        //target: myView
        //property: "model"
        //value: session.items

        // Strangely, these create a QML runtime error
        //myView.model: session.items
        //session.items: myView.model
    }
}
