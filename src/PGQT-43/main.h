#pragma once

#include <QAbstractItemModel>
#include <QObject>
#include <QQmlEngine>
#include <QVariant>
#include <QVariantList>

class DataObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)

    // 19APR23 - Added, doesn't help
    QML_ELEMENT

    QString name_, color_;

signals:
    void nameChanged();
    void colorChanged();

public:
    // +++EXPERIMENTING, doesn't seem to matter
    DataObject() : QObject() {}
    DataObject(QObject* parent) : QObject(parent) {}
    // ---

    QString name() const { return name_; }
    void setName(QString v)
    {
        if(name_ == v) return;

        name_ = v;
        emit nameChanged();
    }

    QString color() const { return color_; }
    void setColor(QString v)
    {
        if(color_ == v) return;

        color_ = v;
        emit colorChanged();
    }
};


class DataObject2 : public QAbstractItemModel
{
public:
    DataObject2(QObject* parent = nullptr) : QAbstractItemModel(parent) {}

    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
};


#define TECHNIQUE_QLIST         1
#define TECHNIQUE_QVARIANT      2
#define TECHNIQUE_QVARIANTLIST  3
#define TECHNIQUE_QLISTPTR      4       // This one works particularly badly
#define TECHNIQUE_QLISTREF      5       // Has trouble compiling
#define TECHNIQUE_QLISTT        6

#define TECHNIQUE TECHNIQUE_QVARIANTLIST


class Session : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString debug READ debug NOTIFY debugChanged)

#if TECHNIQUE == TECHNIQUE_QLIST
    typedef QList<QObject*> result_type;
#elif TECHNIQUE == TECHNIQUE_QVARIANTLIST
    typedef QVariantList result_type;
#elif TECHNIQUE == TECHNIQUE_QVARIANT
    typedef QVariant result_type;
#elif TECHNIQUE == TECHNIQUE_QLISTPTR
    typedef QList<QObject*>* result_type;
#elif TECHNIQUE == TECHNIQUE_QLISTREF
    typedef QList<QObject*>& result_type;
#elif TECHNIQUE == TECHNIQUE_QLISTT
    typedef QList<DataObject*> result_type;
#else
#error
#endif

    Q_PROPERTY(result_type items READ items NOTIFY itemsChanged)

    // 19APR23 - Added, doesn't help
    QML_ELEMENT

    std::remove_reference_t<result_type> items_;
    QString debug_;

signals:
    void itemsChanged(result_type);
    void debugChanged(QString);

public:
    QString debug() const { return debug_; }

    void setDebug(QString v)
    {
        if(debug_ == v) return;

        debug_ = v;
        emit debugChanged(v);
    }

    result_type items() const { return items_; }

    void setItems(QList<QObject*>& v)
    {
#if TECHNIQUE == TECHNIQUE_QLIST
        items_ = v;
        qDebug() << "Session::setItems: v[0].name=" << items_[0]->property("name");
#elif TECHNIQUE == TECHNIQUE_QVARIANTLIST
        QVariantList l;
        for(QObject* o : v) l.append(QVariant::fromValue(o));
        items_ = l;

        //items_ = QVariantList::fromList(v);
        auto o2 = items_[0].value<QObject*>();
        qDebug() << "Session::setItems: v[0].name=" << o2->property("name");
#elif TECHNIQUE == TECHNIQUE_QVARIANT
        items_ = QVariant::fromValue(v);
#elif TECHNIQUE == TECHNIQUE_QLISTPTR
        items_ = &v;
#elif TECHNIQUE == TECHNIQUE_QLISTREF
        items_ = v;
#elif TECHNIQUE == TECHNIQUE_QLISTT
        items_.clear();
        for(QObject* o : v)
        {
            items_.append(dynamic_cast<DataObject*>(o));
        }
        qDebug() << "Session::setItems: v[0].name=" << items_[0]->name();
#else
#error
#endif

        //auto o2 = items_[0].convert(DataObject::staticMetaObject.metaType());

        emit itemsChanged(items_);
    }
};


