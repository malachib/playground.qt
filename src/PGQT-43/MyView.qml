import QtQuick
import QtQuick.Layouts

//import PGQT_43      // EXPERIMENTAL, made no difference except to inform that DataObject can be explicitly stated

ListView {
    required model

    delegate: Rectangle {
        id: testDel

        // Works, though modelData is already present
        //required property DataObject modelData

        // NOTE: modelData always (usually?) present, yet does not exist if
        // doing only below.  Added "after the fact" it seems
        //required modelData

        height: 25
        width: 200

        border.width: 2
        border.color: "gray"

        //required  // crashes out
        property string name    // always null/undefined

        RowLayout {
            // Renders
            Text { Layout.fillWidth: true; text: modelData.name }

            // Does not render
            Text { Layout.fillWidth: true; text: testDel.name }
        }

        //Text { text: parent.modelData }

        /* Works if we explicity defined modelData property
        onModelDataChanged: {
            console.info("modelData: ", modelData)
        }   */
    }

    onModelChanged: {
        console.info("model: ", model, "item[0]=", model[0].name);
    }
}
