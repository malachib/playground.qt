#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QGeoServiceProvider>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QGeoMappingManager* mappingManager;
    QGeoRoutingManager* routingManager;

    // NOTE: this boilerplate code appears to be not directly related
    // to the QtQuick/QML code
    auto providers = QGeoServiceProvider::availableServiceProviders();
    QGeoServiceProvider serviceProvider("mapboxgl");
    auto error = serviceProvider.error();
    auto errorString = serviceProvider.errorString();

    if(error == QGeoServiceProvider::NoError)
    {
        mappingManager = serviceProvider.mappingManager();
        routingManager = serviceProvider.routingManager();
        //searchManager = serviceProvider.searchManager();
    }

    return app.exec();
}
