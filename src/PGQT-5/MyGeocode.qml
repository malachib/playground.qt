import QtQuick 2.0
import QtPositioning 5.3
import QtLocation 5.5

Rectangle {
    Address {
        id :fromAddress
        street: "420 S. San Pedro"
        city: "Los Angeles"
        country: "USA"
        state : "CA"
        postalCode: "90013"
    }
}
