import QtQuick 2.0
import QtPositioning 5.5
import QtLocation 6

Rectangle {

    Plugin {
        id: herePlugin
        name: "here"
        PluginParameter { name: "here.app_id"; value: "NakRhCgD3TQnba9n0h7c" }
        PluginParameter { name: "here.token"; value: "qbR95f74brodOvj02b5_EQ" }
        PluginParameter { name: "here.proxy"; value: "system" }
    }

    Map {
        anchors.fill: parent
        id: map
        plugin: herePlugin
        zoomLevel: 10

        MapItemView {
            model: searchModel
            delegate: MapQuickItem {
                coordinate: place.location.coordinate

                anchorPoint.x: image.width * 0.5
                anchorPoint.y: image.height

                sourceItem: Column {
                    Image
                    {
                        id: image;
                        source: "marker2.png"
                        width: 20
                        height: 20
                    }
                    Text { text: title; font.bold: true }
                }
            }
        }
    }

    property variant locationOslo: QtPositioning.coordinate( 59.93, 10.76)

    PlaceSearchModel {
        id: searchModel

        plugin: map.plugin

        searchTerm: "Pizza"
        searchArea: QtPositioning.circle(locationOslo)

        Component.onCompleted: update()
    }

    PositionSource {
        id: positionSource
        property variant lastSearchPosition: locationOslo
        active: true
        updateInterval: 120000 // 2 mins
        onPositionChanged:  {
            var currentPosition = positionSource.position.coordinate
            map.center = currentPosition
            var distance = currentPosition.distanceTo(lastSearchPosition)
            if (distance > 500) {
                // 500m from last performed pizza search
                lastSearchPosition = currentPosition
                searchModel.searchArea = QtPositioning.circle(currentPosition)
                searchModel.update()
            }
        }
    }
}
