import QtQuick 2
import QtPositioning 5.3
// QtLocation not available
import QtLocation 6

Rectangle {
    Plugin {
        id: osmPlugin
        // Not fully configured, see http://doc.qt.io/qt-5/location-plugin-osm.html
        name: "osm"

        // MapQuest doesn't provide a feed anymore, so one workaround explored:
        // http://stackoverflow.com/questions/29120160/qt-qml-how-to-include-map-tiles-for-offline-usage

        // and a similar solution here
        // http://stackoverflow.com/questions/41790875/how-to-run-openstreetmap-offline-in-qml-qt
    }

    Plugin {
        id: mapBoxPlugin
        name: "mapbox"
        PluginParameter { name: "mapbox.access_token"; value: "pk.eyJ1IjoibWFsYWNoaWIiLCJhIjoiY2l6ZGVreHM0MHRxbjJ3b2VwbTR1d29peCJ9.R_dsru3PS74N0vZgoC4ovA" }

        // experimenting - not actually documented and doesn't seem to work
        PluginParameter { name: "mapbox.mapping.style_url"; value: "mapbox://styles/mapbox/streets-v9" }
        PluginParameter { name: "mapbox.mapping.style"; value: "Basic" }
        // Obsolete
        //PluginParameter { name: "mapbox.mapping.map_id"; value: "mapbox://styles/mapbox/streets-v9" }
        PluginParameter { name: "mapbox.mapping.map_id"; value: "Basic" }
    }

    // Can't use this one either yet since mapBoxGl plugin doesnt bundle in QT5 normally.
    // Presumably, it being a plugin, we can remedy that relatively easily
    Plugin {
        id: mapBoxGlPlugin
        name: "mapboxgl"
        PluginParameter { name: "mapboxgl.access_token"; value: "pk.eyJ1IjoibWFsYWNoaWIiLCJhIjoiY2l6ZGVreHM0MHRxbjJ3b2VwbTR1d29peCJ9.R_dsru3PS74N0vZgoC4ovA" }
    }

    // It looks like 'here' plugin may still work, but has been adjusted since before
    // https://stackoverflow.com/questions/60544057/qt-qml-map-with-here-plugin-how-to-correctly-authenticate-using-here-token
    // https://bugreports.qt.io/browse/QTBUG-118447
    // https://github.com/eyllanesc/stackoverflow/blob/master/questions/60544057/Qt5.14/update-HERE-Qt5.14.patch
    // No longer seems to use app_id & token
    Plugin {
        id: herePlugin
        name: "here"
        // 10DEC23 updating based on new apiKey approach, can't get it to come online though
        // PGQT-5 specific apiKey
        PluginParameter { name: "here.apiKey"; value: "nutXptOZicmLfEdxAva1ayNNOrwGzpzE1cifhr2fAQY" }
        PluginParameter { name: "here.proxy"; value: "system" }
    }


    PositionSource {
        id: positionSource
    }

    Map {
        anchors.fill: parent
        id: map
        // 10DEC23 Used to use 'herePlugin' but support is not well documented now
        //plugin: mapBoxPlugin
        //plugin: mapBoxGlPlugin
        //plugin: herePlugin
        plugin: osmPlugin

        zoomLevel: 9
        /*
        center
        {
            latitude: 59.9485
            longitude: 10.7686
        }*/
        center: QtPositioning.coordinate(59.91, 10.75) // Oslo


        property MapCircle circle
        property variant fromCoordinate: QtPositioning.coordinate(59.9483, 10.7695)
        property variant toCoordinate: QtPositioning.coordinate(59.9645, 10.671)

        signal showRouteList()

        MapItemView {
            model: geocodeModel
            delegate: pointDelegate
        }

        Component.onCompleted: {
            circle = Qt.createQmlObject('import QtLocation 5.3; MapCircle {}', layout)
            //circle.center = positionSource.position.coordinate
            circle.center = map.center
            circle.radius = 2000.0
            circle.color = 'green'
            circle.border.width = 3
            circle.opacity = 0.15
            map.addMapItem(circle)

            geocodeModel.query = MyGeocode.fromAddress
            geocodeModel.update()

            // clear away any old data in the query
            routeQuery.clearWaypoints();

            // add the start and end coords as waypoints on the route
            routeQuery.addWaypoint(fromCoordinate)
            routeQuery.addWaypoint(toCoordinate)
            routeQuery.travelModes = RouteQuery.CarTravel
            routeQuery.routeOptimizations = RouteQuery.FastestRoute

            routeModel.update();

            // center the map on the start coord
            map.center = fromCoordinate;
        }

        DragHandler {
            id: drag
            target: null
            onTranslationChanged: (delta) => map.pan(-delta.x, -delta.y)
        }
    }


    GeocodeModel {
        id: geocodeModel
        plugin: map.plugin.supportsGeocoding(Plugin.AnyGeocodingFeatures) ? map.plugin : null
        onStatusChanged: {
            if ((status == GeocodeModel.Ready) || (status == GeocodeModel.Error))
            {
                // Don't know yet how to stack all this into a single MapViewItem/Component
                // so smaller circle gets drawn manually here
                var circle = Qt.createQmlObject('import QtLocation 5.3; MapCircle {}', layout)

                circle.center.latitude = get(0).coordinate.latitude
                circle.center.longitude = get(0).coordinate.longitude

                circle.radius = 100.0
                circle.color = 'blue'
                circle.border.width = 1
                circle.opacity = 0.3
                map.addMapItem(circle)

                //map.geocodeFinished()
            }
        }
        onLocationsChanged:
        {
            if (count == 1) {
                map.center.latitude = get(0).coordinate.latitude
                map.center.longitude = get(0).coordinate.longitude
            }
        }
    }

    // a MapItemView delegate is like a template
    Component {
        id: pointDelegate

        MapCircle {
            id: point
            radius: 1000
            color: "#46a2da"
            border.color: "#190a33"
            border.width: 2
            smooth: true
            opacity: 0.3
            center: locationData.coordinate
        }
    }

    RouteModel {
        id: routeModel
        plugin: map.plugin.supportsRouting(Plugin.AnyRoutingFeatures) ? map.plugin : null
        query:  RouteQuery {
            id: routeQuery
        }
        onStatusChanged: {
            if (status == RouteModel.Ready) {
                switch (count) {
                case 0:
                    // technically not an error
                    map.routeError()
                    break
                case 1:
                    map.showRouteList()
                    break
                }
            } else if (status == RouteModel.Error) {
                map.routeError()
            }
        }
    }


    MapItemView {
        model: routeModel
        delegate: routeDelegate
    }

    Component {
        id: routeDelegate

        MapRoute {
            id: route
            route: routeData
            line.color: "#46a2da"
            line.width: 5
            smooth: true
            opacity: 0.8
        }
    }

    /*
    ListView {
        interactive: true
        model: ListModel { id: routeInfoModel }
        header: RouteListHeader {}
        delegate:  RouteListDelegate{
            routeIndex.text: index + 1
            routeInstruction.text: instruction
            routeDistance.text: distance
        }
    } */
}
