import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

Window {
    visible: true

    width: 800
    height: 600

    ColumnLayout
    {
        anchors.fill: parent
        RowLayout
        {
            id: layout
            //anchors.fill: parent
            //width: parent.width
            //anchors.fill: parent.width
            spacing: 6

            Rectangle
            {
                color: 'teal';
                Layout.fillWidth: true
                //anchors.top: mainForm.bottom
                Layout.minimumWidth: 50
                //Layout.preferredWidth: parent.width
                Layout.minimumHeight: 150
                Layout.preferredHeight: 300

                MyMapView
                {
                    anchors.fill: parent
                    anchors.margins: 3
                }
            }

            Rectangle
            {
                color: 'plum'
                Layout.fillWidth: true
                Layout.minimumWidth: 50
                //Layout.preferredWidth: parent.width
                Layout.minimumHeight: 150
                Layout.preferredHeight: 300

                MainForm {
                    anchors.fill: parent
                    anchors.margins: 3
                    id: mainForm
                    mouseArea.onClicked: {
                        Qt.quit();
                    }
                }

            }
        }
        RowLayout
        {
            Rectangle
            {
                color: 'green'
                Layout.fillWidth: true
                Layout.minimumWidth: 50
                //Layout.preferredWidth: parent.width
                Layout.minimumHeight: 150
                Layout.preferredHeight: 300

                MyMapSearch
                {
                    anchors.fill: parent
                    anchors.margins: 3
                }
            }

        }
    }
}
