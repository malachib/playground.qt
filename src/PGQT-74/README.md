Document v0.1

# 1. Scope

## 1.1. poc1

Lame custom component flavor [1]

## 1.2. poc2

QML singleton flavor [1]

## 1.3. poc3

Lame poc1 + poc2 hybrid [1]

## 1.4. poc4

This explores a custom usage of styles as described by [1.2]
Favoring [1.3] approach

# 2. Opinions & Observations

## poc1 style

## poc2 style

Noteworthy is the explicit 'singleton' demand [1]

## poc3 style

## poc4 style

It seems this approach might actually be poc3 approach in disguise [2]
Be advised there are a *lot* of considerations for a custom style [1.3]

## Global behavior

It seems that poc2, poc3 and poc4 approaches all implicitly mandate a global style for the entire app.

# 4. Findings

## 4.1. Definition of a Style

"In Qt Quick Controls, a style is essentially a set of QML files within a single directory." [1.3]

# References

1. https://wiki.qt.io/Qml_Styling
	1. https://doc.qt.io/qt-6.5/qtquickcontrols-styles.html
	2. https://doc.qt.io/qt-6.5/qquickstyle.html
	3. https://doc.qt.io/qt-6.5/qtquickcontrols-customize.html#creating-a-custom-style
2. https://stackoverflow.com/questions/74866858/in-pyside6-or-c-qt-how-do-i-set-a-custom-qml-style-at-runtime
