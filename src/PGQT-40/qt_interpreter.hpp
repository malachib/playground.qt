// All references see README.md
#pragma once

#include <QGraphicsItem>
#include <QObject>
#include <QThread>

namespace pybind11 {

class module_;

}

// Only threaded to speed up startup
// NOTE: Python startup is pretty quick.  Less than 40ms on a 2013-era Xeon
class QtPythonInterpreter : public QThread
{
    Q_OBJECT
};

class QtInterpreter : public QObject
{
    Q_OBJECT

public:
    QtInterpreter(QObject* parent);
    ~QtInterpreter();

    enum FakeCommand
    {
        RandomPoint,
        RandomLine
    };

    union FakeParameter
    {
        QString* str;

    };

    FakeParameter fakeParameters[3];

    void init(pybind11::module_&);

signals:
    // a signal emitting a graphic to be drawn as a pointer
    void drawGraphic(QGraphicsItem* item);

    // clears graphic canvas
    void clear();

    // python regular std output
    void std_write(QString message);

    // specialized info, not generated from python
    void info(QString message);

    // interpreter (python) generated error
    void error(QString message);

public slots:
    // a public slot that accepts and expression string and parses/evaluates it
    void parseAndEvaluate(QString entry);

    void fakeEvaluate(FakeCommand);
};
