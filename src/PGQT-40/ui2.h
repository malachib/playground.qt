#pragma once

#include <QWidget>

class QtInterpreter;
class QPlainTextEdit;
class QBoxLayout;

class Ui2 : public QWidget
{
public:
    Ui2(QWidget* parent = nullptr) : QWidget(parent) {}

    QPlainTextEdit* editor_;
    std::string filename_;

    bool openFile(const std::string& filename);
    bool saveFile(const std::string& filename);
    bool saveFile() { return saveFile(filename_); }

    // returns false if operation should be canceled
    // returns true if a save or discard was selected
    bool promptSave();

    // Clears editor, checks first if content is modified and if so,
    // prompts first to be sure that's what the user wants.
    // return same result as promptSave
    bool safeClear();
};
