#include <QGraphicsView>
#include <QVBoxLayout>

#include "canvas_widget.hpp"


CanvasWidget::CanvasWidget(QWidget* parent) : QWidget(parent),
    scene(this)
{
    auto layout = new QVBoxLayout(this);    // Just for testing
    auto view = new QGraphicsView(&scene, this);

    view->setRenderHint(QPainter::Antialiasing);
    //scene.setSceneRect(0, 0, 1000, 1000);

    /*
    QBrush blueBrush(Qt::blue);
    QPen outlinePen(Qt::black);
    outlinePen.setWidth(2);

    scene.addRect(1, 1, 100, 100, outlinePen, blueBrush); */

    //view->show();

    // DEBT: See if we can eliminate this layout/look into why a layout is even
    // needed
    layout->addWidget(view);
}


void CanvasWidget::clear()
{
    scene.clear();
}

void CanvasWidget::addGraphic(QGraphicsItem* item)
{
    scene.addItem(item);
}
