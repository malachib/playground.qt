#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>

#include "message_widget.hpp"


MessageWidget::MessageWidget(QWidget* parent) : QWidget(parent)
{
    auto layout = new QHBoxLayout(this);
    auto label = new QLabel();

    label->setText("Message:");

    layout->addWidget(label);
    layout->addWidget(&message);

    // Figured this out with a bit of trial and error, [2.1] somewhat corroborates my story
    // assigning to palette() seems optional, like we get a default/null one - doing anyway to match student
    QPalette p = palette();
    qDebug() << "MessageWidget: ctor " << p.highlight().color();
    p.setColor(QPalette::Highlight, Qt::red);
    // Despite QPalette being an implicit shared object [13] [14], we still must do setPalette
    // *after* setColor.  Evidently this is due to "data is copied only if and when a function writes to it" [14]
    // I wouldn't expect that to include one-off methods like setColor, but evidently it does (debugger shows us
    // 'p' underlying pointers don't change until setColor is called)
    message.setPalette(p);
    message.setReadOnly(true);
}

void MessageWidget::info(QString v)
{
    /*
    setProperty("cssClass", "");

    message.setStyleSheet(""); */

    message.setText(v);
}

void MessageWidget::error(QString v)
{
    // This works quite well, but doesn't conform to assignment requirement to use 'highlight'
    //message.setStyleSheet("background-color: lightpink");

    // Guidance from [2] and [3] - however, doesn't work yet
    //setProperty("cssClass", "[ 'message-error' ]");

    message.setText(v);
    message.selectAll();

    // diagnostic to ensure QPalette is set right
    auto b = message.palette().highlight();
    auto c = b.color();

    qDebug() << "MessageWidget: error " << c << " text=" << v;
}
