#include <cmath>

#include <QPainter>

#include "qgraphics_arc_item.hpp"

QGraphicsArcItem::QGraphicsArcItem(const Arc& arc, QGraphicsItem* parent) :
    QGraphicsEllipseItem(arc.p1().x(), arc.p1().y(), arc.p2().x(), arc.p2().y(), parent)
{
    double a = arc.radian_spanning_angle() * 2880 / M_PI;

    setStartAngle(arc.starting_angle());
    setSpanAngle(arc.spanning_angle());
}

// "The startAngle and spanAngle must be specified in 1/16th of a degree,
// i.e. a full circle equals 5760 (16 * 360). Positive values for the angles
// mean counter-clockwise while negative values mean the clockwise direction.
// Zero degrees is at the 3 o'clock position." [12.1]
void QGraphicsArcItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    //painter->setRenderHint( QPainter::Antialiasing ); // Not needed since view itself has this hint
    painter->drawArc(this->rect(), this->startAngle(), this->spanAngle());
}
