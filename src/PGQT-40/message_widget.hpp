#pragma once

#include <QLineEdit>
#include <QWidget>

class MessageWidget : public QWidget
{
    Q_OBJECT

private:
    QLineEdit message;

public:
    MessageWidget(QWidget* parent = nullptr);

public slots:
    // a public slot accepting an informational message to display, clearing any error formatting
    void info(QString message);

    // a public slot accepting an error message to display as selected text highlighted with a red background.
    void error(QString message);
};
