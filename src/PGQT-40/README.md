# Maureen K parity project

Document v0.1

## 1. Main Qt App

`main_window` is manually created and used as app's main window as per implied instruction [1] p. 4
Therefore, wizard-created mainwindow.h is present but dormant.

### 1.1. QSS

Not necessary for the assignment.  Interested in using selectors/properties rather than inline CSS

## 2. Dependencies

### 2.1. slisp

Assignment [1] bears similarity to slisp from [7], but they are not the same

### 2.2. Python

Since I am not interested in becoming expert on slisp, nor does the particular slisp student
is using seem to be readily available, I am looking into embedding python as per [4] and [5] though [6] is maybe a possibility too.

#### 2.2.1. pybind11

API reference is at [5.4]

##### 2.2.1.1. CMake

To get `find_package` to work, the `apt install` was necessary for me ala [8]
The `pip install` method seemed to install `pybind11` itself, but CMake still couldn't find it

Remember one also needs `pybind11::embed` as part of `target_link_libraries` [5.1]

##### 2.2.1.2. Problem: Python.h not found

Getting `Python.h: No Such file or directory` as described in:

* https://github.com/CMU-Perceptual-Computing-Lab/openpose/issues/1038

_Resolution_: be sure to specify Python package in `target_link_libraries` (either [5.1] or [5.3])

##### 2.2.1.3. Problem: slots error

Yes, Qt does interfere with variables named slots [5.2]

_Resolution_: Be sure any Python.h including things happen before Qt includes

# References

1. Project 3: sldraw Applied Software Design - Fall22.pdf
2. https://stackoverflow.com/questions/36248322/where-should-qt-style-sheet-code-be-placed
   1. https://stackoverflow.com/questions/27283435/qt-set-background-color-of-qlineedit
3. https://forum.qt.io/topic/85946/qt-style-sheets-cascading-classes-selectors/29
4. https://www.reddit.com/r/cpp/comments/hlabb3/what_is_the_best_way_to_embed_python_on_a_qtc/ 
5. https://pybind11.readthedocs.io/en/stable/advanced/embedding.html
   1. https://pybind11.readthedocs.io/en/stable/cmake/index.html
   2. https://github.com/pybind/pybind11/issues/2305 
   3. https://cmake.org/cmake/help/v3.12/module/FindPython.html
   4. https://pybind11.readthedocs.io/en/stable/reference.html
6. https://docs.python.org/3/extending/embedding.html 
7. https://github.com/bailesofhey/slisp
8. https://stackoverflow.com/questions/54704599/how-to-apt-install-python-pybind11
9. https://stackoverflow.com/questions/56953822/use-c-object-in-python-with-pybind11
   1. https://pybind11.readthedocs.io/en/stable/advanced/pycpp/object.html#casting-back-and-forth
   2. https://pybind11.readthedocs.io/en/stable/classes.html
10. https://stackoverflow.com/questions/15176458/how-can-i-emit-a-signal-from-another-class
11. https://github.com/pybind/pybind11/issues/1622
12. https://doc.qt.io/qt-6.2/qpainter.html
   1. https://doc.qt.io/qt-6.2/qpainter.html#drawArc
13. https://doc.qt.io/qt-6.2/qpalette.html
14. https://doc.qt.io/qt-6.2/implicit-sharing.html
15. https://stackoverflow.com/questions/15994194/how-to-convert-x-y-coordinates-to-an-angle
16. qt_ide.docx (Matt S)
17. https://stackoverflow.com/questions/4341492/respond-to-application-wide-hotkey-in-qt
18. https://stackoverflow.com/questions/41252137/qfiledialog-prompt-to-overwrite-if-selection-changed
   1. https://stackoverflow.com/questions/9285575/how-can-i-get-a-qfiledialog-to-prompt-for-overwrite
   2. https://forum.qt.io/topic/137775/how-to-get-overwrite-result-from-qfiledialog
19. https://stackoverflow.com/questions/43851513/how-can-i-tell-if-a-qfiledialog-was-closed-without-selecting-a-file
20. https://www.qtcentre.org/threads/43158-How-to-center-main-window-on-screen

