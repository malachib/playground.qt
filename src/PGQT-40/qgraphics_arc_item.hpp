#pragma once

#include <QGraphicsEllipseItem>

class Arc
{
    typedef QPointF point_type;

    // p1 SHOULD be "the center of the arc" [1]
    // p2 SHOULD be "the starting point of the arc" [1]
    // spanning angle SHOULD be "in radians" [1]
    // None of these are true, and p2 is particularly difficult possibly requiring a bit of simple trig [15]

    point_type p1_;
    point_type p2_;
    double radian_spanning_angle_ = 0;
    int spanning_angle_;
    int starting_angle_ = 0;

public:
    Arc(point_type p1, point_type p2, int spanning_angle) :
        p1_(p1),
        p2_(p2),
        spanning_angle_(spanning_angle)
    {}

    Arc() = default;

    const point_type& p1() const { return p1_; }
    const point_type& p2() const { return p2_; }
    double radian_spanning_angle() const { return radian_spanning_angle_; }
    int spanning_angle() const { return spanning_angle_; }
    int starting_angle() const { return starting_angle_; }

    void set_starting_angle(int v) { starting_angle_ = v; }
    void set_spanning_angle(int v) { spanning_angle_ = v; }
};


// NOTE: Doesn't support starting angle
class QGraphicsArcItem : public QGraphicsEllipseItem
{
    //Q_OBJECT

public:
    QGraphicsArcItem(QGraphicsItem* parent = nullptr) :
        QGraphicsEllipseItem(parent) {}
    QGraphicsArcItem(const Arc& arc, QGraphicsItem* parent = nullptr);

    void paint(QPainter*, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
};
