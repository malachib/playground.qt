#pragma once

#include <QLabel>
#include <QLineEdit>
#include <QWidget>

class REPLWidget : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString prompt READ prompt WRITE setPrompt)

private:
    QString prompt_;
    QLabel label;
    QLineEdit lineEdit;

public:
    REPLWidget(QWidget* parent = nullptr);

    const QString& prompt() { return prompt_; }
    void setPrompt(QString s)
    {
        prompt_ = s;
        label.setText(s);
    }

    // DEBT: This feels off - needing to wrap up lineEdit.setFocus
    void setFocus()
    {
        QWidget::setFocus();
        lineEdit.setFocus();
    }

signals:
    // A signal that sends the current edited text as a QString when the return key is pressed.
    void lineEntered(QString);
};


class QtInterpreter;

namespace testing {

class FakeREPL : public QWidget
{
    Q_OBJECT

public:
    FakeREPL(QtInterpreter& interpreter, QWidget* parent);
};

}
