// All references from README.md
#ifdef USING_pybind11
#include <cstdio>
#include <iostream>
#include <pybind11/embed.h>

namespace py = pybind11;
#else
#warning "Couldn't activate pybind11"
#endif

#include <QBrush>
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QTime>

#include "qgraphics_arc_item.hpp"
#include "qt_interpreter.hpp"

#ifdef USING_pybind11
static QtInterpreter* qti = nullptr;

// This is externally provided code from [11] - works quite well
PYBIND11_EMBEDDED_MODULE(my_sys, m) {
    struct my_stdout {
        my_stdout() = default;
        my_stdout(const my_stdout &) = default;
        my_stdout(my_stdout &&) = default;
    };

    py::class_<my_stdout> my_stdout(m, "my_stdout");
    my_stdout.def_static("write", [](py::object buffer) {
        std::string out = buffer.cast<std::string>();
        std::cout << out;
        // Careful because CR/LFs come through here and would wipe out appearance
        // if a pure setText is used
        emit qti->std_write(QString::fromStdString(out));
    });
    my_stdout.def_static("flush", []() {
        std::cout << std::flush;
    });

    m.def("hook_stdout", []() {
        auto py_sys = py::module::import("sys");
        auto my_sys = py::module::import("my_sys");
        py_sys.attr("stdout") = my_sys.attr("my_stdout");
    });
}

std::string to_string(const QPointF& p)
{
    std::string s = "<qt.Point x=";

    s += std::to_string(p.x());
    s += ", y=";
    s += std::to_string(p.y());
    s += '>';

    return s;
}


std::string to_string_rect(const QRectF& r)
{
    std::string s = "<qt.Rect p1=";

    s += to_string(r.topLeft()) + ", ";
    s += "p2=" + to_string(r.bottomRight());
    s += '>';

    return s;
}


PYBIND11_EMBEDDED_MODULE(qt, m)
{
    //QtInterpreter* qti = m.attr("qt_interpreter").cast<QtInterpreter*>();

    // See references [9] and [9.1]
    py::class_<QtInterpreter> test(m, "QtInterpreter");

    // As per [9.2] - we can see why PySide etc is so useful
    py::class_<QPointF>(m, "Point").
            def(py::init()).
            def(py::init<int, int>()).
            def("setX", &QPointF::setX).
            def("setY", &QPointF::setY).
            def("getX", &QPointF::x).
            def("getY", &QPointF::y).
            def("__repr__", &to_string);

    py::class_<QLineF>(m, "Line").
            def(py::init<QPointF, QPointF>()).
            def("p1", &QLineF::p1).
            def("p2", &QLineF::p2).
            def("setP1", &QLineF::setP1).
            def("setP2", &QLineF::setP2);

    py::class_<QRectF>(m, "Rect").
            def(py::init<QPointF, QPointF>()).
            def("__repr__", &to_string_rect);

    py::class_<Arc>(m, "Arc").
            def(py::init<QPointF, QPointF, int>()).
            def("p1", &Arc::p1).
            def("p2", &Arc::p2).
            //def("setP1", &QLine::setP1).
            //def("setP2", &QLine::setP2);
            def_property("starting_angle", &Arc::starting_angle, &Arc::set_starting_angle).
            def_property("spanning_angle", &Arc::spanning_angle, &Arc::set_spanning_angle).
            def("__repr__", [](const Arc& v)
            {
                std::string s = "<qt.Arc spanning_angle=";

                s += std::to_string(v.spanning_angle()) + ", ";
                s += "starting_angle=" + std::to_string(v.starting_angle()) + ", ";
                s += "p1=" + to_string(v.p1()) + ", ";
                s += "p2=" + to_string(v.p2());

                s += '>';

                return s;
            });


    m.def("initialize", [](QtInterpreter* q)
    {
        qti = q;
    });

    m.def("test", []()
    {
        qti->fakeEvaluate(QtInterpreter::RandomPoint);
    });

    /*
     * This enters, but we can't get a QPoint back from it yet
    m.def("draw", [](py::object obj)
    {
        py::handle h = obj.get_type();

        //auto& qr = h.cast<QRect&>();
        //auto& qp = h.cast<QPoint&>();
        //auto qpp = h.cast<QPoint*>();
    }); */

    // https://stackoverflow.com/questions/46388755/pybind-overloaded-functions
    m.def("draw", [](const QPointF& point)
    {
        qDebug() << "Draw point";

        // I like radius 10 better than 2.  Note this is a flawed radius, because origin may
        // not be correct
        auto circle = new QGraphicsEllipseItem(point.x(), point.y(), 10, 10);
        circle->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
        circle->setBrush(Qt::green);

        // See [10] for viability of this
        emit qti->drawGraphic(circle);
        emit qti->info("point drawn [python]");
    });


    m.def("draw", [](const Arc& arc)
    {
        qDebug() << "Draw arc";

        auto ellipse = new QGraphicsArcItem(arc);

        emit qti->drawGraphic(ellipse);
    });

    m.def("draw", [](const QLineF& line)
    {
        emit qti->drawGraphic(new QGraphicsLineItem(line));
    });

    m.def("draw", [](const QRectF& rect)
    {
        emit qti->drawGraphic(new QGraphicsRectItem(rect));
    });

    m.def("clear", []()
    {
        emit qti->clear();
    });

    qti->init(m);
}

void QtInterpreter::init(pybind11::module_&)
{
    // NOTE: This is viable, but would require a lambda capture all the way through to get
    // the 'this' pointer.  Even that is not a deal breaker, but knowing this will work combined
    // with existing functioning code suffices for a proof of concept.
}

#endif

QtInterpreter::QtInterpreter(QObject* parent) :
    QObject(parent)
{
    qDebug() << "QtInterpreter: ctor entry " << QTime::currentTime();

#ifdef USING_pybind11
    py::initialize_interpreter();
    auto module = py::module_::import("qt");
    module.attr("initialize")(py::cast(this));

    py::module::import("my_sys").attr("hook_stdout")();

    qDebug() << "QtInterpreter: ctor phase 1 " << QTime::currentTime();

    py::exec("import qt");
    py::exec("print('hello', flush=True)");

    py::exec("p1=qt.Point(10, 10)");
    py::exec("p2=qt.Point(100, 100)");
    py::exec("p3=qt.Point(200, 200)");
    py::exec("arc1=qt.Arc(p1, p2, 2835)");
    py::exec("arc2=qt.Arc(p2, p3, 5670 // 4)");
    py::exec("arc2.starting_angle = 5670 // 4");
    py::exec("rect1=qt.Rect(p1, p2)");
#endif
    qDebug() << "QtInterpreter: ctor exit " << QTime::currentTime();
}


QtInterpreter::~QtInterpreter()
{
#ifdef USING_pybind11
    py::finalize_interpreter();
#endif
}


void QtInterpreter::parseAndEvaluate(QString entry)
{
#ifdef USING_pybind11
    try
    {
        py::exec(entry.toStdString());

        std::cout << std::flush;
    }
    catch(const py::error_already_set& e)
    {
        emit error(e.what());
    }



#endif
}

void QtInterpreter::fakeEvaluate(FakeCommand cmd)
{
    switch(cmd)
    {
        case RandomPoint:
            // Guidance from https://forum.qt.io/topic/98443/how-to-draw-circle-with-line-pattern-using-qgraphicsscene/2
            auto circle = new QGraphicsEllipseItem(0, 0, 100, 100);
            circle->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
            circle->setBrush(Qt::green);

            emit drawGraphic(circle);
            emit info("point drawn");
            break;
    }
}
