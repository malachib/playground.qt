#pragma once

#include <QWidget>

#include "ui2.h"

class QtInterpreter;
class QPlainTextEdit;
class QBoxLayout;

namespace manual {

class Ui1 : public QWidget
{
public:
    Ui1(QWidget* parent = nullptr) : QWidget(parent) {}
};





class MainWindow : public QWidget
{
    Q_OBJECT

    void init_menubar();
    void init_layout();
    void init_ui1();
    void init_ui2();

    Ui1 ui1_;
    Ui2 ui2_;
    QtInterpreter* interpreter_;
    QBoxLayout* holder_;

    // +++ ui2 portion
    void openFileDialog();
    void saveFileDialog();

private slots:
    // For activation from menu
    void saveFile();
    // --- ui2

public:
    MainWindow(QWidget* parent = nullptr);

    // Requirement from [17]
    MainWindow(const MainWindow&) = delete;
    MainWindow(MainWindow&&) = delete;
};

}
