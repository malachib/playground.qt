#include "ui2.h"

#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QPlainTextEdit>

bool Ui2::openFile(const std::string& filename)
{
    // DEBT: Annoying to convert between string formats
    auto _filename = QString::fromStdString(filename);
    QFile f(_filename);
    f.open(QFile::ReadOnly);
    QString s = QString::fromLatin1(f.readAll());
    editor_->clear();
    editor_->insertPlainText(s);
    filename_ = filename;

    // DEBT: return false on failure
    return true;
}


bool Ui2::saveFile(const std::string& filename)
{
    filename_ = filename;
    return false;
}


bool Ui2::promptSave()
{
    QMessageBox m(this);

    m.setText("Editor contains data");
    m.setInformativeText("informative");
    // If we need to actually customize these names, there's
    // https://doc.qt.io/qt-6/qmessagebox.html#advanced-usage
    m.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

    switch(m.exec())
    {
        case QMessageBox::Save:
            saveFile();
            return true;

        case QMessageBox::Discard:
            return true;

        case QMessageBox::Cancel:
            return false;

        default:
            qWarning() << "invalid return from QMessageBox";
            return false;
    }
}

bool Ui2::safeClear()
{
    // DEBT: isModified is really isTouched - there is no actual content changed
    // indicator that I can find.  That said, this has a full revision/undo system
    // so the original content IS lurking in there and can be compared against somehow
    auto doc = editor_->document();
    qDebug() << "isModified:" << doc->isModified() << ", revision:" << doc->revision();
    bool proceed = true;
    if(doc->isModified())
    {
        proceed = promptSave();
    }

    if(proceed)
    {
        editor_->clear();
    }

    return proceed;
}
