#pragma once

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QWidget>

class CanvasWidget : public QWidget
{
    Q_OBJECT

private:
    QGraphicsScene scene;

public:
    CanvasWidget(QWidget* parent = nullptr);

public slots:
    // A public slot that accepts a signal in the form of a QGraphicsItem pointer containing an
    // object derived from QGraphicsItem to draw
    void addGraphic(QGraphicsItem* item);
    void clear();
};
