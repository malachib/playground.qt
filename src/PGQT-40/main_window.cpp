// All references in README.md

#include <QActionGroup>
#include <QBoxLayout>
#include <QCheckBox>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QGridLayout>
#include <QGuiApplication>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QScreen>
#include <QShortcut>
#include <QVBoxLayout>

#include "canvas_widget.hpp"
#include "main_window.hpp"
#include "message_widget.hpp"
#include "qt_interpreter.hpp"
#include "repl_widget.hpp"

namespace manual {

void MainWindow::init_menubar()
{
    QMenuBar* menu = new QMenuBar(this);
    QMenu* file = new QMenu("&File");
    QAction* action;

    menu->addMenu(file);
    file->addAction("&New", this, [=]()
    {
        ui2_.safeClear();
    });
    file->addAction("&Open", this, &MainWindow::openFileDialog);
    file->addAction("&Save", this, &MainWindow::saveFileDialog);
    file->addAction("Save &as", this, &MainWindow::saveFileDialog);
    file->addSeparator();
    file->addAction("&Quit");

    QMenu* edit = new QMenu("&Edit");
    menu->addMenu(edit);

    QMenu* view = new QMenu("&View");
    menu->addMenu(view);

    auto optionGroup = new QActionGroup(this);

    action = optionGroup->addAction("View &1");
    action->setData(1);
    view->addAction(action);
    action = optionGroup->addAction("View &2");
    action->setData(2);
    view->addAction(action);

    // Technique from PGQT-41
    connect(optionGroup, &QActionGroup::triggered, this, [&](QAction* action)
    {
        int v = action->data().value<int>();

        qDebug() << "optionGroup v:" << v;

        QLayoutItem* layoutItem = holder_->takeAt(0);

        if(layoutItem != nullptr)
        {
            auto layout = layoutItem->layout();
            auto widget = layoutItem->widget();

            widget->setParent(nullptr);

            delete layoutItem;
        }

        switch(v)
        {
            case 1:
                holder_->addWidget(&ui1_);
                break;

            case 2:
                holder_->addWidget(&ui2_);
                break;
        }
    });
}

void MainWindow::init_layout()
{
    // Guidance from [20] - centers main app window
    // Sick of it always appearing RIGHT OVER my output area
    move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());

    /*
    QGridLayout* l = new QGridLayout(this);
    setLayout(l);
    l->addWidget(centralWidget());
    l->setContentsMargins(0, 0, 0, 0); */
    //central_.setParent(this);

    holder_ = new QBoxLayout(QBoxLayout::LeftToRight, this);
    QLayout* l = holder_;
    auto placeholderWidget = new QWidget;

    placeholderWidget->setObjectName("placeholder");

    holder_->addWidget(&ui1_);

    // Looks good, but interferes with menu bar selection
    //l->setContentsMargins(0, 0, 0, 0);
}

void MainWindow::init_ui1()
{
    auto layout = new QVBoxLayout(&ui1_);
    auto message = new MessageWidget;

    layout->addWidget(message);
    message->info("Starting up");

    auto canvas = new CanvasWidget;
    auto repl = new REPLWidget;
    auto fake_repl = new testing::FakeREPL(*interpreter_, this);

    layout->addWidget(canvas);
    layout->addWidget(repl);
    layout->addWidget(fake_repl);

    connect(interpreter_, &QtInterpreter::info, message, &MessageWidget::info);
    connect(interpreter_, &QtInterpreter::error, message, &MessageWidget::error);
    connect(interpreter_, &QtInterpreter::drawGraphic, canvas, &CanvasWidget::addGraphic);
    connect(interpreter_, &QtInterpreter::clear, canvas, &CanvasWidget::clear);
    connect(repl, &REPLWidget::lineEntered, interpreter_, &QtInterpreter::parseAndEvaluate);

#ifdef USING_pybind11
    repl->setPrompt("python>");
    // DEBT: Should really check state of python interpreter here
    message->info("Interpreter online");
#endif

    repl->setFocus();
}


void MainWindow::init_ui2()
{
    auto layout = new QGridLayout(&ui2_);

    auto editor = new QPlainTextEdit;
    auto result = new QPlainTextEdit;
    auto query = new QLineEdit;
    auto trace = new QCheckBox("Trace");

    ui2_.editor_ = editor;

    int revision = editor->document()->revision();
    editor->insertPlainText("print('hello')");
    editor->document()->setModified(false);
    revision = editor->document()->revision();

    result->setReadOnly(true);

    layout->addWidget(editor, 0, 0);
    layout->addWidget(result, 0, 1);

    auto lineLayout = new QHBoxLayout();

    lineLayout->addWidget(new QLabel("Query:"));
    lineLayout->addWidget(query, 1);
    lineLayout->addWidget(trace);

    layout->addLayout(lineLayout, 1, 0, 1, 2);

    connect(query, &QLineEdit::editingFinished, this, [=]()
    {
        interpreter_->parseAndEvaluate(query->text());
        query->clear();
    });

    // this appends newlines
    //connect(interpreter_, &QtInterpreter::std_write, result, &QPlainTextEdit::appendPlainText);

    // conveniently, hidden cursor stays at the end, so insertPlainText becomes append, but without newline
    // deviating from [16] requirement since I want to accumulate output and not clear it
    connect(interpreter_, &QtInterpreter::std_write, result, &QPlainTextEdit::insertPlainText);
    connect(interpreter_, &QtInterpreter::error, result, &QPlainTextEdit::setPlainText);

    // Guidance from [17]
    // Conveniently, attaching to &ui2_ makes this shortcut only active when ui2_ itself is active.  Cool.
    auto shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_P), &ui2_);
    shortcut->setContext(Qt::ApplicationShortcut);
    connect(shortcut, &QShortcut::activated, this, [=]()
    {
        //qDebug() << "Ctrl+P pressed";
        result->clear();
        interpreter_->parseAndEvaluate(editor->toPlainText());
    });
}


MainWindow::MainWindow(QWidget* parent) : QWidget(parent)
{
    QCoreApplication::setOrganizationName("Malachi");
    QCoreApplication::setApplicationName("PGQT-40");

    resize(800, 600);

    init_menubar();
    init_layout();

    // Guidance from [2] and [3]
    QFile styleFile(":/styles.qss");

    styleFile.open(QFile::ReadOnly);
    QString s = QString::fromLatin1(styleFile.readAll());
    setStyleSheet(s);
    styleFile.close();

    interpreter_ = new QtInterpreter(this);

    init_ui1();
    init_ui2();
}



void MainWindow::saveFile()
{
    if(ui2_.filename_.empty())
        saveFileDialog();
    else
    {
        ui2_.saveFile();
    }
}


void MainWindow::openFileDialog()
{
    // DEBT: Grab these from QtInterpreter, or similar
    QString title = tr("Open Python File");
    QString filter = "Python Files (*.py)";

    QString filename = QFileDialog::getOpenFileName(this, title, QString(), filter);

    if(ui2_.safeClear())
        ui2_.openFile(filename.toStdString());
}


void MainWindow::saveFileDialog()
{
    // DEBT: Grab these from QtInterpreter, or similar
    QString title = tr("Save Python File");
    QString filter = "Python Files (*.py)";

    // file dialog already has its own check for overwrite.  It doesn't seem we can change its
    // appearance as implied by [18], though I am not 100% sure.  So, not using getSaveFileName

    //QString filename = QFileDialog::getSaveFileName(this, title, QString(), filter);

    QFileDialog fd(this, title, QString(), filter);

    fd.setFileMode(QFileDialog::AnyFile);
    fd.setOption(QFileDialog::DontConfirmOverwrite);
    int ret = fd.exec();

    // As per [19], this is a cancel
    if(ret == 0) return;

    auto s = fd.selectedFiles();
    auto filename = s.at(0);

    if(QFile::exists(filename))
    {
        QMessageBox m;

        m.setWindowTitle("File exists");
        m.addButton("Overwrite", QMessageBox::AcceptRole);
        m.addButton(QMessageBox::Cancel);

        int ret = m.exec();

        switch(ret)
        {
            case QMessageBox::Cancel:
                return;

            default:
                break;
        }
    }

    ui2_.saveFile(filename.toStdString());
}





}
