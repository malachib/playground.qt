#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "qt_interpreter.hpp"
#include "repl_widget.hpp"

REPLWidget::REPLWidget(QWidget* parent) : QWidget(parent),
    lineEdit(this)
{
    auto layout = new QHBoxLayout(this);

    label.setText("slisp>");

    layout->addWidget(&label);
    layout->addWidget(&lineEdit);

    connect(&lineEdit, &QLineEdit::returnPressed, this, [&]()
    {
        emit lineEntered(lineEdit.text());
        lineEdit.clear();
    });
}



namespace testing {

FakeREPL::FakeREPL(QtInterpreter& interpreter, QWidget* parent) : QWidget(parent)
{
    auto layout = new QGridLayout(this);

    auto btn1 = new QPushButton(layout->widget());
    btn1->setText("Random Point");

    layout->addWidget(btn1);

    connect(btn1, &QPushButton::clicked, this, [&]{
        interpreter.fakeEvaluate(QtInterpreter::RandomPoint);
    });
}

}
