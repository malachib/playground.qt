import QtQuick 2.3
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

// where we draw the main hourly thingy
Item {
    ZoomBlur {
        scale: 1.3
        anchors.fill: image2
        source: image2
        id: zoom1
        length: 12
        samples: 12
    }

    ZoomBlur {
        scale: 2
        anchors.fill: image2
        source: zoom1
        id: zoom2
        length: 3
        opacity: 0.5
        samples: 12
        //transparentBorder: true
    }

    Image {
        id: image

        // make sure we stay big so mipmap resize looks right
        fillMode: Image.PreserveAspectCrop

        // presample at a reasonably high res
        sourceSize.width: 1024
        sourceSize.height: 1024

        source: "cloud-fog.svg"
        visible: false

        // presuming better for pre-redering icon than shader, as long as realtime resizing isn't needed
        mipmap: true

        anchors.fill: parent
    }

    // change color of it
    ColorOverlay {
        id: overlay
        anchors.fill: image
        source: image
        color: "blue"
        visible: false
    }

    /*
    // now resize it with the mipmap smoothing
    // https://stackoverflow.com/questions/23286666/qml-image-smooth-property-not-working
    ShaderEffectSource {
        id: src
        sourceItem: overlay
        mipmap: true
    }

    ShaderEffect {
        anchors.fill: parent
        id: image2

        clip: true

        scale: 1.3
        property var source: src
    } */

    ColorOverlay {
        id: image2
        anchors.fill: image
        source: image
        color: "blue"
        visible: true
    }
}
