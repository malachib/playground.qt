import QtQuick 2.6
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

Window {
    width: 640
    height: 480
    id: mainWindow
    visible: true
    color: '#003000'

    Loader {
        source: "main.qml"
    }
}
