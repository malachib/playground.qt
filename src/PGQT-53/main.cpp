#include <QGuiApplication>
#include <QQmlApplicationEngine>

// If qt_policy(SET QTP0001 NEW) is present
// then we MUST leave RESOURCE_PREFIX at its default (/qt/qml)
// otherwise it seems PGQT_53 is built without 'Debug.qml'

//#define OLD_WAY 1
//#define RESOURCE_PREFIX ""
#define RESOURCE_PREFIX "/qt/qml"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

#if OLD_WAY
    const QUrl url(u"qrc:" RESOURCE_PREFIX "/PGQT_53/Main.qml"_qs);
    engine.load(url);
#else
    // Presumes qrc:/qt/qml prefix
    engine.loadFromModule("PGQT_53", "Main");
#endif

    return app.exec();
}
