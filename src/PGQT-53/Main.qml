import QtQuick
import QtQuick.Layouts

// NOTE: This all works.  If import is commented out, we get error message:
// "qrc:/qt/qml/PGQT_53/Main.qml:11:5: PGQT.Debug - PGQT is neither a type nor a namespace" which is not
// the error notos-devtool got - the latter being "type not found" suggesting namespace existed, but type wasn't present

import PGQT_53 as PGQT

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ColumnLayout {
        anchors.fill: parent

        PGQT.Debug {
            Layout.minimumHeight: 30
            Layout.fillWidth: true
        }

        PGQT.Debug2 {
            Layout.minimumHeight: 30
            Layout.fillWidth: true
        }

    }
}
