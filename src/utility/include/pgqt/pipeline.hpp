#pragma once

#include "pipeline.h"

namespace pgqt { inline namespace v1 {

template <class In, class Out>
void Pipeline<In, Out>::process(const In* in, Out* out)
{
    for(std::unique_ptr<action_type>& a : pipeline_)
    {
        if(a->enabled())
        {
            a->process(*in, *out);
            // DEBT: Really only needs to happen once, but a pointer copy we can live with
            in = out;
        }
    }
}

}}
