#pragma once

#include <QActionGroup>
#include <QObject>
#include <QMenu>
#include <QSettings>
#include <QString>

#include "utility_global.h"


namespace pgqt { inline namespace v1 {

// All this lifted and adapted from PGQT-41, which in turn lifted it from somewhere
// (see PGQT-41 references)

class UTILITY_EXPORT MruManagerItem //: public QObject
{
    //Q_OBJECT

    const QString filename_;

public:
    MruManagerItem() = default;
    MruManagerItem(QString filename, QObject* parent = nullptr) :
        //QObject(parent),
        filename_{filename}
    {}

    const QString& filename() const { return filename_; }
};

// For now specific to a list of old opened files
class UTILITY_EXPORT MruManager : public QObject
{
    Q_OBJECT

    const char* settingsRecentFiles = "recentFiles";

    // Menu to which our MRUs are attached.  Useful to clear items back out again
    // DEBT: I don't like keeping this here, feels wrong
    QMenu* menu_;
    QActionGroup actionGroup_;

public:
    MruManager(QObject* parent = nullptr) :
        QObject(parent),
        actionGroup_{this}
    {}

    ///
    /// \brief create
    /// \param after nullptr means add at the end, otherwise insert after specified element
    /// \return
    ///
    int create(QMenu*, QAction* after = nullptr);
    void refresh();
    void update(QString);
    void clear();

    QStringList recentFiles() const
    {
        QSettings settings;
        return settings.value(settingsRecentFiles).toStringList();
    }

private slots:
    void trigger();

signals:
    void triggered(const MruManagerItem&);
};

}}

// DEBT: Looks like the less fancy way at
// https://stackoverflow.com/questions/28646914/qaction-with-custom-parameter
// could be useful
Q_DECLARE_METATYPE(pgqt::v1::MruManagerItem)

