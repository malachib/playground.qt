#pragma once

#include <forward_list>
#include <memory>

namespace pgqt { inline namespace v1 {

// Relies on the notion that
// "QImage objects can be passed around by value since the QImage
//  class uses implicit data sharing. QImage objects can also be
//  streamed and compared." [8]
// In that 'dest' may in fact be value-copied to and we expect that is efficient
// NOTE: Might be better to do this as a pixel pipeline like OpenGL, but we're OK with QImage for the time being
template <class In, class Out = In>
class PipelineAction
{
protected:
    bool enabled_ = false;

public:
    // Just in case someone really does want a destructor.
    // So far no - but this quiets down compiler warnings
    virtual ~PipelineAction() = default;

    virtual void process(const In&, Out&) = 0;

    bool enabled() const { return enabled_; }
};


template <class In, class Out = In>
class Pipeline
{
    using action_type = PipelineAction<In, Out>;

//protected:
public:
    std::forward_list<std::unique_ptr<action_type> > pipeline_;

public:
    void process(const In*, Out*);
};

}}
