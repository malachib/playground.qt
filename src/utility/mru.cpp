#include <QSettings>
#include <QAction>
#include <QStringList>
#include <QVariant>

#include "pgqt/mru.h"

namespace pgqt { inline namespace v1 {


void MruManager::clear()
{
    for(QAction* a : actionGroup_.actions())
    {
        menu_->removeAction(a);
        actionGroup_.removeAction(a);
    }
}

void MruManager::refresh()
{
}

int MruManager::create(QMenu* menu, QAction* after)
{
    menu_ = menu;
    QStringList recentFilePaths = recentFiles();
    int counter = 0;

    for(const QString& s : recentFilePaths)
    {
        QString display_s = "&";

        display_s += QString::number(++counter);
        display_s += " ";
        display_s += s;

        //QAction mruItem(s);
        auto mruItem = new QAction(display_s, this);

        mruItem->setData(QVariant::fromValue(MruManagerItem(s)));

        actionGroup_.addAction(mruItem);

        if(after == nullptr)
            menu->addAction(mruItem);
        else
            menu->insertAction(after, mruItem);

        // DEBT: Disconnect these on updates/shutdown - might want to use an ActionGroup
        // DEBT: Do these auto-disconnect via GC?  Look into that and notate here
        connect(mruItem, &QAction::triggered, this, &MruManager::trigger);
    }

    return recentFilePaths.count();
}


void MruManager::trigger()
{
    QObject* s = sender();
    auto action = dynamic_cast<QAction*>(s);
    auto item = action->data().value<MruManagerItem>();

    emit triggered(item);
}


void MruManager::update(QString s)
{
    QSettings settings;

    QStringList mruList = recentFiles();

    // For now, don't do anything if item appears in list
    // DEBT: Shuffle this item to the top of the list in this case
    if(mruList.contains(s)) return;

    mruList.prepend(s);

    while(mruList.size() > 8)
        mruList.removeLast();

    settings.setValue(settingsRecentFiles, mruList);
}

}}
