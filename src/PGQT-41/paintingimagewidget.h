#ifndef PAINTINGIMAGEWIDGET_H
#define PAINTINGIMAGEWIDGET_H

#include <QPixmap>
#include <QWidget>

class PaintingImageWidget : public QWidget
{
    Q_OBJECT

    QPixmap pixmap_;
    QImage image_;
    int rotation_ = 0;

public:
    explicit PaintingImageWidget(QWidget *parent = nullptr);

    void setRotation(int);
    void setPixmap(QPixmap);
    void setImage(QImage);

    void reset();

protected:
    void paintEvent(QPaintEvent*) override;

signals:

};

#endif // PAINTINGIMAGEWIDGET_H
