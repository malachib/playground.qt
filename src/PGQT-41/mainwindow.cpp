// See README.md for references
#include <QActionGroup>
#include <QDebug>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QMetaEnum>
#include <QPixmap>
#include <QPushButton>
#include <QSettings>
#include <QSizePolicy>
#include <QSlider>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "paintingimagewidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    pixmapHolder(QBoxLayout::LeftToRight)
{
    ui->setupUi(this);

    // DEBT: Just to fit on Chromebook screen easily
    resize(640, 480);

    title = "PGQT-41";

    QCoreApplication::setOrganizationName("Malachi");
    QCoreApplication::setApplicationName("PGQT-41");

    createMenu();
    createLayout();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::createLayout()
{
    // Wizard-created UI already has a layout, need to interact with that
    // - this->setLayout will get upset [5]
    auto placeholderWidget = new QWidget;

    placeholderWidget->setObjectName("placeholder");

    // NOTE: Don't specify 'this' because Qt will try to auto attach
    // the layout and give you warnings that MainWindow already has one
    auto layout = new QVBoxLayout(placeholderWidget);
    auto controls = new QHBoxLayout;

    auto resetButton = new QPushButton(tr("Reset"));
    auto quitButton = new QPushButton(tr("Quit"));

    slider.setOrientation(Qt::Horizontal);

    connect(resetButton, &QPushButton::clicked, this, &MainWindow::resetClicked);
    connect(quitButton, &QPushButton::clicked, this, &MainWindow::close);
    connect(&slider, &QSlider::valueChanged, this, &MainWindow::rotateFromSlider);

    slider.setMinimum(-360);
    slider.setMaximum(360);

    controls->addWidget(resetButton);
    controls->addWidget(quitButton);
    controls->addWidget(&slider, 4);

    layout->addLayout(&pixmapHolder);
    layout->addLayout(controls);

    setCentralWidget(placeholderWidget);
}


void MainWindow::createMenuTechniques(QMenu* menu)
{
    // Some guidance from [7] and [8] though not working as desired (seems to still have complaint
    // from [8] not selecting just one)
    // Guidance from [17] closed the gap

    auto optionGroup = new QActionGroup(this);

    // setData trickery from [20] and [21]
    QAction* action;
    auto imagePaintedAction = new QAction("Image Painted");
    imagePaintedAction->setData(QVariant::fromValue(Techniques::ImagePaint));
    auto optionSceneAction = new QAction("GraphicsScene");
    optionSceneAction->setData(QVariant::fromValue(Techniques::Scene));

    action = optionGroup->addAction("Pixmap Label");
    action->setData(QVariant::fromValue(Techniques::PixmapLabel));

    action = optionGroup->addAction("Pixmap Painted");
    action->setData(QVariant::fromValue(Techniques::PixmapPaint));
    action->setChecked(true);

    optionGroup->addAction(imagePaintedAction);
    optionGroup->addAction(optionSceneAction);

    optionGroup->setExclusive(true);

    for(QAction* action : optionGroup->actions())
    {
        action->setCheckable(true);
        menu->addAction(action);
    }

    connect(optionGroup, &QActionGroup::triggered, this, [&](QAction* action)
    {
        setTechnique(action->data().value<Techniques>());
    });

    QSettings settings;

    QVariant technique = settings.value(Settings::technique);

    // FIX: "unknown user type with name MainWindow::Techniques" -- hmm, I wonder why
    Techniques v = technique.isNull() ? Techniques::PixmapPaint : technique.value<Techniques>();

    // DEBT: This doesn't check to see if nothing is initialized yet, but seems to be OK
    setTechnique(v);
}


void MainWindow::createMenuMru(QMenu* menu, QAction* after)
{
    QSettings settings;
    QStringList recentFilePaths = settings.value(Settings::recentFiles).toStringList();

    auto list = recentFilePaths.toList();

    for(const QString& s : list)
    {
        //QAction mruItem(s);
        auto mruItem = new QAction(s);

        menu->insertAction(after, mruItem);

        connect(mruItem, &QAction::triggered, this, &MainWindow::fileMruClicked);
    }
}


void MainWindow::createMenu()
{
    // As per [1]
    auto fileMenu = menuBar()->addMenu(tr("&File"));

    fileMenu->addAction(tr("&Open"), this, &MainWindow::fileOpen);
    fileMenu->addSeparator();
    fileMenu->addAction(tr("mru1"), this, &MainWindow::fileMru1);
    QAction* afterMru = fileMenu->addSeparator();
    createMenuMru(fileMenu, afterMru);
    fileMenu->addAction(tr("E&xit"), this, &MainWindow::close);

    auto optionMenu = menuBar()->addMenu(tr("Option"));

    createMenuTechniques(optionMenu);
}

// DEBT: Technically technique is better suited as a property.  Things work well
// enough as is for proof of concept code though
void MainWindow::setTechnique(Techniques v)
{
    // Guidance from [19] to get name from enum
    //auto s = QMetaEnum::fromType<Techniques>().valueToKey((int)v);
    //auto s = QVariant::fromValue(v);  // Works also, produces more robust output
    auto s = QVariant::fromValue(v).toString();

    qInfo() << "setTechnique: v=" << s;

    setWindowTitle(title + ": " + s);

    QLayoutItem* layoutItem = pixmapHolder.takeAt(0);

    // DEBT: Layouts don't quite update how we'd like them (pixmaps stay rendered) when removing item [14], [15]
    if(layoutItem != nullptr)
    {
        auto layout = layoutItem->layout();
        auto widget = layoutItem->widget();

        // This returns a value to placeholder widget
        auto parent = widget->parentWidget();       // just retrieving for reference

        qDebug() << "setTechnique: parent=" << parent;

        if(layout != nullptr) delete layout;

        widget->setParent(nullptr);                 // Thank you [16] for showing us the way!
        //widget->setVisible(false);                // show/hide technique - vastly prefer null'ing parent

        //pixmapHolder.removeWidget(widget);        // takeAt does this already

        //if(widget != nullptr) delete widget;      // Crashes, likely because of our non-new'd widgets

        delete layoutItem;
        //deleteLater();
    }

    /* always empty
    auto list = pixmapHolder.children();
    auto item = list.at(0);
    auto _item = dynamic_cast<QLayoutItem*>(item); */

    //pixmapHolder.removeItem(__item);  // takeAt already removes the item

    resetClicked();

    switch(v)
    {
        case Techniques::PixmapLabel:
            pixmapLabel.setStyleSheet("border: 1px solid green");
            pixmapLabel.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            pixmapHolder.addWidget(&pixmapLabel);
            break;

        case Techniques::ImagePaint:
        case Techniques::PixmapPaint:
            paintingImage.reset();
            pixmapHolder.addWidget(&paintingImage);
            break;


        case Techniques::Scene:
            // Without this we get an odd availability of real estate.
            // Specifically, qt-logo.png results in way more pixels at the top and bottom of the view than one would think
            // DEBT: This is not ideal, it would be better for view/scene to be a tidy rectangle of the exact size
            // of the image in question
            graphicsView.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
            graphicsView.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

            scene.clear();
            graphicsView.setScene(&scene);
            pixmapHolder.addWidget(&graphicsView);
            graphicsView.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
            break;
    }

    // Was needed for hide/show approach but I MUCH prefer removing from parent/readding
    //pixmapHolder.itemAt(0)->widget()->setVisible(true);

    technique = v;

    QSettings settings;

    settings.setValue(Settings::technique, QVariant::fromValue(v));
}

void MainWindow::fileMruClicked()
{
    auto action = dynamic_cast<QAction*>(sender());

    QString filename = action->text();

    QPixmap pixmap;
    bool loaded = pixmap.load(filename);
    qDebug() << "pixmap loaded=" << loaded;
    if(loaded)  setPixmap(pixmap);
}


void MainWindow::fileMru1()
{
    // Fake MRU just for speeding my own testing
    //"../../PGQT-9/qt-logo.png"
    QPixmap pixmap;
    bool loaded = pixmap.load("../PGQT-9/qt-logo.png");
    qDebug() << "pixmap loaded=" << loaded;
    if(loaded)  setPixmap(pixmap);
}


void MainWindow::fileOpen()
{
    // [4]
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Image"),QString(),
                                 "Image Files (*.png *.jpg *.bmp)");

    QPixmap pixmap(filename);

    if(!pixmap.isNull())
    {
        QSettings settings;

        // Guidance from [13]
        auto mruList = settings.value(Settings::recentFiles).toStringList();

        mruList.prepend(filename);

        while(mruList.size() > 8)
            mruList.removeLast();

        settings.setValue(Settings::recentFiles, mruList);

        setPixmap(pixmap);
    }
}


void MainWindow::resetClicked()
{
    slider.setValue(0);
}


void MainWindow::rotateFromSlider(int value)
{
    switch(technique)
    {
        case Techniques::PixmapLabel:
        {
            // As per [6], [7]
            QTransform transform;

            //transform.translate(pixmap.width() / 2, pixmap.height() /2);
            transform.rotate(value);
            pixmapLabel.setPixmap(pixmap.transformed(transform, Qt::SmoothTransformation));
            break;
        }

        case Techniques::ImagePaint:
        case Techniques::PixmapPaint:
        {
            paintingImage.setRotation(value);
            break;
        }

        case Techniques::Scene:
        {
            // Guidance from [11]
            pixmapItem->setRotation(value);
            break;
        }
    }
}


void MainWindow::setPixmap(QPixmap pixmap)
{
    switch(technique)
    {
        case Techniques::PixmapLabel:
        {
            this->pixmap = pixmap;
            pixmapLabel.setPixmap(pixmap);

            // Make this label's maximum size be that of the image.  This way no oddball resizing as it rotates
            QSizePolicy policy;
            pixmapLabel.setMaximumSize(pixmap.size());
            pixmapLabel.setSizePolicy(policy);
            break;
        }

        case Techniques::ImagePaint:
            paintingImage.setImage(pixmap.toImage());
            break;

        case Techniques::PixmapPaint:
            paintingImage.setPixmap(pixmap);
            break;

        case Techniques::Scene:
            scene.clear();
            pixmapItem = scene.addPixmap(pixmap);
            // Guidance from [11]
            auto r = pixmapItem->pixmap().rect();
            pixmapItem->setTransformOriginPoint(r.width() / 2, r.height() / 2);
            graphicsView.fitInView(r, Qt::KeepAspectRatio);
            break;
    }
}
