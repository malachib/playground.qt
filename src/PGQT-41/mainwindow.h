#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QBoxLayout>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QSlider>

#include "paintingimagewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    enum class Techniques
    {
        PixmapLabel,
        PixmapPaint,
        ImagePaint,
        Scene
    };

    Q_ENUM(Techniques)

private:
    struct Settings
    {
        static constexpr const char* recentFiles = "recentFiles";
        static constexpr const char* technique = "technique";
    };

    Ui::MainWindow *ui;

    QString title;

    void createMenu();
    void createMenuTechniques(QMenu* menu);
    void createMenuMru(QMenu* menu, QAction* after);
    void createLayout();

    // Implicit shared [12] so automatically behaves a bit like a shared_ptr
    void setPixmap(QPixmap);

    QSlider slider;
    QBoxLayout pixmapHolder;

    QLabel pixmapLabel;
    QPixmap pixmap;

    PaintingImageWidget paintingImage;

    QGraphicsScene scene;
    QGraphicsView graphicsView;
    QGraphicsPixmapItem* pixmapItem;

    Techniques technique;

    void setTechnique(Techniques);

private slots:
    void resetClicked();
    void fileOpen();
    void fileMru1();
    void rotateFromSlider(int value);
    void fileMruClicked();
};


Q_DECLARE_METATYPE(MainWindow::Techniques)


#endif // MAINWINDOW_H
