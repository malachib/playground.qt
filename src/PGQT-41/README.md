# Image Rotate

Parity code with Cayman H tutoring (CS335 Excercise 8)

# References

1. https://doc.qt.io/qt-6.2/qmenubar.html
2. https://doc.qt.io/qt-6.2/qtwidgets-mainwindows-menus-example.html
3. Image Rotate.pdf
4. https://doc.qt.io/qt-6/qfiledialog.html
5. https://forum.qt.io/topic/53190/qlayout-attempting-to-add-qlayout-to-mainwindow-which-already-has-a-layout-solved/9
6. https://stackoverflow.com/questions/4665606/rotate-image-in-qt
7. https://www.qtcentre.org/threads/53765-rotate-an-image
8. https://stackoverflow.com/questions/62191890/qt-qactiongroup-with-qaction-in-toolbar
9. https://stackoverflow.com/questions/33375829/qt-paintevent-not-triggered
10. https://stackoverflow.com/questions/30728820/refreshing-a-qwidget
11. https://stackoverflow.com/questions/64379202/how-to-rotate-an-qgraphicspixmapitem-in-qt
12. https://doc.qt.io/qt-6/implicit-sharing.html
13. https://www.walletfox.com/course/qtopenrecentfiles.php
14. https://forum.qt.io/topic/16660/solved-remove-items-from-layout/5
15. https://forum.qt.io/topic/94034/how-to-remove-children-from-a-widget
16. https://stackoverflow.com/questions/3939690/how-to-remove-widget-from-another-qwidget
17. https://www.qtcentre.org/threads/23174-How-do-you-add-a-QActionGroup-to-a-QMenuBar
18. https://forum.qt.io/topic/87345/qimage-rotation/9
19. https://stackoverflow.com/questions/34281682/how-to-convert-enum-to-qstring
20. https://stackoverflow.com/questions/28646914/qaction-with-custom-parameter
21. https://forum.qt.io/topic/6588/solved-qvariant-and-enums/10
