// All references see README.md
// Guidance from [7]
#include <QDebug>
#include <QPainter>

#include "paintingimagewidget.h"

PaintingImageWidget::PaintingImageWidget(QWidget *parent)
    : QWidget{parent}
{

}

// update() guidance from [9], [10]
// (don't need to do the resize as [9] gently suggests)
//inline    // Oddly, inline upsets linker here - though compile/linker could auto resolve and make a non-inline on demand in this context but maybe not
void PaintingImageWidget::setPixmap(QPixmap pixmap)
{
    reset();

    pixmap_ = pixmap;
    update();
}


void PaintingImageWidget::setImage(QImage image)
{
    reset();

    image_ = image;
    update();
}

void PaintingImageWidget::setRotation(int value)
{
    rotation_ = value;
    update();
}


// Scale r to fit into gui_rect dimensions, preserving aspect ratio
QRect scale(QRectF gui_rect, QRect r)
{
    float gui_aspect = gui_rect.width() / gui_rect.height();
    float img_aspect = (float)r.width() / r.height();

    if(gui_aspect > img_aspect) // If gui's aspect ratio is wider than img
    {
        // put horizontal margins in to keep img aspect
        float percent = gui_rect.height() / r.height();
        r.setWidth(r.width() * percent);
        r.setHeight(gui_rect.height());        // max out height
    }
    else
    {   // otherwise, gui's aspect ratio is narrower than img
        // put vertical margins in to keep img aspect
        float percent = gui_rect.width() / r.width();
        r.setWidth(gui_rect.width());   // max out width
        r.setHeight(r.height() * percent);
    }

    return r;
}


void PaintingImageWidget::paintEvent(QPaintEvent*)
{
    QPainter p(this);

    // [18] indicates this will smooth out rotations, but it doesn't look that way
    p.setRenderHint(QPainter::SmoothPixmapTransform);

    bool pixmapPresent = !pixmap_.isNull();
    bool qimagePresent = !image_.isNull();
    bool imagePresent = qimagePresent | pixmapPresent;

    // Grab in float precision so that aspect ratio math is easier down below
    QRectF gui_rect = rect();

    // Just for debug reference, nice to see a bounding box.  Seems we need to
    // do -1 perhaps due to layout?
    p.drawRect(gui_rect.x(), gui_rect.y(), gui_rect.width() - 1, gui_rect.height() - 1);

    if(imagePresent)
    {
        qDebug() << "Painting: " << rotation_;

        // Move coordinate system of canvas so that 0, 0 becomes width/2, height/2
        p.translate(width() / 2, height() / 2);

        QRect r = pixmapPresent ? pixmap_.rect() : image_.rect();

        // do aspect-safe scale to achieve best fit into visible area
        r = scale(gui_rect, r);

        // Changes dimensions of rectangle (negative x and y start values) so that 0, 0 is its center
        r.moveCenter(QPoint(0, 0));

        // Rotate the canvas on which we are drawing the pixmap
        p.rotate(rotation_);

        if(pixmapPresent)
            p.drawPixmap(r, pixmap_);
        else
            p.drawImage(r, image_);
    }
}


void PaintingImageWidget::reset()
{
    pixmap_.detach();
    image_.detach();

    pixmap_ = QPixmap();
    image_ = QImage();
}
