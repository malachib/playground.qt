# Overview

Trying to get aspect ratio preserved on added widgets

# References

1. https://doc.qt.io/qt-5/qtwidgets-layouts-flowlayout-example.html
2. https://stackoverflow.com/questions/452333/how-to-maintain-widgets-aspect-ratio-in-qt