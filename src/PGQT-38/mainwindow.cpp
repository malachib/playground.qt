#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QLabel>
#include <QPushButton>

#include "AspectGridLayout.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initLayout1(*ui->gridLayout);
    //initLayout2(*ui->gridLayout2);

    auto l = new AspectGridLayout(this);

    ui->verticalLayout->addLayout(l);

    initLayout2(*l);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initLayout1(QGridLayout& l)
{
    auto label = new QLabel(this);

    label->setText("layout 1");

    l.addWidget(label, 0, 0);

    for(int rows = 0; rows < 3; rows++)
        for(int columns = 0; columns < 4; columns++)
        {
            auto button = new QPushButton(this);
            QString title = QVariant(rows).toString() + ":" + QVariant(columns).toString();

            button->setText(title);

            //button->setMinimumSize(QSize(50, 100));
            //button->setMaximumSize(QSize(500, 1000));
            button->setBaseSize(QSize(100, 200));

            QSizePolicy policy;

            policy.setHorizontalPolicy(QSizePolicy::Policy::MinimumExpanding);
            policy.setVerticalPolicy(QSizePolicy::Policy::MinimumExpanding);

            button->setSizePolicy(policy);

            l.addWidget(button, rows + 1, columns);
        }
}


void MainWindow::initLayout2(QGridLayout& l)
{
    auto label = new QLabel(this);

    label->setText("layout 2");

    l.addWidget(label, 0, 0);

    l.setContentsMargins(QMargins(20, 0, 20, 0));

    QRect r = l.geometry();

    l.setHorizontalSpacing(20);
    l.setVerticalSpacing(0);

    for(int rows = 0; rows < 3; rows++)
        for(int columns = 0; columns < 4; columns++)
        {
            auto button = new QPushButton(this);
            QString title = QVariant(rows).toString() + ":" + QVariant(columns).toString();

            button->setText(title);

            //button->setMinimumSize(QSize(50, 100));
            button->setMaximumSize(QSize(500, 1000));
            button->setBaseSize(QSize(100, 200));

            QSizePolicy policy;

            policy.setHorizontalPolicy(QSizePolicy::Policy::Minimum);
            policy.setVerticalPolicy(QSizePolicy::Policy::Expanding);
            //policy.setHeightForWidth(true);   // Interestingly seems to affect 1st layout more

            button->setSizePolicy(policy);

            l.addWidget(button, rows + 1, columns);
        }

    QRect cellRect = l.cellRect(0, 0);
}

void MainWindow::exit()
{
    close();
}
