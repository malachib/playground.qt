// Refer to README.md for references
#pragma once

#include <QGridLayout>


// May need a fully customized layout as per [1]
class AspectGridLayout : public QGridLayout
{
    Q_OBJECT

public:
    explicit AspectGridLayout(QWidget *parent = nullptr) : QGridLayout(parent) {}

    // never called
    // https://doc.qt.io/qt-6/qlayoutitem.html#heightForWidth
    int heightForWidth(int w) const override
    {
        return w;
    }
};
