#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void initLayout1(QGridLayout& l);
    void initLayout2(QGridLayout& l);

public slots:
    // NOTE: Experimental, seeing of UI designer can see this.  Nope
    void exit();
};
#endif // MAINWINDOW_H
