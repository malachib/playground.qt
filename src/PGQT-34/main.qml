import QtQuick 2.15
import QtQuick.Window 2.15

import pgqt34 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ListModel {
        id: listModel
    }

    onActiveChanged: {
        // FIX: "Error: Unknown method parameter type: const QAbstractItemModel*" which is the
        // whole point of this PGQT-34
        Utility.test(listModel);
    }
}
