#pragma once

#include <QAbstractItemModel>

#include <QObject>

class Utility : public QObject
{
    Q_OBJECT

public:
    explicit Utility(QObject* parent = nullptr) : QObject(parent) {}

    Q_INVOKABLE void test(const QAbstractItemModel* model)
    {

    }
};
