import QtQuick 2.15
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.15

import "qrc:/qml/component"

// "no such directory"
//import "qrc:/qml/component-lib"

import PGQT_47_child as Child

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World: PGQT-47")

    ColumnLayout {
        Text {
            text: "Hello from PGQT-47"
            Layout.fillHeight: true
            Layout.minimumHeight: 25
        }

        Debug {
            Layout.fillHeight: true
            Layout.minimumHeight: 25
        }

        Child.MyItem {
            Layout.preferredHeight: 50
            Layout.preferredWidth: 50
        }
    }
}
