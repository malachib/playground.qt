#ifndef MYITEM_H
#define MYITEM_H

#include <QtQuick/QQuickPaintedItem>

// NOTE: Namespace is optional here.  Gets baked into QML_ELEMENT secretly somehow
namespace child {

class MyItem : public QQuickPaintedItem
{
    Q_OBJECT
    QML_ELEMENT
    Q_DISABLE_COPY(MyItem)
public:
    explicit MyItem(QQuickItem *parent = nullptr);
    void paint(QPainter *painter) override;
    ~MyItem() override;
};

}

#endif // MYITEM_H
