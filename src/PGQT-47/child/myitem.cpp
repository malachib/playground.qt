#include "myitem.h"

#include <QPainter>

namespace child {

MyItem::MyItem(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    // By default, QQuickItem does not draw anything. If you subclass
    // QQuickItem to create a visual item, you will need to uncomment the
    // following line and re-implement updatePaintNode()

    // setFlag(ItemHasContents, true);
}

void MyItem::paint(QPainter *painter)
{
    QPen pen(QColorConstants::Blue, 2);
    QBrush brush(pen.color());

    painter->setPen(pen);
    painter->setBrush(brush);
    painter->drawRect(0, 0, 100, 100);
}

MyItem::~MyItem() {}

}
