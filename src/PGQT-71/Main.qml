import QtQuick
import QtCharts
import QtQuick.Controls
import QtQuick.Layouts

import PGQT.App as App

Window {
    width: 1024
    height: 768
    visible: true
    title: qsTr("PGQT-71")

    GridLayout {
        anchors.fill: parent
        rows: 2
        columns: 2

        TabBar {
            id: tabBar
            Layout.fillWidth: true
            Layout.columnSpan: 2

            TabButton {
                text: "Line"
            }

            TabButton {
                text: "Line (model)"
            }

            TabButton {
                text: "AreaChart"
            }
        }

        StackLayout {
            Layout.fillWidth: true
            Layout.columnSpan: 2
            currentIndex: tabBar.currentIndex

            Rectangle {
                border.width: 2
                ChartView {
                    title: "Line Chart"
                    anchors.fill: parent
                    antialiasing: true

                    LineSeries {
                        name: "Line"
                        XYPoint { x: 0; y: 0 }
                        XYPoint { x: 1.1; y: 2.1 }
                        XYPoint { x: 1.9; y: 3.3 }
                        XYPoint { x: 2.1; y: 2.1 }
                        XYPoint { x: 2.9; y: 4.9 }
                        XYPoint { x: 3.4; y: 3.0 }
                        XYPoint { x: 4.1; y: 3.3 }
                    }
                }
            }

            Rectangle {
                // Guidance from [2.3]
                ChartView {
                    title: "Line Chart (model)"
                    anchors.fill: parent
                    antialiasing: true
                    //animationOptions: ChartView.AllAnimations

                    ValueAxis {
                        id: axisX
                        min: 0
                        max: 10000
                    }

                    ValueAxis {
                        id: axisY
                        min: 0
                        max: 100
                    }

                    LineSeries {
                        id: line
                        axisX: axisX
                        axisY: axisY
                    }

                    VXYModelMapper {
                        model: App.Session.lineModel
                        firstRow: 1
                        series: line
                        xColumn: 2
                        yColumn: 1
                    }

                    /*
                    HXYModelMapper {
                        model: App.Session.lineModel
                        firstColumn: 1
                        series: line
                        xRow: 1
                        yRow: 2
                    }
                    */
                }
            }

            Rectangle {
                ChartView {

                }
            }
        }

    }
}
