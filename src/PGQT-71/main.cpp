#include <QApplication>
#include <QQmlContext>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "session.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);   // See README 3.3 for why we don't use QGuiApplication

    QQmlApplicationEngine engine;

    qmlRegisterSingletonInstance("PGQT.App", 1, 0, "Session", new Session(engine.rootContext()));

    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection
    );
    engine.loadFromModule("PGQT-71", "Main");

    return app.exec();
}
