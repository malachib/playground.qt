#pragma once

#include <QObject>
#include <QStandardItemModel>

#include "datasource.h"

class Session : public QObject
{
    QStandardItemModel* lineModel_;
    DataSource* dataSource_;

    Q_OBJECT

    Q_PROPERTY(QStandardItemModel* lineModel MEMBER lineModel_ CONSTANT)
    Q_PROPERTY(DataSource* dataSource MEMBER dataSource_ CONSTANT)

protected:
    void timerEvent(QTimerEvent*) override;

public:
    Session(QObject* parent);
    ~Session();
};
