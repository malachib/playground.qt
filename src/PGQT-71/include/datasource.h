#pragma once

#include <QObject>

// Guidance from [1.4]

QT_FORWARD_DECLARE_CLASS(QAbstractSeries)
QT_FORWARD_DECLARE_CLASS(QQuickView)

class DataSource : public QObject
{
    Q_OBJECT

protected:
    QAbstractSeries* series_;

public:
    DataSource(QObject*);

public slots:
    void attach(QAbstractSeries*);
};
