# Overview

# 1. Design Goals

## 1.1. QML based

QML charts used, populated by synthetic QML data

## 1.2. c++ interaction

C++ feeds QML charts, in addition to 1.1. spec.  See [2]

# 2. RESERVED

# 3. Opinions & Observations

## 3.1. c++ -> QML mapping

Looks like QML charts expect a fully QML LineSeries [2], [2.1] so c++ interaction
is a bit awkward/nasty [2.1]

Stumbled on VXYModelMapper via [2.2] and now noticing a ton of ModelMappers [1].  OK so the reverse-link approach [2] is doable, but clearly the Qt people want
us to feed via a  QAbstractItemModel

A full example populating a line series with aforementioned [2.3] is very promising

Some curious discussions I have not fully considered are at [2.4] and [1.4].
It seems the canonical example [1.4] uses its own version of the reverse-link [2] approach

## 3.2. CMake / Build Settings

package is 'Charts' [1.2]

## 3.3. Crash on startup

QtCharts needs `QApplication` rather than the default `QGuiApplication` [3]
"As Qt Charts utilizes Qt Graphics View Framework for drawing, QApplication must be used" [1.3] - curious that doesn't appear on newer docs though

## 3.4. QStandardItem

As a feeder for ModelMappers, QStandardItemModel [2.3] looks useful

## 3.5. Animation and Performance

Unclear how to smoothly animate C++ -> QML series changes

# Terminology

| Term  | Context | Description
| ----- | --      | ---
| 

# References

1. https://doc.qt.io/qt-6.5/qtcharts-qmlmodule.html 
    1. https://doc.qt.io/qt-6.5/qtcharts-module.html 
    2. https://doc.qt.io/qt-6.5/qtcharts-index.html
    3. https://doc.qt.io/qt-5/qtcharts-qmlmodule.html
    4. https://doc.qt.io/qt-6.5/qtcharts-qmloscilloscope-example.html
2. https://stackoverflow.com/questions/51793635/how-to-create-chart-in-qml-with-series-data-exposed-from-c
    1. https://stackoverflow.com/questions/54459713/pass-c-lineseries-to-qml-charts 
    2. https://stackoverflow.com/questions/58840614/qml-chart-using-qabstracttablemodel-doesnt-dynamically-update
    3. https://stackoverflow.com/questions/48687705/how-to-use-the-vxymodelmapper-with-qstandarditemmodel 
    4. https://forum.qt.io/topic/85621/qml-lineseries-with-c-vector-data/7
3. https://stackoverflow.com/questions/73369059/chartview-crashing-in-qml
