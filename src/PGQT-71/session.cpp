#include <QDebug>
#include <QRandomGenerator>

#include "session.h"

Session::Session(QObject* parent) :
    QObject(parent),
    lineModel_{ new QStandardItemModel(100, 3, this) },
    dataSource_{ new DataSource(this) }
{
    qDebug() << "Session::Session";

    // [2.3]
    for(int row = 0; row < lineModel_->rowCount(); row++)
    {
        QStandardItem *item1 = new QStandardItem(QString::number(row));
        QStandardItem *item2 = new QStandardItem(QString::number((double)row));
        QStandardItem *item3 = new QStandardItem(QString::number(row*row));
        lineModel_->setItem(row, 0, item1);
        lineModel_->setItem(row, 1, item2);
        lineModel_->setItem(row, 2, item3);
    }

    startTimer(500);
}


void Session::timerEvent(QTimerEvent*)
{
    static QRandomGenerator r(1234);

    for(int row = 0; row < lineModel_->rowCount(); row++)
    {
        QStandardItem* item = lineModel_->item(row, 1);

        bool ok;

        auto data = item->data(Qt::DisplayRole);
        auto d = data.toDouble(&ok);

        d += r.generateDouble() - 0.5;

        item->setData(d, Qt::DisplayRole);
    }
}

Session::~Session()
{
    qDebug() << "Session::~Session";
}

