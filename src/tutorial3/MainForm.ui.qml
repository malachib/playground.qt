import QtQuick 2.6

Rectangle {
    id: page
    property alias mouseArea: mouseArea

    // this width and height are ignored for some reason
    // had to set this up in main.qml at the window/app level
    width: 480; height: 320
    color: "lightblue"

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Text {
        id: helloText
        anchors.centerIn: parent
        text: "Hello World"
        font.pointSize: 24; font.bold: true;

        MouseArea
        {
            id: mouseArea2; anchors.fill: parent
        }

        states: State {
            name: "down"; when: mouseArea2.pressed == true
            PropertyChanges { target: helloText; y: 160; rotation: 180; color: "red" }
        }

        transitions: Transition {
            from: ""; to: "down"; reversible: true
            ParallelAnimation {
                NumberAnimation { properties: "y,rotation"; duration: 500; easing.type: Easing.InOutQuad }
                ColorAnimation { duration: 500 }
            }
        }
    }

    Grid {
        id: colorPicker
        x: 4; anchors.bottom: page.bottom; anchors.bottomMargin: 4
        rows: 2; columns: 3; spacing: 3

        Cell { cellColor: "red"; onClicked: helloText.color = cellColor }
        Cell { cellColor: "green"; onClicked: helloText.color = cellColor }
        Cell { cellColor: "blue"; onClicked: helloText.color = cellColor }
        Cell { cellColor: "yellow"; onClicked: helloText.color = cellColor }
        Cell { cellColor: "steelblue"; onClicked: helloText.color = cellColor }
        Cell { cellColor: "black"; onClicked: helloText.color = cellColor }
    }
}
