import QtQuick 2.6
import QtQuick.Window 2.2

// http://doc.qt.io/qt-5/qml-tutorial3.html
Window {
    visible: true
    width: 480
    height: 320

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            Qt.quit();
        }
    }
}
