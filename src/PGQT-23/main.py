#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from PyQt5.QtCore import QDateTime, QObject, QUrl, pyqtSignal
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtQuick import QQuickView, QQuickItem
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QPainter, QColor, QPen
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine


app = QApplication(sys.argv)

# Create the QML user interface.  Auto creates its own engine
view = QQuickView()

view.setSource(QUrl('main.qml'))
#view.setResizeMode(QDeclarativeView.SizeRootObjectToView)
view.setGeometry(100, 100, 750, 480)
# ala https://pythonspot.com/pyqt5-colors/
view.setColor(QColor(0, 30, 0))

#list_view = view.findChild(QQuickItem, "list")

view.show()

app.exec_()
