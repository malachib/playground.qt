import QtQuick 2.6
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

Rectangle {
    id: root
    //width: 640
    //height: 480

    Item {
        // NOTE: Not sure why this results in a 0-width listview
        //anchors.fill: parent
        width: 640
        height: 480
        y: 100

        ListView {
            anchors.fill: parent

            clip: true
            model: 30
            id: list

            orientation: ListView.Horizontal
            layoutDirection: Qt.LeftToRight
            highlightMoveDuration: 500
            highlightMoveVelocity: 15

            delegate: Column {
                Icon {
                    width: 100
                    height: 100
                }

                Text {
                    width: 100
                    color: 'white'
                    text: index
                }
            }
        }

        /*
        ZoomBlur {
            scale: 1.3
            anchors.fill: list
            source: list
            id: zoom1
            length: 12
            samples: 12
        } */
    }



    Timer {
        interval: 500
        repeat: true
        running: true

        onTriggered: {
            if(list.currentIndex === list.count - 1)
            {
                console.log('got here')
                stop();
                var v = list.highlightMoveVelocity;
                list.highlightMoveVelocity = 150;
                list.currentIndex = 0
                list.highlightMoveVelocity = v;
                start();
            }
            else
                list.incrementCurrentIndex()
        }
    }
}
