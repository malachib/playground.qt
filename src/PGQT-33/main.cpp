#include <QGuiApplication>
#include <QQmlApplicationEngine>


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);


    /*
    engine.addImportPath("qrc:/");
    engine.addImportPath(":/");
    engine.addImportPath("qrc:/qml");
    engine.addImportPath(":/qml");
    engine.addImportPath("qrc:///"); */

    // NOTE: Unlike CMake's QML_IMPORT_PATH, these do not appear recursive.
    engine.addImportPath("qrc:///qml");
    engine.addImportPath("qrc:///");

    engine.load(url);

    return app.exec();
}
