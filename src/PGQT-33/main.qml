import QtQuick 2.13
import QtQuick.Layouts 1.12
import QtQuick.Window 2.13

import qml 1.0

//import TestModule 1.0
import nested 1.0     // Works to grab just Test2, but above qml import does this just fine

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ColumnLayout {

        Test1 {
            Layout.fillHeight: true
            Layout.preferredHeight: 100
        }

        Test2 {
            Layout.fillHeight: true
            Layout.preferredHeight: 100
        }

    }
}
