#include <QApplication>
#include <QQmlApplicationEngine>

#include <QAbstractButton>
#include <QQuickWindow>

#include <buttonresponder.h>

// references:
// http://andrew-jones.com/blog/qml2-to-c-and-back-again-with-signals-and-slots/

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QObject* topLevel = engine.rootObjects().value(0);
    QQuickWindow* window = qobject_cast<QQuickWindow*>(topLevel);

    ButtonResponder buttonResponder;

    QObject::connect(window, SIGNAL(onClick(QString)), &buttonResponder, SLOT(handleOnClick(QString)));
    QObject::connect(window, SIGNAL(onClickFile(QString)), &buttonResponder, SLOT(handleOnClickFile(QString)));

    QObject::connect(&buttonResponder, SIGNAL(setBackgroundColor()), window, SLOT(setBackgroundColor()));
    QObject::connect(&buttonResponder, SIGNAL(setTextFromFile(QVariant)), window, SLOT(setTextFromFile(QVariant)));

    return app.exec();
}
