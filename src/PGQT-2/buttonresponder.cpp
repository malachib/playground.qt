#include <iostream>
#include <fstream>
#include <sstream>

#include "buttonresponder.h"

ButtonResponder::ButtonResponder(QObject* parent) : QObject(parent)
{

}

void ButtonResponder::handleOnClick(const QString& in)
{
    qDebug() << "c++: HandleTextField::handleOnClick:" << in;
    emit setBackgroundColor();
}


void ButtonResponder::handleOnClickFile(const QString& in)
{
    std::ifstream infile("test.txt");

    if(infile)
    {
        std::ostringstream contents;

        contents << infile.rdbuf();
        infile.close();

        std::string contentsStr = contents.str();

        //QVariant* window = qobject_cast<QVariant*>();
        //QVariant variant(contents.str());
        QString qstr = QString::fromStdString(contentsStr);
        emit setTextFromFile(qstr);
    }
    else
    {
        emit setTextFromFile(QString("Dummy Text: file not loaded"));
    }

    qDebug() << "c++: HandleTextField::handleOnClick:" << in;
    //emit setBackgroundColor();
}
