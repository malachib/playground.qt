import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    // following is hand-coded & sourced from our own connection in main.cpp/buttonResponder
    // +++
    signal onClick(string text)
    signal onClickFile(string text)

    function setBackgroundColor()
    {
        console.log("setBackgroundColor");
        color = "red";
    }

    function setTextFromFile(text)
    {
        //messageDialog.show(text);
        fileDialog.open();
    }

    // ---

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    MainForm {
        anchors.fill: parent

        //button1.onClicked: messageDialog.show(qsTr("Button 1 pressed"))
        button1.onClicked: onClickFile(qsTr("File Button"))
        button2.onClicked: onClick(qsTr("Button 2 pressed"))
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = true
    }
}
