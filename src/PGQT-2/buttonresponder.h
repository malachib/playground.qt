#ifndef BUTTONRESPONDER_H
#define BUTTONRESPONDER_H

#include <QObject>
#include <QDebug>

class ButtonResponder : public QObject
{
    Q_OBJECT

public:
    ButtonResponder(QObject* parent = 0);

public slots:
    void handleOnClick(const QString& in);
    void handleOnClickFile(const QString& in);

signals:
    void setBackgroundColor();
    // a blog mentioned this needs to be QVariant, presumably because the QML is weakly typed
    // supporting that notion is if you make this a QString, it has trouble connecting the method
    void setTextFromFile(const QVariant text);
};

#endif // BUTTONRESPONDER_H
