# Overview

## Qt5

Not yet online.  As one might deduce, qt5.qrc is not used for qt6

## DEBT

QML/moc can't seem to pick up SOURCES from a subfolder, so termporarily I have to
symlink (yuck)

# References

1. https://stackoverflow.com/questions/32969414/populate-gridlayout-with-repeater
2. https://doc.qt.io/qt-6/qtquick-modelviewsdata-cppmodels.html
3. https://stackoverflow.com/questions/20691414/qt-qml-send-qimage-from-c-to-qml-and-display-the-qimage-on-gui
4. https://doc.qt.io/qt-6/qquickimageprovider.html
