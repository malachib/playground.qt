// See README.md for references
#pragma once

#include <QObject>
#include <QQuickImageProvider>

#include "cardmodel.h"
#include <games/matchcontroller.h>

class BoardView : public QObject
{
    Q_OBJECT

protected:
    typedef const cards::qml::LocationModel* location_type;

public:
    BoardView(QObject* parent = nullptr) : QObject(parent) {}

    Q_INVOKABLE virtual void click(location_type location) = 0;
};


class BoardView2 : public BoardView
{
    Q_OBJECT

public:
    BoardView2(QObject* parent = nullptr) : BoardView(parent) {}

    void click(location_type location) override
    {
        emit clicked(*location);
    }

signals:
    void clicked(cards::Card::Location);
};


class MatchView : public BoardView
{
    Q_OBJECT

    games::MatchController& controller;

public:
    MatchView(games::MatchController& controller, QObject* parent = nullptr) :
        BoardView(parent),
        controller{controller}
    {}

    void click(location_type location) override;
};


// NOTE: References from [3] and [4] but we want to pass in Card object iself to resolve image, not a URL.
// Doable, but connvering Card to URL is clunky
class CardImageProvider : public QQuickImageProvider
{
public:
    CardImageProvider() : QQuickImageProvider(QQuickImageProvider::Pixmap) {}

    QPixmap requestPixmap(const QString& id, QSize* size, const QSize& requestedSize) override;
};
