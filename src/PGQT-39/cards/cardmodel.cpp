#include "cardmodel.h"
#include "cards_pack.h"

#include <QModelIndex>

namespace cards { namespace qml {

CardModel::CardModel(Card card, QObject* parent) :
    QObject(parent),
    card(card)
{

}

void CardsModel::initConnections()
{
    connect(appState.get(), &cards::AppState::forceCardsUpChanged, this, &CardsModel::updateAllCards);
}

void CardsModel::updateAllCards()
{
    QModelIndex start = createIndex(0, 0);
    QModelIndex end = createIndex(model.card_count() - 1, 0);
    emit dataChanged(start, end);
}


void CardsModel::updateCard(Card::Location location)
{
    QModelIndex index = createIndex(location.position, 0);
    emit dataChanged(index, index);
}


QHash<int, QByteArray> CardsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[SuitRole] = "suit";
    roles[RankRole] = "rank";
    roles[ShowingRole] = "showing";
    roles[BentRole] = "bent";
    roles[LocationRole] = "location";
    roles[ImageUrlRole] = "url";

    return roles;
}


QVariant CardsModel::data(const QModelIndex& index, int role) const
{
    const cards::Card& card = getCard(index);

    switch(role)
    {
        case SuitRole: return (int)card.identity.suit;
        case RankRole: return card.identity.rank;
        case ShowingRole: return card.state.showing | appState->forceCardsUp();
        case BentRole: return card.traits.bent;
        case LocationRole:
        {
            Card::Location location{0, (uint16_t)index.row()};
            return QVariant::fromValue(new LocationModel(location));
        }
        case ImageUrlRole:
        {
            return appState->cardsPack.name(card, appState->forceCardsUp());
        }
        default: return QVariant();
    }
}


bool CardsModel::setData(const QModelIndex& index, const QVariant& v, int role)
{
    switch(role)
    {
        case ShowingRole:
            model.set_showing(index.row(), v.toBool());
            // DEBT: Somehow explicitly specifying just the ShowingRole isn't enough
            //emit dataChanged(index, index, QList<int>(ShowingRole));
            emit dataChanged(index, index);
            return true;
    }

    return false;
}


int CardsModel::rowCount(const QModelIndex&) const
{
    return model.card_count();
}

// DEBT: Existing controllers would be better for this
void CardsModel::shuffle()
{
    // WORKS!  Just nothing is wired up to respond once this is clicked
    model.shuffle();

    updateAllCards();
}

void CardsModelPlugin::registerTypes(const char* uri)
{
    qmlRegisterType<CardsModel>(uri, 1, 0, "CardsModel");
}



}}
