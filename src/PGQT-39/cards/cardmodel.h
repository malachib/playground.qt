// See README.md for references
#pragma once

#include <QAbstractListModel>
#include <QObject>
#include <QQmlExtensionPlugin>
#include <qqml.h>

#include <cards/appstate.h>
#include <cards/deck.h>
#include <cards/cardsmodel.h>

namespace cards { namespace qml {

class LocationModel : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(int position READ position CONSTANT)

private:
    Card::Location location;

public:
    explicit LocationModel(Card::Location location, QObject* parent = nullptr) :
        QObject(parent),
        location{location}
    {}

    int id() const { return location.id; }
    int position() const { return location.position; }

    operator const Card::Location& () const { return location; }
};

class CardModel : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int suit READ suit NOTIFY suitChanged)
    Q_PROPERTY(int rank READ suit NOTIFY rankChanged)

private:
    cards::Card card;

public:
    // DEBT: Don't like an uninitialized card, but may be OK in the short term
    explicit CardModel(QObject *parent = nullptr) : QObject(parent),
        card{cards::Card::Identity{Suits::Club, 0}, cards::Card::Traits{false, 0}}
    {}
    explicit CardModel(Card card, QObject *parent = nullptr);

    int suit() const { return (int)card.identity.suit; }

signals:
    void suitChanged();
    void rankChanged();
};



// Guidance from [2]
class CardsModel : public QAbstractListModel
{
    Q_OBJECT

    std::shared_ptr<cards::CardsModel> _model;
    std::shared_ptr<cards::AppState> appState;
    cards::CardsModel& model;

private:
    inline const Card& getCard(const QModelIndex& index) const
    {
        return model.at(index.row());
    }

    void initConnections();

public:
    explicit CardsModel(QObject* parent = nullptr) :
        QAbstractListModel(parent),
        _model(new cards::CardsModel(cards::CardsModel::Notifier::singleton)),
        appState(new cards::AppState),
        model{*_model}
    {
        initConnections();
    }

    explicit CardsModel(std::shared_ptr<cards::AppState> appState, cards::CardsModel& model, QObject* parent = nullptr) :
        QAbstractListModel(parent),
        appState(appState),
        model{model}
    {
        initConnections();
    }

    enum CardRoles
    {
        SuitRole = Qt::UserRole + 1,
        RankRole,
        ShowingRole,
        BentRole,
        LocationRole,

        // EXPERIMENTAL
        ImageUrlRole,
    };

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, int role) const override;
    bool setData(const QModelIndex& index, const QVariant& v, int role) override;
    int rowCount(const QModelIndex& parent) const override;

public slots:
    // These two slots are for external parties to indicate underlying CardsModel has changed
    // DEBT: Naming seems problematic
    void updateAllCards();
    void updateCard(Card::Location);

public slots:
    void shuffle();
};

// Makes model QML accessible [2] 'exposting-c-data-models-to-qml'
// NOTE: Doesn't work yet
class CardsModelPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-playground.pgqt-39.CardsModel")

public:
    explicit CardsModelPlugin(QObject* parent = nullptr) : QQmlExtensionPlugin(parent) {}

    void registerTypes(const char* uri) override;
};


}}
