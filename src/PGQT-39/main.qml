// All references refer to README.md
import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import QtQml.StateMachine 1.0 as DSM

import PGQT_39 1.0

Window {
    id: mainWindow
    width: 640
    height: 480
    visible: true
    title: qsTr("PGQT-39: Card Game")

    ColumnLayout {

        anchors.fill: parent

        BasicBoard {
            id: basicBoard
            view: BoardView
            //model: CardsModel
            redMode: GameController.state === 4

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.height

            iconShowing: iconShowing.checked
        }

        RowLayout {
            Button {
                text: "Shuffle"
                //onClicked: basicBoard.model.shuffle() // DEBT: This is broken, fix it.  Lingering Q if we want model or controller to be authority here
                onClicked: GameController.shuffle()
            }

            Button {
                text: "Reset"
                onClicked: GameController.sort()
            }

            Button {
                text: "Show All"
                onClicked: AppState.forceCardsUp = !AppState.forceCardsUp
            }

            CheckBox {
                id: iconShowing
                text: "Icon Showing"
            }

            Label {
                text: "State: " + GameController.state
            }
        }
    }

    /*
     * EXPERIMENTAL - we control the transitions ourself, so not sure if this is useful
    DSM.StateMachine
    {
        initialState: idleState
        running: true

        DSM.State
        {
            id: idleState
            DSM.SignalTransition
            {
                signal: GameController.stateChanged
                guard: GameController.state() === 0
            }

            onEntered: console.log("idle state entered")
        }

        DSM.State
        {
            id: firstGuessState
            DSM.SignalTransition
            {
                signal: GameController.stateChanged
                guard: GameController.state() === 1
            }

            onEntered: console.log("first guess state entered")
        }
    } */
}
