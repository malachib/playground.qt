#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <memory>

#include "cards/cardmodel.h"
#include <cards/container.h>
#include <games/matchcontroller.h>

#include <cards_pack_view.h>

#include "boardview.h"

#include <QIcon>

int main(int argc, char *argv[])
{
    //static cards::Card debugCard(cards::Card::Identity{cards::Suits::Club, 0}, cards::Card::Traits{false, 0});

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    std::shared_ptr<cards::AppState> appState(new cards::AppState);
    //cards::CardsModel _model(false);

    auto modelNotifier = std::make_shared<cards::QtModelNotifier>();
    games::MatchModel _model(modelNotifier);
    cards::QtGameModelNotifier gameNotifier;
    cards::GameModel gameModel(gameNotifier);

    games::initMatch(gameModel, _model);

    std::shared_ptr<games::MatchController> controller(new games::MatchController(*appState, _model));
    //MatchView boardView(*controller);
    BoardView2 boardView;
    QObject::connect(&boardView, &BoardView2::clicked, controller.get(), &games::MatchController::guess);
    auto model = new cards::qml::CardsModel(appState, _model);

    QObject::connect(modelNotifier.get(), &cards::QtModelNotifier::allUpdated, model, &cards::qml::CardsModel::updateAllCards);
    QObject::connect(&gameNotifier, &cards::QtGameModelNotifier::cardStateUpdated, model, &cards::qml::CardsModel::updateCard);

    // NOTE: Actually this loads.  So QRC loading from QML may be pilot error
    //QIcon icon = appState->cardsPack.icon(debugCard);

    // NOTE: Makes a difference that we do this here before engine.load
    qmlRegisterSingletonInstance("PGQT_39", 1, 0, "CardsModel", model);
    qmlRegisterSingletonInstance("PGQT_39", 1, 0, "AppState", appState.get());
    qmlRegisterSingletonInstance("PGQT_39", 1, 0, "GameController", controller.get());
    qmlRegisterSingletonInstance("PGQT_39", 1, 0, "BoardView", &boardView);
    qmlRegisterType<BoardView>("PGQT_39", 1, 0, "BoardView_");

#if QT_VERSION_MAJOR >= 6
    const QUrl url(u"qrc:/PGQT_39/main.qml"_qs);
#else
    const QUrl url("qrc:/PGQT_39/main.qml");
#endif
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
