import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import PGQT_39 1.0

//Item {
    Button {
        id: cardButton

        background: Rectangle {
            border.width: 1
            opacity: 0.05
        }

        property bool iconVisible : false;
        property bool textVisible : true;

        Layout.fillWidth: true
        Layout.fillHeight: true

        //Layout.minimumWidth: cardImage.width / 4
        //Layout.minimumHeight: cardImage.height / 4
        //Layout.maximumWidth: 100
        //Layout.maximumHeight: 200

        Layout.preferredWidth: cardImageRef.width
        Layout.preferredHeight: cardImageRef.height
        Layout.maximumWidth: cardImageRef.width
        Layout.maximumHeight: cardImageRef.height
        //Layout.preferredWidth: 103
        //Layout.preferredHeight: 150

        property string textIdentity: model.suit + ":" + model.rank
        property string title: (model.showing ? textIdentity : "down") + (model.bent ? "'" : "")
        property Image cardImage: cardImage

        text: textVisible ? title : null

        // NOTE: icon.source never seemed to work, but Image does
        //icon.source: "qrc://PGQT_35/png/large/back_red_1.png"
        //icon.source: "/png/large/back_red_1.png"
        //icon.source: "qrc://png/large/back_red_1.png"

        Image {
            id: cardImage
            source: "/png/large/" + model.url
            //anchors.left: parent.left
            //anchors.bottom: parent.bottom
            // FIX: This technique *almost* works, but image size fluctuates within button :(
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            //Layout.maximumWidth: cardButton.width
            //Layout.maximumHeight: cardButton.height
            opacity: 0.6

            // NOTE: Even with a 100% fixed height, we still get the verticle height flutter
            //width: cardButton.width
            //height: 150
            visible: iconVisible
        }

        Image {
            id: cardImageRef
            source: "/png/large/" + model.url
            //anchors.left: parent.left
            //anchors.bottom: parent.bottom
            // FIX: This technique *almost* works, but image size fluctuates within button :(
            //anchors.fill: parent
            //fillMode: Image.PreserveAspectFit
            visible: false
        }
    }
//}
