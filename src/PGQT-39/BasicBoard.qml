// All references refer to README.md
import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import PGQT_39 1.0;

Rectangle {
    id: rect

    property BoardView_ view
    property bool redMode

    property bool iconShowing

    GridLayout {
        id: simpleBoard
        columns: 13
        rows: 4

        anchors.fill: parent

        // DEBT: Following line does nothing
        //property CardsModel model: repeater.model

        // Guidance from [1]
        Repeater {
            id: repeater

            model: CardsModel

            //anchors.fill: parent
            //Layout.alignment: Lay

            Card {
                // TODO: feed BoardView into Card and have onClicked handled inside there
                onClicked: view.click(model.location);

                iconVisible: rect.iconShowing
                textVisible: !rect.iconShowing
            }
        }
    }

    Rectangle {
        id: backColor

        anchors.fill: parent

        /*
        Layout.fillHeight: true

        Layout.columnSpan: 13
        Layout.rowSpan: 4
        Layout.column: 0
        Layout.row: 0 */

        color: "red"
        opacity: redMode ? 0.4 : 0.1
    }

}
