#include "boardview.h"

#include "cards_pack.h"

#include <QDebug>

void MatchView::click(location_type location)
{
    qDebug() << "MatchView:: click location=" << location->position();
    controller.guess(*location);
}

/*
QPixmap CardImageProvider::requestPixmap(const QString& id, QSize* size, const QSize& requestedSize)
{
    //CardsPackViewModel{}.icon()
}
*/
