#pragma once

#include <QGraphicsScene>
#include <QImage>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QSlider>

#include <pgqt/mru.h>

#include "worker.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    enum States
    {
        Original,
        Overlay,
        Loaded
    };

    States state_ = Original;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    //Ui::MainWindow *ui;
    QGraphicsScene graphicsScene_;
    pgqt::v1::MruManager mru_;

    QSlider contrastSlider_;
    QSlider brightnessSlider_;

    ImageProcessWorker worker_;

    // https://stackoverflow.com/questions/10307860/what-is-the-difference-between-qimage-and-qpixmap
    // so we'll hold on to this image to process and modify, and when we do so, we'll update the pixmap
    // in the graphics scene
    QImage image_;
    QImage processedImage_;

    // This is kept updated when image_ changes
    QGraphicsPixmapItem pixmapItem_;

    void createLayout();
    void createMenus();
    void connectSliders();


    void mruItemSelected(const pgqt::v1::MruManagerItem&);
    void loadImage(QString filename);

    void updateStatusbar();
    void setMode(States m);

protected slots:
    void load();
    void save();
    void saveAs();
    void updatePixmap(const QPixmap&);
    void updateImage(const QImage&);
};
