# Parity Application: Grant S Image Processing

Using Qt 5.11 specifically as that is implied as a requirement in his assigment

## Steps [follows Lab_Assignment 1.pdf]

Using manual algorithms for overlay, brightness, contrast

0. Prerequisites
    1. BMP 8-bit images always employed
    2. Image size minimum supported 1024x767
1. Task 1
    1. Get basic UI online
        1. Load/display 8-bit original (image 1)
        2. Load/display 8-bit overlay (image 2)
        3. Render overlay of above 2
2. Task 2
    1. Placeholder
        1. Do 1.1.1
        2. Adjust brightness of 2.1.1.
        3. Adjust contrast of 2.1.1.
        4. Both 2.1.2 and 2.1.3 together
        5. Save #4 to 8-bit bitmap
        
## Todo

### 1. Account for combining brightness and contrast

## Design Decisions

### QImage -> QPixmap

I was really hoping to avoid copying QImage to a QPixmap every time.
However, all the examples I find render right on the QImage, except for
rendering into a raw array but STILL copies it into pixmap 
via `QPixmap::loadFromData` [^2]

Even Qt themselves seem to recommend this[^1][^3]

It may even be hazardous to touch a QPixmap outside the GUI thread[^6]

Since `QPixmap` pixel data is internal, one must use a `QPainter` to practically
manipulate it[^7].  Since we're doing manual pixel processing as necessitated
by the assignment, we are sticking with `QImage`

Note also "If you want to update the pixmap [...], you need to call the `setPixmap()`[^7]

"QImage is designed and optimized for I/O, and for direct pixel access
and manipulation, while QPixmap is designed and optimized for showing images on screen"[^8]

### Signal compression / only last signal processed

Lots of exploration of this[^4] and similar techniques[^5] which are compelling,
but involve a lot of legwork AND looks like using Qt internals.

Being that is the case, I will manually clue in a worker QObject
how many signa/slot requests it is received, and noop my way until
they are all processed.  Not quite as elegant, but one step short
of kludgey.

# References

[^1]: https://doc.qt.io/qt-5/qtcore-threads-mandelbrot-example.html
[^2]: https://www.qtcentre.org/threads/13765-Image-processing-via-matrix
[^3]: https://forum.qt.io/topic/64426/how-to-efficiently-draw-a-qimage-to-a-window/18
[^4]: https://stackoverflow.com/questions/20866996/how-to-compress-slot-calls-when-using-queued-connection-in-qt
[^5]: https://stackoverflow.com/questions/44440584/how-to-monitor-qt-signal-event-queue-depth
[^6]: https://forum.qt.io/topic/52397/not-safe-to-use-qpixmap-outside-the-gui-thread
[^7]: https://stackoverflow.com/questions/17888795/how-to-use-qpainter-on-qpixmap
[^8]: https://doc.qt.io/qt-6/qimage.html
[^9]: https://stackoverflow.com/questions/35968431/qimage-padding-any-function-availavble

