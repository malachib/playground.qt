#pragma once

#include <pgqt/pipeline.hpp>

#include "pipeline.h"


template <class F>
void processImagePipeline(const QImage& source, QImage& dest, F&& perPixel)
{
    for(int y = 0; y < source.height(); ++y)
    {
        auto source_line = reinterpret_cast<const QRgb*>(source.scanLine(y));
        auto line = reinterpret_cast<QRgb*>(dest.scanLine(y));

        for(int x = 0; x < source.width(); ++x)
        {
            const QRgb& rgb = source_line[x];
            QRgb& dest_rgb = line[x];
            perPixel(rgb, dest_rgb);
        }
    }
}


template <class F>
void processImage(const QImage& source, QImage& dest, F&& perPixel)
{
    for(int y = 0; y < source.height(); ++y)
    {
        auto source_line = reinterpret_cast<const QRgb*>(source.scanLine(y));
        auto line = reinterpret_cast<QRgb*>(dest.scanLine(y));

        for(int x = 0; x < source.width(); ++x)
        {
            const QRgb& rgb = source_line[x];
            line[x] = perPixel(rgb);
        }
    }
}


template <class F, class ... Args>
void processImageRgbChannel(const QImage& source, QImage& dest, F&& perChannel, Args&&...args)
{
    return processImage(source, dest, [&](const QRgb& rgb)
    {
        unsigned r = perChannel(qRed(rgb), std::forward<Args>(args)...);
        unsigned g = perChannel(qGreen(rgb), std::forward<Args>(args)...);
        unsigned b = perChannel(qBlue(rgb), std::forward<Args>(args)...);
        return qRgb(r, g, b);
    });
}


template <class F>
QImage processImage(const QImage& source, F&& perPixel)
{
    QImage dest(source.size(), source.format());

    processImage(source, dest, std::forward<F>(perPixel));

    return dest;
}



// Lifted from
// https://stackoverflow.com/questions/4981474/brightness-contrast-gamma-adjustments-with-c-qt
inline int changeBrightness( int value, int brightness) {
    return qBound<int>(0, value + brightness * 255 / 100, 255);
}
inline int changeContrast( int value, int contrast ) {
    return qBound<int>(0, int(( value - 127 ) * contrast / 100 ) + 127, 255 );
}
/*
inline int changeGamma( int value, int gamma ) {
    return qBound<int>(0, int( pow( value / 255.0, 100.0 / gamma ) * 255 ), 255 );
}
*/

inline int averagePixels(int lhs, int rhs)
{
    return (lhs + rhs) / 2;
}


