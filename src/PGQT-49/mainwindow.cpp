#include <QActionGroup>
#include <QGuiApplication>
#include <QDebug>
#include <QFileDialog>
#include <QGraphicsView>
#include <QGridLayout>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QScreen>
#include <QSlider>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    //ui(new Ui::MainWindow),
    // DEBT: Double check if we want to pass in 'this', don't think so though
    contrastSlider_(Qt::Horizontal),
    brightnessSlider_(Qt::Horizontal)
{
    // PGQT-40 guidaes us how to do main window widgets manually
    //ui->setupUi(this);

    QGuiApplication::setOrganizationName("malachib");

    createMenus();

    // Center + resize window, otherwise it's tiny and usually appears
    // in the least convenient spot on screen
    QScreen* screen = QGuiApplication::screens().at(0);
    QRect geometry = screen->geometry();

    resize(geometry.size() * 3 / 4);
    move(geometry.center() - frameGeometry().center());

    createLayout();

    connectSliders();

    graphicsScene_.addItem(&pixmapItem_);

    connect(&worker_, &ImageProcessWorker::imageUpdated, this, &MainWindow::updateImage);

    connect(&mru_, &pgqt::v1::MruManager::triggered, this, &MainWindow::mruItemSelected);
}


void MainWindow::updateStatusbar()
{
    // FIX: Need to grab label back out of statusbar and use that since 'showMessage' is only temporary
    auto label = dynamic_cast<QLabel*>(statusBar()->children().first());

    switch(state_)
    {
        case Original:
            statusBar()->showMessage("Load Mode: Original");
            break;

        case Overlay:
            statusBar()->showMessage("Load Mode: Overlay");
            break;

        case Loaded:
            statusBar()->showMessage("Running");
            break;
    }


}

void MainWindow::setMode(States m)
{
    state_ = m;
    updateStatusbar();
}


void MainWindow::mruItemSelected(const pgqt::v1::MruManagerItem& mmi)
{
    loadImage(mmi.filename());
}


void MainWindow::createLayout()
{
    // NOTE: No 'this' here, otherwise it tries to displace
    // inbuilt QMainWindow layout (which theoretically is desireable,
    // but then we should use just a QWidget then, not a QMainWindow)
    auto l = new QGridLayout();

    auto button1 = new QPushButton("TEST");

    connect(button1, &QPushButton::clicked, this, [=]
    {
        mru_.clear();
        mru_.refresh();
    });

    auto graphicsView = new QGraphicsView(&graphicsScene_, this);

    l->addWidget(new QLabel("contrast"), 0, 0);
    l->addWidget(&contrastSlider_, 0, 1);
    l->addWidget(new QLabel("brightness"), 1, 0);
    l->addWidget(&brightnessSlider_, 1, 1);

    l->addWidget(button1, 3, 0);
    l->addWidget(graphicsView, 2, 0, 1, 2);

    graphicsView->show();

    // Dummy widget for QMainWindow to use - normally autogen UI goes
    // here.
    setCentralWidget(new QWidget(this));

    // As per
    // https://stackoverflow.com/questions/73556963/change-qmainwindow-layout-programmatically-in-c
    centralWidget()->setLayout(l);

    statusBar()->addWidget(new QLabel);

    updateStatusbar();
}


void MainWindow::createMenus()
{
    //QMenuBar* menuBar = new QMenuBar();
    QMenuBar* menuBar = this->menuBar();

    QAction* action;

    auto menu = new QMenu("&File");

    menuBar->addMenu(menu);

    action = menu->addAction("&Open", this, &MainWindow::load);

    action->setShortcut(QKeySequence("Ctrl+o"));

    action = menu->addAction("&Save", this, &MainWindow::save);

    action->setShortcut(QKeySequence("Ctrl+s"));

    action = menu->addAction("Save &as...", this, &MainWindow::saveAs);

    menu->addSeparator();

    action = menu->addAction("&Reset");

    menu->addSeparator();

    // If we have any history of opening files, separate that out
    if(mru_.create(menu) > 0)   menu->addSeparator();

    // DEBT: This wants a context, but I'm 50% sure for a global slot we don't need one
    menu->addAction("E&xit", &QCoreApplication::quit);

    //layout()->setMenuBar(menuBar);

    menu = new QMenu("&Technique");
    auto group = new QActionGroup(this);

    menuBar->addMenu(menu);

    action = menu->addAction("&1 - worker mode", [=]{ worker_.mode_ = ImageProcessWorker::Modes::Worker; });
    action->setCheckable(true);
    action->setChecked(worker_.mode_ == ImageProcessWorker::Modes::Worker);
    group->addAction(action);
    action = menu->addAction("&2 - pipeline mode",  [=]{ worker_.mode_ = ImageProcessWorker::Modes::Pipeline; });
    action->setCheckable(true);
    action->setChecked(worker_.mode_ == ImageProcessWorker::Modes::Pipeline);
    group->addAction(action);
}


void MainWindow::connectSliders()
{
    // Don't use queue to hop thread boundaries, we specifically want to indicate a call counter right away on UI thread
    connect(&brightnessSlider_, &QSlider::valueChanged, &worker_, &ImageProcessWorker::signalCompressor, Qt::DirectConnection);
    connect(&contrastSlider_, &QSlider::valueChanged, &worker_, &ImageProcessWorker::signalCompressor, Qt::DirectConnection);

    connect(&brightnessSlider_, &QSlider::valueChanged, &worker_, &ImageProcessWorker::adjustBrightness);
    connect(&contrastSlider_, &QSlider::valueChanged, &worker_, &ImageProcessWorker::adjustContrast);
}


void MainWindow::loadImage(QString filename)
{
    image_.load(filename);

    if(image_.isNull()) return;

    mru_.update(filename);

    // Image seems to really load, but it's not rendering yet on the scene
    //QPixmap pixmap = QPixmap::fromImage(image_);

    //pixmapItem_.setPixmap(pixmap);
    //graphicsScene_.update();

    if(state_ == Original)
    {
        worker_.setImage(image_);
        setMode(Overlay);
    }
    else
    {
        worker_.setOverlayImage(image_);
        setMode(Loaded);
    }
}


void MainWindow::load()
{
    if(!(state_ == Original || state_ == Overlay))
    {
        // TODO: Display popup warning here

        return;
    }

    // DEBT: We see QXcbConnection: XCB error: 3 (BadWindow) - there was a way to fix this before, look around
    QString file = QFileDialog::getOpenFileName(this, tr("Open Image"), {}, "Image Files (*.png *.jpg *.gif *.bmp)");

    if(file.isEmpty()) return;

    loadImage(file);
}


void MainWindow::save()
{
    QString filename = "TEST.bmp";

    bool result = processedImage_.save(filename, "BMP");

    qDebug() << "result=" << result;
}


void MainWindow::saveAs()
{
    // DEBT: Doesn't auto append .bmp suffix
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Open Image"),
        {}, "Image File (*.bmp)");

    bool result = processedImage_.save(fileName, "BMP");

    qDebug() << "result=" << result;
}


void MainWindow::updatePixmap(const QPixmap& pixmap)
{
    //auto value = pixmap.depth();
    //auto p = (unsigned long)QThread::currentThread();
    //qDebug() << "updatePixmap=" << value << "p=" << hex << p;

    pixmapItem_.setPixmap(pixmap);
}


void MainWindow::updateImage(const QImage& image)
{
    updatePixmap(QPixmap::fromImage(image));

    // DEBT: I think this is a shallow copy of sorts (shared object?) - double check
    // and make sure it's not a deep copy, and document
    processedImage_ = image;
}

MainWindow::~MainWindow()
{
    //delete ui;
}

