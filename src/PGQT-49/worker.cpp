#include <QDebug>
#include <QPainter>
#include <QPixmap>
#include <QRgb>

#include "worker.h"
#include "pipeline.hpp"


ImageProcessWorker::ImageProcessWorker(QObject* parent) :
    // As per https://doc.qt.io/qt-6/qobject.html#moveToThread
    // "The object cannot be moved if it has a parent."
    QObject(nullptr),
    worker_(parent)
{
    moveToThread(&worker_);

    overlayAction_ = new OverlayAction;
    brightnessAction_ = new BrightnessAction;
    contrastAction_ = new ContrastAction;

    pipeline_.pipeline_.push_front(std::unique_ptr<ImagePipelineAction>{overlayAction_});
    pipeline_.pipeline_.push_front(std::unique_ptr<ImagePipelineAction>{brightnessAction_});
    pipeline_.pipeline_.push_front(std::unique_ptr<ImagePipelineAction>{contrastAction_});

    // NOTE: Good to know, if we don't start this, the adjustXYZ slots are never invoked.  That's proof
    // we're doing things on another thread
    worker_.start();
}


ImageProcessWorker::~ImageProcessWorker()
{
    worker_.quit();
    worker_.wait(500);
}


void ImageProcessWorker::signalCompressor()
{
    anyRequests.release();

    //auto p = (unsigned long)QThread::currentThread();
    //qDebug() << "signalCompressor " << anyRequests.available() << "p=" << hex << p;
}



void ImageProcessWorker::adjustBrightness(int value)
{
    auto p = (unsigned long)QThread::currentThread();

    //qDebug() << "r1" << anyRequests.available() <<  "adjustBrightness=" << value << "p=" << hex << p;

    anyRequests.acquire();

    if(anyRequests.available() > 1) return;

    //qDebug() << "r2" << anyRequests.available() <<  "adjustBrightness=" << value << "p=" << hex << p;

    // DEBT: Put a checkbox or similar on here to adjust artificial delay portion
    // DEBT: Put a profiler in to see how long our crude processor really takes
    QThread::msleep(100);

    if(mode_ == Modes::Pipeline)
    {
        brightnessAction_->value(value);
        processPipeline();
    }
    else
    {
        processImage(image_, processedImage_, [value](const QRgb& rgb)
        {
            unsigned r = changeBrightness(qRed(rgb), value);
            unsigned g = changeBrightness(qGreen(rgb), value);
            unsigned b = changeBrightness(qBlue(rgb), value);
            return qRgb(r, g, b);
        });
    }

    emit processed(processedImage_);
    emit imageUpdated(processedImage_);
}

void ImageProcessWorker::adjustContrast(int value)
{
    anyRequests.acquire();

    if(anyRequests.available() > 1) return;

    auto p = (unsigned long)QThread::currentThread();

    //qDebug() << "adjustContrast=" << value << "p=" << hex << p;

    QThread::msleep(100);

    if(mode_ == Modes::Pipeline)
    {
        contrastAction_->value(value);
        processPipeline();
    }
    else
    {
        processImage(image_, processedImage_, [value](const QRgb& rgb)
        {
            unsigned r = qRed(rgb);
            unsigned g = qGreen(rgb);
            unsigned b = qBlue(rgb);
            r = changeContrast(r, value);
            g = changeContrast(g, value);
            b = changeContrast(b, value);
            return QColor(r, g, b).rgb();
        });
    }

    emit processed(processedImage_);
    emit imageUpdated(processedImage_);
}



void ImageProcessWorker::setImage(const QImage& image)
{
    // DEBT: Not worrying about data races for now, specifically I am not going to worry
    // if the worker is still processing an old image when we put in a new one - we can live
    // with that artifact

    image_ = image;
    processedImage_ = QImage(image.size(), image.format());

    // Image not processed yet, so emit what we just got
    emit imageUpdated(image_);
}


void ImageProcessWorker::setOverlayImage(const QImage& image)
{
    overlayAction_->image(image);

    QSize expandedTo = image_.size().expandedTo(image.size());

    // As per [9]
    // DEBT: Do this in process area itself, possibly as part of pipeline -
    // because modifying image means that if we change overlay again,
    // we get undefined behavior
    if(expandedTo != image.size() ||
        expandedTo != image_.size())
    {
        QImage padded(expandedTo, image_.format());
        QPainter p(&padded);

        p.drawImage(0, 0, image_);
        setImage(padded);
    }

    // Brute force one pipeline process so that overlay is visible
    processPipeline();

    emit imageUpdated(image_);
}




// NOTE: This needs to happen on worker thread
void ImageProcessWorker::processPipeline()
{
    pipeline_.process(&image_, &processedImage_);
}
