#pragma once

#include <QImage>

#include <pgqt/pipeline.h>

class PipelineContext
{
public:
};

using ImagePipelineAction = pgqt::v1::PipelineAction<QImage>;


class CacheAction : ImagePipelineAction
{
    QImage cached_;

public:
    void process(const QImage& source, QImage& dest) override;
};

class BrightnessAction : public ImagePipelineAction
{
    int value_;

public:
    void value(int v) { value_ = v; enabled_ = true; }

    void process(const QImage& source, QImage& dest) override;
};


class ContrastAction : public ImagePipelineAction
{
    int value_;

public:
    void value(int v) { value_ = v; enabled_ = true; }

    void process(const QImage& source, QImage& dest) override;
};


class CropAction : public ImagePipelineAction
{
    QSize size_;

public:
    void setSize(const QSize& v) { size_ = v; }

    void process(const QImage& source, QImage& dest) override;
};


class OverlayAction : public ImagePipelineAction
{
    QImage overlay_;

public:
    void image(const QImage& i) { overlay_ = i; enabled_ = true; }

    void process(const QImage& source, QImage& dest) override;
};
