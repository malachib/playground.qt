#pragma once

#include <forward_list>

#include <QObject>
#include <QImage>
#include <QSemaphore>
#include <QThread>

#include "pipeline.h"

class ImageProcessWorker: public QObject
{
    Q_OBJECT

    QThread worker_;
    QImage image_;
    QImage processedImage_;

    pgqt::v1::Pipeline<QImage> pipeline_;

    // DEBT: Cumbersome using this AND unique_ptr in pipeline_
    BrightnessAction* brightnessAction_;
    ContrastAction* contrastAction_;
    OverlayAction* overlayAction_;

public:
    ImageProcessWorker(QObject* parent = nullptr);
    ~ImageProcessWorker();

    QSemaphore brightnessRequests;
    QSemaphore contrastRequests;
    QSemaphore anyRequests;

    void setImage(const QImage&);
    void setOverlayImage(const QImage&);

    void processPipeline();

    enum class Modes
    {
        Worker,
        Pipeline
    };

    Modes mode_ = Modes::Pipeline;


public slots:
    void signalCompressor();
    void adjustBrightness(int value);
    void adjustContrast(int value);

signals:
    void processed(const QImage&);

    // Occurs with any image update, (i.e. process or set)
    void imageUpdated(const QImage&);
};
