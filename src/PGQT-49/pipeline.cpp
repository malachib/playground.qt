#include <QPainter>

#include "pipeline.hpp"


void OverlayAction::process(const QImage& source, QImage& dest)
{
    if(overlay_.width() == 0)
    {
        dest = source;
        return;
    }

    for(int y = 0; y < source.height(); ++y)
    {
        auto source_line = reinterpret_cast<const QRgb*>(source.scanLine(y));
        auto overlay_line = reinterpret_cast<const QRgb*>(overlay_.scanLine(y));
        auto dest_line = reinterpret_cast<QRgb*>(dest.scanLine(y));

        for(int x = 0; x < source.width(); ++x)
        {
            const QRgb& rgb = source_line[x];
            const QRgb& overlay_rgb = overlay_line[x];

            unsigned r = averagePixels(qRed(rgb), qRed(overlay_rgb));
            unsigned g = averagePixels(qGreen(rgb), qGreen(overlay_rgb));
            unsigned b = averagePixels(qBlue(rgb), qBlue(overlay_rgb));

            QRgb& dest_rgb = dest_line[x];

            dest_rgb = qRgb(r, g, b);
        }
    }
}

void BrightnessAction::process(const QImage& source, QImage& dest)
{
    const int value = value_;

    processImageRgbChannel(source, dest, [value](int channel)
    {
        return changeBrightness(channel, value);
    });
}


void ContrastAction::process(const QImage& source, QImage& dest)
{
    const int value = value_;

    processImageRgbChannel(source, dest, [value](int channel)
    {
        return changeContrast(channel, value);
    });
}


void CacheAction::process(const QImage& source, QImage& dest)
{

}


void CropAction::process(const QImage& source, QImage& dest)
{
    QImage padded(size_, source.format());
    QPainter p(&padded);

    p.drawImage(0, 0, source);
    dest = padded;
}
